<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use App\Traits\CaptureIpTrait;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\StudentClass;
use App\Models\Matricula;
use App\Models\Role;
use App\Models\Term;
use App\Models\Subject;
use App\Models\SubjectAssessment;
use App\Models\StudentSubjectTaken;
use App\Models\StudentAssessmentResult;
use Response;
use Validator;
use View;

class AdminMarksManagementController extends Controller {
  /**
  * Display the specified resource.
  *
  */
  public function index()
  {
    $students = Student::latest()->get();
    $teachers = Teacher::latest()->get();
    $roles = Role::latest()->get();
    $terms = Term::latest()->get();
    $subjects = Subject::latest()->get();
    $assessments = SubjectAssessment::all();

    $data = [
      'students' => $students,
      'roles' => $roles,
      'terms' => $terms,
      'subjects' => $subjects,
      'teachers' => $teachers,
      'assessments' => $assessments,
    ];

    return View('management.students.notas.create', $data);
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
    $student = Student::findOrFail($id);
    $roles = Role::latest()->get();
    $terms = Term::latest()->get();

    return View('management.students.notas.show',
      compact('students', 'student', 'roles', 'terms'));
  }


  public function create($id)
  { 
    $student = Student::findOrFail($id);
    $teachers = Teacher::latest()->get();
    $roles = Role::latest()->get();
    $terms = Term::latest()->get();
    $subjects = Subject::latest()->get();
    $assessments = SubjectAssessment::all();

    $data = [
      'estudante' => $student,
      'roles' => $roles,
      'terms' => $terms,
      'subjects' => $subjects,
      'teachers' => $teachers,
      'assessments' => $assessments,
    ];

    return View('management.students.notas.create-single', $data);
  }

  public function store(Request $request) {
    $rules = [
      'estudante_id'    => 'required',
      'trimestre_id'  => 'required',
      'disciplina_id' => 'required',
      'professor_id' => 'required',
    ];

    if($request->input('final_assessments') == 'false'){
      $rules['mac'] = 'required';
      $rules['cpp'] = 'required';
      $rules['ct' ] = 'required';
    }else{
      $rules['cap'  ] = 'required';
      $rules['cepce'] = 'required';
      $rules['exame'] = 'required';
    }

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $student = Student::with('disciplinas_cursadas', 'matriculas')
                      ->find($request->input('estudante_id'));
    $matricula = Matricula::where('estudante_id', $student->id)->get()->last();
    $teacher =  Teacher::find($request->input('professor_id'));
    $class_id = $matricula->classe_id ;//StudentClass::whereNome('12')->first()->id;
    $trimestre_id = $request->input('trimestre_id');
    $disciplina_id   = $request->input('disciplina_id');

    if(count($student->matriculas) == 0){
      return back()
              ->with('error', trans('marksmanagement.createNoMatriculation'));
    }

    $subject = $student->disciplinas_cursadas
                          ->where('disciplina_id', $disciplina_id)
                          ->first();

    $this->saveResult($request,
                  $student->id,
                  $matricula->id,
                  $trimestre_id,
                  $teacher->id,
                  $class_id,
                  $subject->id);

    return redirect(route('estudantes.show', $student->id));
  }

  public function getSubjects(Request $request, $user)
  {
    $student = Student::where('nr_de_reg', $user)->first();
    $user = $student->user;
    $disciplinas_cursadas = $student->disciplinas_cursadas;
    $avaliacoes = SubjectAssessment::latest()->get();

    foreach ($disciplinas_cursadas as $key => $disciplina_cursada) {
      $subjects[] = Subject::find($disciplina_cursada->disciplina_id);
    }

    return  View::make('management.students.notas._subjects',
    [
      'user' => $user,
      'subjects' => $subjects,
    ])->render();

  }

  public function getCurrentMarks(Request $request, $user)
  {
    $student = Student::findOrFail($user);
    $matricula = Matricula::where('estudante_id', $student->id)
                          ->get()
                          ->last();
    $teacher =  Teacher::find($request->input('professor_id'));
    $disciplina_id   = $request->input('disciplina_id');
    $trimestre_id = $request->input('trimestre_id');
    $class_id = $matricula->classe_id ;
    $subject = $student->disciplinas_cursadas
                      ->where('disciplina_id', $disciplina_id)
                      ->first();


    if(is_null($student)){
      return response()->json(['error' => 404, 'message' => trans('app.notFound')]);
    }

    $assessmentResults = StudentAssessmentResult::where('estudante_id',  $student->id)
                                                ->where('matricula_id', $matricula->id)
                                                ->where('classe_id'   , $class_id)
                                                ->with(['disciplina_cursada.disciplina', 
                                                        'professor',
                                                        'avaliacao.resultados' => function ($query) use($student) {
                                                            $query->where('estudante_id', $student->id);
                                                        }
                                                       ])
                                                ->get();



    return $assessmentResults;
  }

  public function getCurrentMarksBySubject(Request $request, $user)
  {
    $student = Student::findOrFail($user);
    $matricula = Matricula::where('estudante_id', $student->id)
                          ->get()
                          ->last();
    $teacher =  Teacher::find($request->input('professor_id'));
    $disciplina_id   = $request->input('disciplina_id');
    $trimestre_id = $request->input('trimestre_id');
    $class_id = $matricula->classe_id ;
    $subject = $student->disciplinas_cursadas
                      ->where('disciplina_id', $disciplina_id)
                      ->first();
    
    if(is_null($student)){
      return response()->json(['error' => 404, 'message' => trans('app.notFound')]);
    }
    

    $assessmentResults = StudentAssessmentResult::where('trimestre_id', $trimestre_id)
                                                ->where('classe_id'   , $class_id);

    if(!is_null($student)){
      $assessmentResults->where('estudante_id',  $student->id);
    }
    if(!is_null($matricula)){
      $assessmentResults->where('matricula_id', $matricula->id);
    }
    if(!is_null($subject)){
      $assessmentResults->where('disciplina_cursada_id', $subject->id);
    }

    $assessmentResults->with(['disciplina_cursada.disciplina', 
                            'professor',
                            'avaliacao.resultados' => function ($query) use($student) {
                                $query->where('estudante_id', $student->id);
                            }
                            ]);
    return Response::json($assessmentResults->get());
  }

  public function getMarks(Request $request, $user)
  {
    $current_term = Term::first();
    $student = Student::where('nr_de_reg', $user)->first();
    if(is_null($student)){
      return response()->json(['error' => 404, 'message' => trans('app.notFound')]);
    }

    if($request->has('trimestre_id')){
      $matricula = Matricula::where('estudante_id', $student->id)->get()->last();
      $disciplinas_cursadas = $student->disciplinas_cursadas
                                      ->where('matricula_id', $matricula->id);
    }else{
      $disciplinas_cursadas = $student->disciplinas_cursadas;
    }

    $terms = Term::latest()->get();
    $avaliacoes = SubjectAssessment::latest()->get();

    return View::make('management.students.notas._marks',
    [
      'user' => $user,
      'student' => $student,
      'current_term' => $current_term,
      'terms' => $terms,
      'disciplinas_cursadas' => $disciplinas_cursadas,
      'avaliacoes' => $avaliacoes,
    ])->render();
  }

  public function saveResult($request,
                             $user_id,
                             $matricula_id,
                             $trimestre_id,
                             $teacher_id,
                             $student_class_id,
                             $subject_id) {

    $avaliacoes = [];
    if($request->has('mac'))
    {
      $avaliacao =  ['name' => 'mac', 'value' => $request->input('mac')];
      array_push($avaliacoes, $avaliacao);
    }

    if($request->has('cpp'))
    {
      $avaliacao =  ['name' => 'cpp', 'value' => $request->input('cpp')];
      array_push($avaliacoes, $avaliacao);
    }

    if($request->has('ct'))
    {
      $avaliacao =  ['name' => 'ct', 'value' => $request->input('ct')];
      array_push($avaliacoes, $avaliacao);
    }

    if($request->has('cap'))
    {
      $avaliacao =  ['name' => 'cap', 'value' => $request->input('cap')];
      array_push($avaliacoes, $avaliacao);
    }

    if($request->has('cepce'))
    {
      $avaliacao =  ['name' => 'cepce', 'value' => $request->input('cepce')];
      array_push($avaliacoes, $avaliacao);
    }

    if($request->has('exame'))
    {
      $avaliacao =  ['name' => 'exame', 'value' => $request->input('exame')];
      array_push($avaliacoes, $avaliacao);
    }

    
    foreach($avaliacoes as $avaliacao)
    {
      $avaliacao_id = SubjectAssessment::whereSlug($avaliacao['name'])->first()->id;
      $data = [
        'estudante_id' => $user_id,
        'disciplina_cursada_id' => $subject_id,
        'classe_id'    => $student_class_id,
        'trimestre_id' => $trimestre_id,
        'matricula_id' => $matricula_id,
        'avaliacao_id' => $avaliacao_id,
        'professor_id' => $teacher_id,
        'nota' => $avaliacao['value'],
      ];
      
      $assessmentResult = StudentAssessmentResult::where('estudante_id', $user_id)
                                                ->where('disciplina_cursada_id', $subject_id)
                                                ->where('classe_id'   , $student_class_id)
                                                ->where('trimestre_id', $trimestre_id)
                                                ->where('matricula_id', $matricula_id)
                                                ->where('avaliacao_id', $avaliacao_id)
                                                ->first();
      if(!is_null($assessmentResult)){
        $assessmentResult->update($data);
      }else{
        StudentAssessmentResult::create($data);
      }
    }
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;
use App\Models\Contact;
use Validator;

class ContactosManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $contactos = Contact::latest()->get();
    $data = [
      'contactos' => $contactos,
    ];
    return View('management.contactos.index', $data);
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
    $contacto = Contact::findOrFail($id);
    return view('management.contactos.show')->with('contacto', $contacto);
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\SiteInfo;
use App\Helpers\UploadImage;
use Illuminate\Support\Facades\Validator;
use Auth;

class EventsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();

    $events = Event::latest()->get();
    $data = [
      'events' => $events
    ];

    return view('management.pages.events.index', $data);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('management.pages.events.create');
  }

  public function store(Request $request)
  {

    $validator = Validator::make($request->all(),[
      'usuario_id'      => 'required',
      'nome'            => 'required',
      'descricao'       => 'required',
      'hora_de_comeco'  => 'required',
      'hora_de_termino' => 'required',
      'data' => 'required|date',
      'lugar' => 'required',
    ]);

    if ($validator->fails())
    {
      return back()
          ->withInput($request->all())
          ->withErrors($validator);
    }
    $eventData = $request->all();

    $event = Event::create($eventData);
    return redirect(route('admin.eventos'))
               ->with('success', trans('eventsmanagement.createSuccess'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $event = Event::findOrFail($id);

    $data = [
      'event'        => $event,
    ];

    return view('management.pages.events.edit', $data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int                      $id
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $event = Event::findOrFail($id);

    $validator = Validator::make($request->all(), [
      'usuario_id'      => 'required',
      'nome'            => 'required',
      'descricao'       => 'required',
      'hora_de_comeco'  => 'required',
      'hora_de_termino' => 'required',
      'data' => 'required|date',
      'lugar' => 'required',
      // 'comprovativo'    => 'file|mimes:pdf',
    ]);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $event->update($request->all());

    // if ($request->has('comprovativo')) {
    //   $event->comprovativo = $this->upload($request);
    //   $event->save();
    // }

    return back()->with('success', trans('eventsmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $event = Event::findOrFail($id);
    $event->delete();

    return redirect(route('admin.eventos'))
            ->with('success', trans('eventsmanagement.deleteSuccess'));
  }

  function getFile($file=null, $size = 100){
    return UploadImage::uploadImage($file, '/images/site', $size, null);
  }
}

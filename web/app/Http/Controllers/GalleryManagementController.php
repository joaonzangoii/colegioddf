<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Helpers\UploadImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GalleryManagementController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    return view('management.gallery.home');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('management.gallery.create');
  }

  /**
   * This creates a new Student and also stores its corresponding detail in the user's table.
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

    $validator = Validator::make(
      $request->all(),
      Gallery::$validationRules);

    if ($validator->fails())
    {
      return back()
          ->withInput($request->all())
          ->withErrors($validator);
    }

    $imageData = array (
      'titulo' => $request->input('titulo'),
      'imagem' => UploadImage::uploadImage(
        $request->file('imagem'),
        '/images/gallery', 600, null),
      'responsavel_id' => $request->input('responsavel_id'),
    );

    if ($gallery = Gallery::create($imageData))
    {
      return view('management.gallery.show')->with('image', $gallery);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
  $image = Gallery::findOrFail($id);
    return view('management.gallery.show')->with('image', $image);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $image = Gallery::findOrFail($id);
    return view('management.gallery.edit')->with('image', $image);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->all(), Gallery::$editValidationRules);

    if ($validator->fails())
    {
        return back()
                ->withInput($request->all())
                ->withErrors($validator);
    }

    $gallery = Gallery::findOrFail($id);
    $gallery->titulo = $request->input('titulo');

    if ($request->hasFile('imagem')) {
      $gallery->image =
      UploadImage::uploadImage(
        $request->file('imagem'),
        '/images/gallery', 600, null);
    }

    if ($gallery->save())
    {
      return redirect(route('photos.all'));
    }

    return back()
        ->withInput($request->all())
        ->withErrors(trans('imagesmanagement.updateError'));
  }

  /**
   * This fetches all students without filtering.
   * @return $this
   */
  public function all()
  {
    $gallery = Gallery::latest()->get();
    return view('management.gallery.index')
            ->with('images', $gallery);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $photo = Gallery::find($id);

    if ($photo->delete()) {
      // if (file_exists(public_path().'/gallery/'.$photo->imagem))
      // {
      //   $deleted = Storage::delete('gallery/'. $photo->imagem);

      //   // dd('gallery/'. $photo->imagem);
      // }
      return redirect('photo/all');
    }
  }

  /**
   * This fetches all students without filtering.
   * @return $this
   */

  /**
   *THis method shows the list of filtered students
   * @param Request $request
   * @return $this
   */
  public function filtered(Request $request)
  {
    $year_from = $request->input('news_year_from');
    $month_from = $request->input('news_month_from');

    $year_to = $request->input('news_year_to');
    $month_to = $request->input('news_month_to');

    if (intval($year_from) > intval($year_to))
        return redirect(route('photos'))
        ->withErrors(trans('imagesmanagement.wrongDateRange'));

    $dateFrom = $year_from."-".$month_from."-"."01";
    $dateTo = $year_to."-".$month_to."-"."31";

    $dateFrom = new \DateTime($dateFrom);
    $dateTo = new \DateTime($dateTo);

    $dateFrom = $dateFrom->format('Y-m-d H:i:s');
    $dateTo = $dateTo->format('Y-m-d H:i:s');

    // echo $dateFrom. " ". $dateTo;
    $gallery = Gallery::where('created_at', '>=', $dateFrom)
                      ->where('created_at', '<=', $dateTo)
                      ->get();

    if ($gallery->count() > 0)
    {
      return view('management.gallery.index')->with('images', $gallery);
    }
    return redirect(route('photos'))
    ->withErrors(trans('imagesmanagement.noImages'));

  }
}

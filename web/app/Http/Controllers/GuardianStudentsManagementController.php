<?php

namespace App\Http\Controllers;

use App;
use Auth;
use App\Models\Profile;
use App\Models\User;
use App\Models\Student;
use App\Models\Guardian;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\StudentClass;
use App\Models\Matricula;
use App\Models\Role;
use App\Models\Curso;
use App\Models\StudentSubjectTaken;
use App\Traits\ActivationTrait;
use Validator;

class GuardianStudentsManagementController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $guardian = Auth::user()->userable;
    $students = Student::with('user')
                       ->where('encarregado_id', $guardian->id)
                       ->latest()
                       ->get();
    $roles = Role::latest()->get();
    $data =[
      'students' => $students,
      'roles' => $roles,
      'show_route'    => 'encarregado.estudantes.show',
      'edit_route'    => 'encarregado.estudantes.edit',
      'destroy_route' => 'estudante.destroy',
      'notas_add'     => 'estudante.notas.add.index',
      'payments_route' => 'encarregado.estudantes.pagamentos',
      'deleted_route' => 'deleted.index',
    ];
    return View('management.students.index', $data);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $student = Student::findOrFail($id);
    $terms = \App\Models\Term::latest()->get();
    $disciplinas_cursadas = $student->disciplinas_cursadas;
    $matriculas = $student->matriculas;
    $avaliacoes = \App\Models\SubjectAssessment::latest()->get();

    $data = [
      'student' => $student,
      'terms' => $terms,
      'avaliacoes' => $avaliacoes,
      'matriculas' => $matriculas,
      'index_route' => 'encarregado.estudantes',
    ];
    return view('management.students.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $student = Student::findOrFail($id);
    $user = $student->user;
    $roles = Role::latest()->get();
    foreach ($user->roles as $students_role) {
        $currentRole = $students_role;
    }

    $data = [
      'student'       => $student,
      'roles'         => $roles,
      'currentRole'   => $currentRole,
      'index_route'    => 'encarregado.estudantes',
      'show_route'     => 'encarregado.estudantes.show',
      'edit_action'    => 'GuardianStudentsManagementController@update',
    ];

    return view('management.students.edit')->with($data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int                      $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $currentUser = Auth::user();
    $student = Student::findOrFail($id);
    $user = $student->user;

    $emailCheck = ($request->input('email') != '')
    && ($request->input('email') != $user->email);
    $ipAddress = new CaptureIpTrait();

    if ($emailCheck) {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'email'                 => 'required|email|max:255|unique:users',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'present|min:6|max:20|confirmed',
          'password_confirmation' => 'present|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'email.required'        => trans('auth.emailRequired'),
          'email.email'           => trans('auth.emailInvalid'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    } else {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'nullable|min:6|max:20|confirmed',
          'password_confirmation' => 'nullable|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    }

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $user->nome = $request->input('nome');
    $student->primeiro_nome = $request->input('primeiro_nome');
    $student->sobrenome = $request->input('sobrenome');

    if ($emailCheck) {
        $user->email = $request->input('email');
    }

    if ($request->input('password') != null) {
        $user->password = bcrypt($request->input('password'));
    }

    $user->detachAllRoles();
    $user->attachRole($request->input('role'));
    $user->updated_ip_address = $ipAddress->getClientIp();
    $student->save();
    $user->save();

    return back()->with('success', trans('studentsmanagement.updateSuccess'));
  }
}

<?php

namespace App\Http\Controllers;
use App\Models\News;
use App\Models\Event;

class NewsController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $noticias = News::paginate(12);
    $eventos =  Event::paginate(3);
    return view('website.pages.noticias.index', compact('noticias', 'eventos'));
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $noticia =  News::findOrFail($id);
    $noticias = News::paginate(4);
    $eventos =  Event::paginate(3);
    return view('website.pages.noticias.show', compact('noticia', 'noticias', 'eventos'));
  }
}

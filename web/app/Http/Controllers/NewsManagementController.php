<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\SiteInfo;
use App\Helpers\UploadImage;
use Illuminate\Support\Facades\Validator;
use Auth;

class NewsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();

    $news = News::latest()->get();
    $data = [
      'news' => $news
    ];

    return view('management.pages.news.index', $data);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('management.pages.news.create');
  }

  public function store(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'imagem'    => 'required|image',
      'usuario_id'      => 'required',
      'titulo'            => 'required',
      'descricao'       => 'required',
    ]);

    if ($validator->fails())
    {
      return back()
          ->withInput($request->all())
          ->withErrors($validator);
    }

    $newsData = array (
      'titulo' => $request->input('titulo'),
      'descricao' => $request->input('descricao'),
      'imagem' => UploadImage::uploadImage(
        $request->file('imagem'),
        '/images/news', 600, null),
      'usuario_id' => $request->input('usuario_id'),
    );

    $news = News::create($newsData);
    return redirect(route('admin.noticias'))
               ->with('success', trans('noticiasmanagement.createSuccess'));
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $news = News::findOrFail($id);

    return view('management.pages.news.show', compact('news'));
  }


  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $news = News::findOrFail($id);

    $data = [
      'news' => $news,
    ];

    return view('management.pages.news.edit', $data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int                      $id
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $news = News::findOrFail($id);

    $validator = Validator::make($request->all(), [
      'usuario_id'      => 'required',
      'titulo'          => 'required',
      'descricao'       => 'required',
    ]);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $news->update($request->all());

    if ($request->has('imagem')) {
      $news->imagem = UploadImage::uploadImage(
        $request->file('imagem'),
        '/images/news', 600, null);
      $news->save();
    }

    return back()->with('success', trans('noticiasmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $news = News::findOrFail($id);
    $news->delete();

    return redirect(route('admin.eventos'))
            ->with('success', trans('noticiasmanagement.deleteSuccess'));
  }
}

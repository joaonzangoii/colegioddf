<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\SiteInfo;
use App\Helpers\UploadImage;
use Illuminate\Support\Facades\Validator;
use Auth;

class PagesManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();

    $events = Event::latest()->get();
    $data = [
      'events' => $events
    ];
    if ($user->isAdmin()) {

      return view('management.pages.admin.home', $data)
             ->with('success', 'ddd');
    }

    return view('management.pages.user.home', $data)
           ->with('success', 'ddd');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function listRoutes()
  {
    $routes = Route::getRoutes();
    $data = [
      'routes' => $routes,
    ];

    return view('management.pages.admin.route-details', $data);
  }

  public function listPHPInfo()
  {
      return view('management.pages.admin.php-details');
  }

  public function listSiteInfo()
  {
    $siteinfo = SiteInfo::latest()->first();
    if($siteinfo == null){
      $siteinfo = new SiteInfo;
    }
    return view('management.pages.admin.site-details', compact('siteinfo'));
  }

  public function postSiteInfo(Request $request)
  {
    $validator = Validator::make($request->all(),[
      'nome'=> 'required',
      'email'=> 'required',
      'telefone'=> 'required',
      'fax'=> 'required',
      'endereco'=> 'required',
      'coordenadas'=> 'required',
      'descricao'=> 'required',
      'endereco'=> 'required',
      'logo_cabecalho'      => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      'logo_rodape'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      'usuario_id'=> 'required',
    ]);

    if ($validator->fails())
    {
      return back()
          ->withInput($request->all())
          ->withErrors($validator);
    }

    $siteData = array (
      'nome' => $request->input('nome'),
      'email' => $request->input('email'),
      'telefone' => $request->input('telefone'),
      'fax' => $request->input('fax'),
      'endereco' => $request->input('endereco'),
      'coordenadas' => $request->input('coordenadas'),
      'descricao' => $request->input('descricao'),
      'logo_cabecalho' => '/images/site/' . $this->getFile($request->file('logo_cabecalho')),
      'logo_rodape' => '/images/site/' . $this->getFile($request->file('logo_rodape')),
      'usuario_id' => $request->input('usuario_id')
    );

    $siteinfo = SiteInfo::latest()->first();
    if($siteinfo == null){
      $siteinfo = SiteInfo::create($siteData);
    }else{
      $siteinfo->update($siteData);
    }

    return back()->with('success', trans('siteinfodetailsmanagement.updateSuccess'));
  }

  public function getAboutUsPage()
  {
    $siteinfo = SiteInfo::latest()->first();
    if($siteinfo == null){
      $siteinfo = new SiteInfo;
    }

    return view('management.pages.admin.about-us', compact('siteinfo'));
  }

  public function postAboutUsPage(Request $request)
  {

    $validator = Validator::make($request->all(),[
      'acerca'=> 'required',
      'foto_de_capa_acerca'=> 'required|image',
      'usuario_id'=> 'required',
    ]);

    if ($validator->fails())
    {
      return back()
          ->withInput($request->all())
          ->withErrors($validator);
    }
    $siteData = array (
      'acerca' => $request->input('acerca'),
      'foto_de_capa_acerca' => '/images/site/' .
       $this->getFile($request->file('foto_de_capa_acerca'), 1024),
      'usuario_id' => $request->input('usuario_id')
    );

    $siteinfo = SiteInfo::latest()->first();
    if($siteinfo == null){
      $siteinfo = SiteInfo::create($siteData);
    }else{
      $siteinfo->update($siteData);
    }

    return back()->with('success', trans('siteinfodetailsmanagement.updateSuccess'));
  }

  function getFile($file=null, $size = 100){
    return UploadImage::uploadImage($file, '/images/site', $size, null);
  }

  public function getEventsPage()
  {
    return view('management.pages.admin.events');
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Term;
use App\Models\Student;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use File;
use Image;

class PaymentsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $payments = Payment::latest()->get();
    return View('management.payments.index', compact('payments'));
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $students = Student::latest()->get();
    $cf = Term::where('codigo', 'CF')->first();
    $terms = Term::where('id', '!=', $cf->id)->get();
    $data =[
      'terms'=> $terms
    ];
    return View('management.payments.create', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),
    [
      'trimestre_id'                  => 'required',
      'valor'                         => 'required|numeric',
      'mes'                           => 'required|numeric',
    ]);

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $request->merge(['mes' => ltrim($request->input('mes'), '0')]);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $data = $request->all();
    $students = Student::with('user')->latest()->get();
    foreach ($students as $key => $student) {
      $matriculas = $student->matriculas;
      if(count($matriculas)>0){
        $matricula = $matriculas->slice(-1)->pop();
        if(!Payment::where('matricula_id', $matricula->id)
                   ->where('trimestre_id', $data['trimestre_id'])
                   ->where('mes', $data['mes'])
                   ->first()){
          $data['matricula_id'] = $matricula->id;
          $data['estudante_id'] = $student->id;
          $payment = Payment::create($data);
        }else{
          return back()->withErrors([trans('paymentsmanagement.alreadyAdded')])->withInput();
        }
      }
    }

    return redirect(route('pagamentos'))
           ->with('success', trans('paymentsmanagement.createSuccess'));
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $payment = Payment::findOrFail($id);
    return view('management.payments.show')->withPayment($payment);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $payment = Payment::findOrFail($id);
    $students = Student::latest()->get();
    $approveds = collect(['1'=> trans('titles.approved'),
                          '0'=> trans('titles.notApproved')]);
    $currentState = $payment->aprovado;
    $currentStudent = $payment->estudante_id;
    $data = [
      'payment'      => $payment,
      'approveds'    => $approveds,
      'students'     => $students,
      'currentState' => $currentState,
      'currentStudent' => $currentStudent,
    ];

    return view('management.payments.edit')->with($data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int                      $id
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $payment = Payment::findOrFail($id);

    $validator = Validator::make($request->all(), [
      'estudante_id'      => 'required',
      'aprovado'          => 'required',
      'comprovativo'      => 'file|mimes:pdf',
    ]);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $payment->update($request->all());

    if ($request->has('comprovativo')) {
      $payment->comprovativo = $this->upload($request);
      $payment->save();
    }

    return back()->with('success', trans('paymentsmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $payment = Payment::findOrFail($id);
    $payment->delete();
    return redirect(route('pagamentos'))
           ->with('success', trans('paymentsmanagement.deleteSuccess'));
  }

  public function upload(Request $request) {
    $file = $request->file('comprovativo');
    $filename = uniqid() . $file->getClientOriginalName();
    $savepath = public_path('files');
    $file->store($savepath);
    return  Storage::disk('uploads')->put('files', $file);
  }
}

<?php

namespace App\Http\Controllers;

use App;
use Auth;
use App\Models\Profile;
use App\Models\User;
use App\Models\Student;
use App\Models\Guardian;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\StudentClass;
use App\Models\Matricula;
use App\Models\Role;
use App\Models\Curso;
use App\Models\StudentSubjectTaken;
use App\Traits\ActivationTrait;
use Validator;

class StudentsManagementController extends Controller
{
  use ActivationTrait;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $students = Student::with('user')->latest()->get();
    $roles = Role::latest()->get();

    $data =[
      'students' => $students,
      'roles' => $roles,
      'show_route'    => 'estudantes.show',
      'edit_route'    => 'estudantes.edit',
      'destroy_route' => 'estudante.destroy',
      'notas_add'     => 'estudante.notas.add.index',
      'payments_route' => 'estudantes.pagamentos',
      'deleted_route' => 'deleted.index',
    ];

    return View('management.students.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $encarregados = Guardian::latest()->get();
    $roles = Role::latest()->get();
    $cursos = Curso::latest()->get();
    $classes = StudentClass::latest()->get();
    $disciplinas = Subject::latest()->get();
    $currentRole = Role::whereName('Estudante')->first();
    $sexos = ['masculino' => "Masculino",
              'feminino' => 'Feminino'
             ];

    $data = [
      'encarregados' => $encarregados,
      'cursos' => $cursos,
      'classes' => $classes,
      'disciplinas' => $disciplinas,
      'currentRole'   => $currentRole,
      'sexos' => $sexos,
    ];

    return view('management.students.create')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),
      [
        'primeiro_nome'      => 'required',
        'sobrenome'          => 'required',
        'email'           => 'required|email|max:255|unique:users',
        'telefone'          => 'required',
        'sexo'              => 'required',
        'encarregado_id'    => 'required',
        'data_de_nascimento'=> 'required|date|date_format:d-m-Y',
        'password'          => 'required|min:6|max:20|confirmed',
        'password_confirmation' => 'required|same:password',

        'curso_id'   => 'required',
        'classe_id'   => 'required',
        'ano_lectivo' => 'required',
        'valor_da_matricula' => 'required',
        'propina_mensal'     => 'required',
        'disciplinas_ids'    => 'required',
      ],
      [
        'primeiro_nome.required'=> trans('auth.fNameRequired'),
        'sobrenome.required'    => trans('auth.lNameRequired'),
        'email.required'        => trans('auth.emailRequired'),
        'email.email'           => trans('auth.emailInvalid'),
        'password.required'     => trans('auth.passwordRequired'),
        'password.min'          => trans('auth.PasswordMin'),
        'password.max'          => trans('auth.PasswordMax'),
      ]
    );

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $ipAddress = new CaptureIpTrait();
    $profile = new Profile();
    $generator = App::make('GenerateRegNumber');
    $reg_no = $generator->generateStudentReg();
    $studentRole = Role::whereName('Estudante')->first();

    $date = new \DateTime($request->input('data_de_nascimento'));
    $dob =  $date->format('Y-m-d');

    $student = Student::create([
      'primeiro_nome' => $request->input('primeiro_nome'),
      'sobrenome' => $request->input('sobrenome'),
      'sexo' => $request->input('sexo'),
      'nr_de_reg' => $reg_no,
      'data_de_nascimento' => $dob,
      'telefone' => $request->input('telefone'),
      'encarregado_id' => $request->input('encarregado_id'),
    ]);

    $matricula = Matricula::create([
      'numero_de_matricula'    => '',
      'numero_de_boletim'      => '',
      'situacao_da_matricula'  => 'criada',
      'disciplinas_em_curso'   => 0,
      'disciplinas_concluidas' => 0,
      'valor_da_matricula'     => $request->input('valor_da_matricula'),
      'propina_mensal' => $request->input('propina_mensal'),
      'ano_lectivo'    => $request->input('ano_lectivo'),
      'estudante_id'   => $student->id,
      'curso_id'       => $request->input('curso_id'),
      'classe_id'      => $request->input('classe_id'),
      'responsavel_id' => Auth::user()->id,
    ]);

    $subjects = $request->input('disciplinas_ids');

    foreach ($subjects as $key => $id) {
      $subjetTaken = StudentSubjectTaken::create([
        'estudante_id'  => $student->id,
        'matricula_id'  => $matricula->id,
        'disciplina_id' => $id,
        'frequencia'    => 0,
        'nota_global'   => 0,
        'situacao'      => 'criada',
      ]);
    }

    $user = User::create([
      'nome' => $student->nome,
      'email'   => $request->input('email'),
      'userable_id'   =>$student->id,
      'userable_type' => "Estudante",
      'password'         => bcrypt($request->input('password')),
      'token'            => str_random(64),
      'activated'        => false,
      'admin_ip_address' => $ipAddress->getClientIp(),
    ]);

    $user->profile()->save($profile);
    $user->attachRole($studentRole);
    $user->save();
    $this->initiateEmailActivation($user);

    return redirect(route('estudantes'))
           ->with('success', trans('studentsmanagement.createSuccess'));
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $student = Student::findOrFail($id);
    $terms = \App\Models\Term::latest()->get();
    $disciplinas_cursadas = $student->disciplinas_cursadas;
    $matriculas = $student->matriculas;
    $avaliacoes = \App\Models\SubjectAssessment::latest()->get();

    $data = [
      'student' => $student,
      'terms' => $terms,
      'avaliacoes' => $avaliacoes,
      'matriculas' => $matriculas,
      'index_route' => 'estudantes',
      'notas_add'     => 'estudante.notas.add.create',
    ];
    return view('management.students.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $student = Student::findOrFail($id);
    $user = $student->user;
    $roles = Role::latest()->get();

    foreach ($user->roles as $students_role) {
      $currentRole = $students_role;
    }

    $data = [
      'student'       => $student,
      'roles'         => $roles,
      'currentRole'   => $currentRole,
      'index_route'    => 'estudantes',
      'show_route'     => 'estudantes.show',
      'edit_action'    => 'StudentsManagementController@update',
    ];

    return view('management.students.edit')->with($data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int                      $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $currentUser = Auth::user();
    $student = Student::findOrFail($id);
    $user = $student->user;

    $emailCheck = ($request->input('email') != '')
    && ($request->input('email') != $user->email);
    $ipAddress = new CaptureIpTrait();

    if ($emailCheck) {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'email'                 => 'required|email|max:255|unique:users',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'present|min:6|max:20|confirmed',
          'password_confirmation' => 'present|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'email.required'        => trans('auth.emailRequired'),
          'email.email'           => trans('auth.emailInvalid'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    } else {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'nullable|min:6|max:20|confirmed',
          'password_confirmation' => 'nullable|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    }

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $user->nome = $request->input('nome');
    $student->primeiro_nome = $request->input('primeiro_nome');
    $student->sobrenome = $request->input('sobrenome');

    if ($emailCheck) {
        $user->email = $request->input('email');
    }

    if ($request->input('password') != null) {
        $user->password = bcrypt($request->input('password'));
    }

    $user->detachAllRoles();
    $user->attachRole($request->input('role'));
    $user->updated_ip_address = $ipAddress->getClientIp();
    $student->save();
    $user->save();

    return back()->with('success', trans('studentsmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $currentUser = Auth::user();
    $student = Student::findOrFail($id);
    $user = $student->user;

    $ipAddress = new CaptureIpTrait();

    if ($user->id != $currentUser->id) {
      $user->deleted_ip_address = $ipAddress->getClientIp();
      $user->save();
      $user->delete();

      return redirect(route('estudantes'))->with('success', trans('studentsmanagement.deleteSuccess'));
    }

    return back()->with('error', trans('studentsmanagement.deleteSelfError'));
  }

  public function getReadmitir()
  {

  }

  public function postReadmitir()
  {

  }
}

<?php

namespace App\Http\Controllers;

use App;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Student;
use Validator;

class StudentsMarksManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
  * Fetch user
  * (You can extract this to repository method).
  *
  * @param $username
  *
  * @return mixed
  */
  public function getUserByUsername($username)
  {
    return User::with('profile','userable')->whereemail($username)->firstOrFail();
  }

  /**
   * Display a latest marks of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $user = $this->getUserByUsername(\Auth::user()->email);
    } catch (ModelNotFoundException $exception) {
      abort(404);
    }

    $student = $user->userable;
    $matricula = $student->matriculas->last();
    $disciplinas_cursadas = $student->disciplinas_cursadas
                                    ->where('matricula_id', $matricula->id)
                                    ->where('estudante_id', $student->id);

    $data = [
      'student' => $student,
      'matricula_id' => $matricula,
      'disciplinas_cursadas' => $disciplinas_cursadas,
    ];

    return View('management.students.notas.show', $data);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($matricula, $username)
  {
    try {
      $user = $this->getUserByUsername($username);
    } catch (ModelNotFoundException $exception) {
      abort(404);
    }

    $student = $user->userable;
    $disciplinas_cursadas = $student->disciplinas_cursadas
                                    ->where('matricula_id', $matricula)
                                    ->where('estudante_id', $student->id);
    $data = [
      'student' => $student,
      'matricula_id' => $matricula,
      'disciplinas_cursadas' => $disciplinas_cursadas,
    ];

    return View('management.students.notas.show', $data);
  }
}

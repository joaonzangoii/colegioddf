<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Student;
use App\Models\Matricula;
use App\Models\StudentAssessmentResult;
use Auth;

class StudentsMatriculationsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();
    $student = $user->userable;
    $matriculas = $student->matriculas;
    return View('management.students.matriculas.index', compact('matriculas'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = Auth::user();
    $student = $user->userable;
    $matricula = Matricula::find($id);
    $subjects = $student->disciplinas_cursadas
                        ->where('matricula_id', $matricula->id)
                        ->sortByDesc('created_at');
    $data = [
      'matricula' => $matricula,
      'subjects'  => $subjects
    ];
    return View('management.students.matriculas.show', $data);
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function showResults($id)
  {
    $user = Auth::user();
    $student = $user->userable;
    $subject = $student->disciplinas_cursadas->find($id);
    if(!is_null($subject)){
      $matricula = Matricula::find($subject->matricula_id);
      if(!is_null($subject->resultados) && count($subject->resultados)){
        $resultados = StudentAssessmentResult::where('matricula_id', $matricula->id)
                                             ->where('disciplina_cursada_id', $subject->id)
                                             ->get();
      }else{
        $resultados = [];
      }
    }else{
      $resultados = [];
    }

    if(!is_null($resultados) && count($resultados) > 0){
      $resultadoGroups = $resultados->groupBy('avaliacao_id');
    }else{
      $resultadoGroups = collect([]);
    }
    $data = [
      'student' => $student,
      'subject' => $subject,
      'resultadoGroups' => $resultadoGroups,
    ];
    return View('management.students.matriculas.show', $data);
  }
}

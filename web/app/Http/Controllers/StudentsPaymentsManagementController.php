<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Student;
use App\Models\User;
use App\Models\Term;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use File;
use Image;
use Auth;

class StudentsPaymentsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();
    $student = $user->userable;
    $payments = Payment::where('estudante_id', $student->id)
                       ->latest()
                       ->get();
    $total = 0;
    foreach ($payments as $key => $payment) {
      if($payment->aprovado == true){
        $total = $total + $payment->valor;
      }
    }

    $data = [
      'user' => $user,
      'student' => $student,
      'payments' => $payments,
      'total' => $total,
    ];
    return View('management.students.pagamentos.index', $data);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $cf = Term::where('codigo', 'CF')->first();
    $terms = Term::where('id', '!=', $cf->id)->get();
    $estudante = Auth()->user()->userable;
    $matricula = $estudante->matriculas->last();
    $data = [
      'terms' => $terms,
      'estudante_id' => $estudante->id,
      'matricula_id' => $matricula->id,
    ];
    return View('management.students.pagamentos.create', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),
      [
        'estudante_id'                  => 'required',
        'matricula_id'                  => 'required',
        'trimestre_id'                  => 'required',
        'valor'                         => 'required|numeric',
        'mes'                           => 'required|numeric',
      ]
    );

    $request->merge(['mes' => ltrim($request->input('mes'), '0')]);
    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $payment = Payment::create($request->all());

    return redirect(route('estudante.pagamentos'))
           ->with('success', trans('paymentsmanagement.createSuccess'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $payment = Payment::findOrFail($id);
    $currentState = $payment->aprovado;
    $currentStudent = Auth::user()->userable;
    $data = [
      'payment'      => $payment,
      'currentStudent' => $currentStudent,
      'currentState' => $currentState,
    ];

    return view('management.students.pagamentos.edit')->with($data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int                      $id
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $payment = Payment::findOrFail($id);

    $validator = Validator::make($request->all(), [
      'comprovativo'      => 'file|mimes:pdf',
    ]);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }


    if ($request->has('comprovativo')) {
      $payment->comprovativo = $this->upload($request);
      $payment->save();
    }

    return back()->with('success', trans('paymentsmanagement.updateSuccess'));
  }

  public function upload(Request $request) {
    $file = $request->file('comprovativo');
    $filename = uniqid() . $file->getClientOriginalName();
    $savepath = public_path('files');
    $file->store($savepath);
    return  Storage::disk('uploads')->put('files', $file);
  }
}

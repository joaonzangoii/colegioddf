<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Student;
use App\Models\Matricula;
use App\Models\StudentAssessmentResult;
use Auth;

class StudentsSubjectsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();
    $student = $user->userable;
    $subjects = $student->disciplinas_cursadas->sortByDesc('created_at');
    return View('management.students.disciplinas.index', compact('subjects'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = Auth::user();
    $student = $user->userable;
    $subject = $student->disciplinas_cursadas->find($id);
    if(!is_null($subject)){
      $matricula = Matricula::find($subject->matricula_id);
      if(!is_null($subject->resultados) && count($subject->resultados)){
        $resultados = StudentAssessmentResult::where('disciplina_cursada_id', $subject->id)
                                            ->where('matricula_id', $matricula->id)
                                             ->get();
      }else{
        $resultados = [];
      }
    }else{
      $resultados = [];
    }

    if(!is_null($resultados) && count($resultados) > 0){
      $resultadoGroups = $resultados->groupBy('avaliacao_id');
    }else{
      $resultadoGroups = collect([]);
    }
    $data = [
      'student' => $student,
      'subject' => $subject,
      'resultadoGroups' => $resultadoGroups,
    ];
    return View('management.students.disciplinas.show', $data);
  }
}

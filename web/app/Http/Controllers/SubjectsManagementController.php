<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use Validator;

class SubjectsManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $subjects = Subject::latest()->get();
    return View('management.subjects.index', compact('subjects'));
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return View('management.subjects.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),
      [
        'nome'                  => 'required|max:255|unique:users',
      ],
      [
        'name.unique'           => trans('auth.userNameTaken'),
        'name.required'         => trans('auth.userNameRequired'),
      ]
    );

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $subject = Subject::create([
      'nome' => $request->input('nome'),
    ]);

    return redirect(route('disciplinas'))
           ->with('success', trans('subjectsmanagement.createSuccess'));
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $subject = Subject::findOrFail($id);

    return view('management.subjects.show')->withSubject($subject);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $subject = Subject::findOrFail($id);

    $data = [
      'subject' => $subject,
    ];

    return view('management.subjects.edit')->with($data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int                      $id
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $subject = Subject::find($id);

    $validator = Validator::make($request->all(), [
      'nome'      => 'required|max:255',
    ]);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $subject->nome = $request->input('nome');
    $subject->save();

    return back()->with('success', trans('subjectsmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $subject = Subject::findOrFail($id);

    if ($subject->delete()) {
      $subject->delete();
      return redirect(route('disciplinas'))->with('success', trans('subjectsmanagement.deleteSuccess'));
    }

    return back()->with('error', trans('subjectsmanagement.deleteSelfError'));
  }
}

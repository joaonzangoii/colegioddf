<?php

namespace App\Http\Controllers;

use App;
use Auth;
use App\Models\Profile;
use App\Models\User;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Guardian;
use App\Models\Role;
use Validator;

class UsersManagementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $type =  $this->replaceLanguage($request);
    $type =  $this->replaceLanguage($request);

    switch ($type) {
      case 'professores':
        $title = trans('usersmanagement.showAllTeachers');
        $createTitle = trans('usersmanagement.createTeacher');
        $createRoute = 'professores.create';
        $showRoute   = 'professor.show';
        $editRoute   =  'professor.edit';
        $deleteRoute =  'professor.destroy';
        $users = $this->getUsers('Professor');
        break;
      case 'encarregados':
        $title = trans('usersmanagement.showAllGuardians');
        $createTitle = trans('usersmanagement.createGuardian');
        $createRoute = 'encarregados.create';
        $showRoute   = 'encarregado.show';
        $editRoute   =  'encarregado.edit';
        $deleteRoute =  'encarregado.destroy';
        $users = $this->getUsers('Encarregado');
        break;
      default:
        $title = trans('usersmanagement.showAllUsers');
        $createTitle = trans('usersmanagement.createUser');
        $createRoute = 'usuarios.create';
        $showRoute   = 'usuario.show';
        $editRoute   =  'usuario.edit';
        $deleteRoute =  'usuario.destroy';
        $users = User::latest()->get();
        break;
    }

    $roles = Role::latest()->get();
    $data = [
      'title' => $title,
      'createTitle' => $createTitle,
      'createRoute' => $createRoute,
      'showRoute' => $showRoute,
      'editRoute'   => $editRoute,
      'deleteRoute' => $deleteRoute,
      'users' => $users,
      'roles' => $roles,
      'type' => $type,
    ];
    return View('management.users.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $roles = Role::latest()->get();
    $sexos = ['masculino' => "Masculino", 'feminino' => 'Feminino'];
    $type = str_replace('/create', '' , $this->replaceLanguage($request));
    switch ($type) {
      case 'professores':
        $indexRoute = 'professores';
        $createTitle = trans('usersmanagement.createTeacher');
        break;
      case 'encarregados':
        $indexRoute = 'encarregados';
        $createTitle = trans('usersmanagement.createGuardian');
        break;
      default:
        $indexRoute = 'usuarios';
        $createTitle = trans('usersmanagement.createUser');
        break;
    }
    $data = [
      'roles' => $roles,
      'sexos' => $sexos,
      'indexRoute' => $indexRoute,
      'createTitle' => $createTitle,
    ];

    return view('management.users.create')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),
      [
        'primeiro_nome'         => 'required',
        'sobrenome'             => 'required',
        'email'                 => 'required|email|max:255|unique:users',
        'sexo'                  => 'required',
        'data_de_nascimento'    => 'required|date|date_format:d-m-Y',
        'password'              => 'required|min:6|max:20|confirmed',
        'password_confirmation' => 'required|same:password',
        'role'                  => 'required',
      ],
      [
        'primeiro_nome.required'=> trans('auth.fNameRequired'),
        'sobrenome.required'    => trans('auth.lNameRequired'),
        'email.required'        => trans('auth.emailRequired'),
        'email.email'           => trans('auth.emailInvalid'),
        'password.required'     => trans('auth.passwordRequired'),
        'password.min'          => trans('auth.PasswordMin'),
        'password.max'          => trans('auth.PasswordMax'),
        'role.required'         => trans('auth.roleRequired'),
      ]
    );

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $ipAddress = new CaptureIpTrait();
    $profile = new Profile();
    $generator   = App::make('GenerateRegNumber');
    $date = new \DateTime($request->input('data_de_nascimento'));
    $dob =  $date->format('Y-m-d');

    $indexRoute = 'usuarios';
    switch($request->input('role')){
      case $this->getRoleId('Admin'):
        $type = "Admin";
        $role = $request->input('role');
        $indexRoute = 'administradores';
        $user = Admin::create([
          'primeiro_nome' => $request->input('primeiro_nome'),
          'sobrenome' => $request->input('sobrenome'),
          'sexo'=> $request->input('sexo'),
        ]);
        break;
      case $this->getRoleId('Estudante'):
        $type = "Estudante";
        $role = $request->input('role');
        $reg_no = $generator->generateStudentReg();
        $user = Student::create($this->createUserDetailsW(
          $request->input('primeiro_nome'),
          $request->input('sobrenome'),
          $request->input('sexo'),
          $dob,
          $request->input('telefone'),
          $reg_no
        ));
        break;
      case $this->getRoleId('Professor'):
        $type = "Professor";
        $role = $request->input('role');
        $indexRoute = 'professores';
        $reg_no = $generator->generateTeacherReg();
        $user = Teacher::create($this->createUserDetailsW(
          $request->input('primeiro_nome'),
          $request->input('sobrenome'),
          $request->input('sexo'),
          $dob,
          $request->input('telefone'),
          $reg_no
        ));
        break;
      default:
        $type = "Encarregado";
        $role = $request->input('role');
        $indexRoute = 'encarregados';
        $user = Guardian::create($this->createUserDetails(
          $request->input('primeiro_nome'),
          $request->input('sobrenome'),
          $request->input('sexo'),
          $dob,
          $request->input('telefone')
        ));
        break;
    }

    $user = User::create([
      'nome' => $user->nome,
      'email'   => $request->input('email'),
      'userable_id'   =>$user->id,
      'userable_type' => $type,
      'password'      => bcrypt($request->input('password')),
      'token'            => str_random(64),
      'activated'        => false,
      'admin_ip_address' => $ipAddress->getClientIp(),
    ]);

    $user->profile()->save($profile);
    $user->attachRole($role);
    $user->save();

    return redirect(route($indexRoute))
           ->with('success', trans('usersmanagement.createSuccess'));
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
      $user = User::findOrFail($id);
      $type = $this->getType($request);
      switch ($type) {
        case 'professores':
          $title = 'Showing User' . $user->nome ;
          $createTitle = trans('usersmanagement.createTeacher');
          $indexRoute = 'professores';
          $createRoute = 'professores.create';
          $showRoute   = 'professor.show';
          $editRoute   =  'professor.edit';
          $deleteRoute =  'professor.destroy';
          $users = $this->getUsers('Professor');
          break;
        case 'encarregados':
          $title = trans('usersmanagement.showAllGuardians');
          $createTitle = trans('usersmanagement.createGuardian');
          $indexRoute = 'encarregados';
          $createRoute = 'encarregados.create';
          $showRoute   = 'encarregado.show';
          $editRoute   =  'encarregado.edit';
          $deleteRoute =  'encarregado.destroy';
          $users = $this->getUsers('Encarregado');
          break;
        default:
          $title = trans('usersmanagement.showAllUsers');
          $createTitle = trans('usersmanagement.createUser');
          $indexRoute = 'usuarios';
          $createRoute = 'usuarios.create';
          $showRoute   = 'usuario.show';
          $editRoute   =  'usuario.edit';
          $deleteRoute =  'usuario.destroy';
          $users = User::latest()->get();
          break;
      }

      $data = [
        'title' => $title,
        'createTitle' => $createTitle,
        'indexRoute' => $indexRoute,
        'createRoute' => $createRoute,
        'showRoute' => $showRoute,
        'editRoute'   => $editRoute,
        'deleteRoute' => $deleteRoute,
        'user' => $user,
      ];
      return view('management.users.show', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $roles = Role::latest()->get();
    $type = $this->getType($request);;
    switch ($type) {
      case 'professores':
        $title = 'Showing User' . $user->nome ;
        $createTitle = trans('usersmanagement.createTeacher');
        $indexRoute = 'professores';
        $createRoute = 'professores.create';
        $showRoute   = 'professor.show';
        $editRoute   =  'professor.edit';
        $deleteRoute =  'professor.destroy';
        $users = $this->getUsers('Professor');
        break;
      case 'encarregados':
        $title = trans('usersmanagement.showAllGuardians');
        $createTitle = trans('usersmanagement.createGuardian');
        $indexRoute = 'encarregados';
        $createRoute = 'encarregados.create';
        $showRoute   = 'encarregado.show';
        $editRoute   =  'encarregado.edit';
        $deleteRoute =  'encarregado.destroy';
        $users = $this->getUsers('Encarregado');
        break;
      default:
        $title = trans('usersmanagement.showAllUsers');
        $createTitle = trans('usersmanagement.createUser');
        $indexRoute = 'usuarios';
        $createRoute = 'usuarios.create';
        $showRoute   = 'usuario.show';
        $editRoute   =  'usuario.edit';
        $deleteRoute =  'usuario.destroy';
        $users = User::latest()->get();
        break;
    }

    foreach ($user->roles as $user_role) {
      $currentRole = $user_role;
    }

    $data = [
      'title' => $title,
      'createTitle' => $createTitle,
      'indexRoute' => $indexRoute,
      'createRoute' => $createRoute,
      'showRoute' => $showRoute,
      'editRoute'   => $editRoute,
      'deleteRoute' => $deleteRoute,
      'user' => $user,
      'currentRole'   => $currentRole,
      'roles'         => $roles,
    ];

    return view('management.users.edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int                      $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $currentUser = Auth::user();
    $user = User::find($id);
    $emailCheck = ($request->input('email') != '')
    && ($request->input('email') != $user->email);
    $ipAddress = new CaptureIpTrait();

    if ($emailCheck) {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'email'                 => 'required|email|max:255|unique:users',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'present|min:6|max:20|confirmed',
          'password_confirmation' => 'present|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'email.required'        => trans('auth.emailRequired'),
          'email.email'           => trans('auth.emailInvalid'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    } else {
      $validator = Validator::make($request->all(),
        [
          'primeiro_nome'         => 'required',
          'sobrenome'             => 'required',
          'data_de_nascimento'    => 'required|date|date_format:Y-m-d',
          'password'              => 'nullable|min:6|max:20|confirmed',
          'password_confirmation' => 'nullable|same:password',
          'role'                  => 'required',
        ],
        [
          'primeiro_nome.required'=> trans('auth.fNameRequired'),
          'sobrenome.required'    => trans('auth.lNameRequired'),
          'password.required'     => trans('auth.passwordRequired'),
          'password.min'          => trans('auth.PasswordMin'),
          'password.max'          => trans('auth.PasswordMax'),
          'role.required'         => trans('auth.roleRequired'),
        ]
      );
    }

    if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
    }

    $user->nome = $request->input('nome');
    $user->userable->primeiro_nome = $request->input('primeiro_nome');
    $user->userable->sobrenome = $request->input('sobrenome');
    if ($emailCheck) {
        $user->email = $request->input('email');
    }

    if ($request->input('password') != null) {
      $user->password = bcrypt($request->input('password'));
    }

    $user->detachAllRoles();
    $user->attachRole($request->input('role'));
    $user->updated_ip_address = $ipAddress->getClientIp();
    $user->userable->save();
    $user->save();

    return back()->with('success', trans('usersmanagement.updateSuccess'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $currentUser = Auth::user();
    $user = User::findOrFail($id);
    $ipAddress = new CaptureIpTrait();

    if ($user->id != $currentUser->id) {
        $user->deleted_ip_address = $ipAddress->getClientIp();
        $user->save();
        $user->delete();

        $type = $this->getType($request);
        switch ($type) {
          case 'professores':
            $indexRoute = 'professores';
            break;
          case 'encarregados':
            $indexRoute = 'encarregados';
            break;
          default:
            $indexRoute = 'usuarios';
            break;
        }

        return redirect(route($indexRoute))->with('success',
         trans('usersmanagement.deleteSuccess'));
    }

    return back()->with('error', trans('usersmanagement.deleteSelfError'));
  }

  public function getRole($role) {
    return Role::whereName($role)->first();
  }

  public function getRoleId($role) {
    return $this->getRole($role)->id;
  }

  public function getUsers($type) {
    return User::where('userable_type', $type)->latest()->get();
  }

  public function getType(Request $request) {
    return explode('/', $this->replaceLanguage($request))[0];
  }

  public function replaceLanguage(Request $request) {
    $url = $request->route()->uri;
    $type = str_replace('en/', '', $url);
    $type = str_replace('pt/', '', $type);
    return $type;
  }

  public function createUserDetails(String $primeiro_nome,
                                    String $sobrenome,
                                    String $sexo,
                                    String $dob,
                                    String $telefone) {
    return [
      'primeiro_nome' => $primeiro_nome,
      'sobrenome' => $sobrenome,
      'sexo'=> $sexo,
      'data_de_nascimento' => $dob,
      'telefone' => $telefone
    ];
  }

  public function createUserDetailsW(String $primeiro_nome,
                                     String $sobrenome,
                                     String $sexo,
                                     String $dob,
                                     String $telefone,
                                     String $nr_de_reg) {
    $createUserDetails = $this->createUserDetails($primeiro_nome,
                                                  $sobrenome,
                                                  $sexo,
                                                  $dob,
                                                  $telefone,
                                                  $nr_de_reg);
    $createUserDetails['nr_de_reg'] = $nr_de_reg;
    return $createUserDetails;
  }
}

<?php

namespace App\Http\Controllers;
use App\Models\Event;
use App\Models\Testimonial;
use App\Models\Subject;
use App\Models\Gallery;
use App\Models\News;
use App\Models\SiteInfo;
use App\Models\Teacher;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Mail\MailContacto;
use Auth;
use Mail;
use Validator;

class WelcomeController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function welcome()
  {
    $bodyClass = 'home-page';
    $siteinfo = SiteInfo::latest()->first();
    $noticias = News::latest()->take(4)->get();
    $disciplinas = Subject::latest()->take(10)->get();
    $eventos =  Event::latest()->take(4)->get();
    $testemunhos = Testimonial::with('user')->take(4)->get();
    $data = [
      'bodyClass' => $bodyClass,
      'siteinfo' => $siteinfo,
      'noticias' => $noticias,
      'eventos' => $eventos,
      'testemunhos' => $testemunhos,
      'disciplinas' => $disciplinas

    ];
    return view('website.pages.welcome', $data);
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function eventos()
  {
    $eventos = Event::paginate(10);
    $noticias = News::paginate(4);
    return view('website.pages.events', compact('eventos','noticias'));
  }

  public function about()
  {
    $siteinfo = SiteInfo::latest()->first();
    return view('website.pages.about', compact('siteinfo'));
  }

  public function terms()
  {
    return view('website.pages.terms');
  }

  public function contactar()
  {
    return view('website.pages.contactar');
  }

  public function postContactar(Request $request)
  {
    $validator = Validator::make($request->all(),
      [
        'nome'       => 'required',
        'email'      => 'required|email|max:255',
        'telefone'   => 'required',
        'mensagem'    => 'required|min:15',
      ]
    );

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    Contact::create($request->all());
    Mail::to($request->input('email'))->send(new MailContacto);
    return back()->with('success', trans('app.contactCreateSuccess'));
  }


  public function gallery()
  {
    $images = Gallery::paginate(12);
    return view('website.pages.gallery', compact('images'));
  }

  public function teachers()
  {
    $teachers = Teacher::paginate(12);
    return view('website.pages.teachers', compact('teachers'));
  }

  public function informacoes()
  {
    $teachers = Teacher::paginate(12);
    return view('website.pages.informacoes', compact('teachers'));
  }

  public function documentosMatricula()
  {
    $noticias = News::paginate(4);
    return view('website.pages.registration_documents',  compact('noticias'));
  }

  public function tabelaDePrecos()
  {
    $noticias = News::paginate(4);
    return view('website.pages.prices_table', compact('noticias'));
  }
}

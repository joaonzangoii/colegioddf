<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
  protected $table = "administradores";
 
  protected $fillable = [
    'nome', 
    'primeiro_nome', 
    'sobrenome', 
    'sexo', 
    'telefone',
  ];

  public function user()
  {
    return $this->morphOne('App\Models\User', 'userable');
  }

  public function getNomeAttribute()
  {
    return $this->primeiro_nome . " " . $this->sobrenome;
  } 
}

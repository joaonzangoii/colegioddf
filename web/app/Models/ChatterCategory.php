<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatterCategory extends Model
{
  protected $table = "chatter_categories";
  protected $fillable = [
    'id',
    'parent_id',
    'order',
    'name',
    'color',
    'slug',
  ];
}

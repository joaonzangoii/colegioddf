<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatterDiscussion extends Model
{
  protected $table = "chatter_discussion";
  protected $fillable = [
    'id',
    'chatter_category_id',
    'title',
    'user_id',
    'sticky',
    'views',
    'answered',
    'slug',
    'color'
  ];
}

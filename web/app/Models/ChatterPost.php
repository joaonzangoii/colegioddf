<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatterPost extends Model
{
  protected $table = "chatter_post";
  protected $fillable = [
    'id',
    'chatter_discussion_id',
    'user_id',
    'body',
  ];
}

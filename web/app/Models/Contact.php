<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  public $table = 'contactos';

  public $fillable = ['nome', 'telefone', 'email','mensagem'];

}

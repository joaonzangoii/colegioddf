<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $table = "eventos";
  protected $dates = ['data'];
  protected $fillable = [
    'imagem',
    'nome',
    'descricao',
    'hora_de_comeco',
    'hora_de_termino',
    'lugar',
    'data',
    'usuario_id'
  ];

  public function user()
  {
    return $this->belongsTo('App\Models\User', 'usuario_id');
  }
}

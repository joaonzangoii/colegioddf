<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Gallery extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'imagens';

  protected $fillable = ['titulo',
                         'responsavel_id',
                         'imagem',
                         'created_at',
                         'updated_at'];

  /*
   * Some Validation rules for this model's new instance.
   */
  public static $validationRules = array(
    'titulo' => 'required|string',
    'imagem' => 'required|image'
  );
  /*
   * Some Edit Validation rules for this model's new instance.
   */
  public static $editValidationRules = array(
    'titulo' => 'required|string',
    'responsavel_id' => 'required'
  );

  protected static function boot()
  {
    parent::boot();
    static::deleting(function($deleted) {
      Storage::delete("gallery/{$deleted->iagem}");
    });
  }

  protected static function getImagemAttribute($imagem){
    return '/images/gallery/' . $imagem;
  }
}

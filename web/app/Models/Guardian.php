<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guardian extends Model
{
  protected $table = "encarregados";

  protected $fillable = [
    'primeiro_nome',
    'sobrenome',
    'data_de_nascimento',
    'sexo',
    'telefone',
    'endereco',
    'foto_de_perfil',
  ];

  public function user()
  {
      return $this->morphOne('App\Models\User', 'userable');
  }

  public function estudantes()
  {
    return $this->hasMany('App\Models\Student', 'encarregado_id');
  }

  public function getNomeAttribute()
  {
    return $this->primeiro_nome . " " . $this->sobrenome;
  }
}

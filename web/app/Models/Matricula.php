<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
  protected $table = "matriculas";

  protected $fillable = [
    'estudante_id',
    'responsavel_id',
    'curso_id',
    'classe_id',
    'situacao_da_matricula',
    'disciplinas_em_curso',
    'disciplinas_concluidas',
    'ano_lectivo',
    'valor_da_matricula',
    'propina_mensal',
    'total_da_propina',
  ];

  public function estudante()
  {
    return $this->hasMany('App\Models\Student', 'estudante_id');
  }

  public function disciplinas_cursadas()
  {
    return $this->hasMany('App\Models\StudentSubjectTaken');
  }

  public function setPropinaMensalAttribute ($propina_mensal)
  {
    $this->attributes['propina_mensal']   = $propina_mensal;
    $this->attributes['total_da_propina'] = $propina_mensal * 12;
  }
}

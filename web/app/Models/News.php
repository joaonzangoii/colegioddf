<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Noticia;

class News extends Model
{
  protected $fillable = [
    'imagem',
    'titulo',
    'descricao',
    'usuario_id'
  ];

  public function usuario()
  {
    return $this->belongsTo(User::class);
  }

  protected static function getImagemAttribute($imagem){
    return '/images/news/' . $imagem;
  }
}

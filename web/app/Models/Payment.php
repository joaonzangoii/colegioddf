<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Payment extends Model
{
  protected $table = 'pagamentos';
  protected $fillable = [
    'mes',
    'valor',
    'aprovado',
    'comprovativo',
    'trimestre_id',
    'estudante_id',
    'matricula_id',
  ];

  /**
  * The attributes that should be cast to native types.
  *
  * @var array
  */
  protected $casts = [
    'aprovado' => 'boolean',
  ];

  public function estudante()
  {
    return $this->belongsTo('App\Models\Student', 'estudante_id');
  }

  public function matricula()
  {
    return $this->belongsTo('App\Models\Matricula', 'matricula_id');
  }

  public function trimestre()
  {
    return $this->belongsTo('App\Models\Term', 'trimestre_id');
  }

  public function getMesAttribute($mes){
    $array = trans('months.months');
    $key = sprintf('%02d', $mes);
    if (!array_key_exists($key, $array)){
      return "Invalid";
    }
    
    return $array[$key];
  }
}

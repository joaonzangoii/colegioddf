<?php

namespace App\Models;

use jeremykenedy\LaravelRoles\Models\Permission as kPermission;

class Permission extends kPermission
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';
}

<?php

namespace App\Models;

use jeremykenedy\LaravelRoles\Models\Role as kRole;

class Role extends kRole
{
  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'roles';

  protected $fillable = [
    'name',
    'slug',
    'descricao',
    'level'
  ];

}

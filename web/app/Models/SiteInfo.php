<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteInfo extends Model
{
  protected $table = 'informacoes';
  protected $fillable = [
    'nome',
    'email',
    'telefone',
    'fax',
    'endereco',
    'coordenadas',
    'descricao',
    'acerca',
    'foto_de_capa_acerca',
    'termos_e_condicoes',
    'logo_cabecalho',
    'logo_rodape',
    'usuario_id',
  ];
}

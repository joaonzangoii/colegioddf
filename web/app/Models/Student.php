<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $table = 'estudantes';

  protected $fillable = [
    'primeiro_nome',
    'sobrenome',
    'sexo',
    'nr_de_reg',
    'data_de_nascimento',
    'telefone',
    'endereco',
    'bilhete_de_identidade',
    'profissao',
    'estado_civil',
    'encarregado_id',
    'professor_id'
  ];

  public function user()
  {
    return $this->morphOne('App\Models\User', 'userable');
  }

  public function results()
  {
    return $this->hasMany('App\Models\Result');
  }

  public function encarregado()
  {
    return $this->belongsTo('App\Models\Guardian', 'encarregado_id');
  }

  public function matriculas()
  {
    return $this->hasMany('App\Models\Matricula', 'estudante_id');
  }

  public function disciplinas_cursadas()
  {
    return $this->hasMany('App\Models\StudentSubjectTaken', 'estudante_id');
  }

  public function getFullNomeAttribute()
  {
    return $this->primeiro_nome . " " . $this->sobrenome;
  }

  public function getNomeAttribute()
  {
    return $this->primeiro_nome . " " . $this->sobrenome;
  }
}

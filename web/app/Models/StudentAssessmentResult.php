<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAssessmentResult extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'resultados';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
    'estudante_id',
    'disciplina_cursada_id',
    'classe_id',
    'trimestre_id',
    'avaliacao_id',
    'professor_id',
    'matricula_id',
    'nota',
  ];

  public function avaliacao()
  {
    return $this->belongsTo('App\Models\SubjectAssessment');
  }

  public function trimestre()
  {
    return $this->belongsTo('App\Models\Term');
  }

  public function disciplina_cursada()
  {
    return $this->belongsTo('App\Models\StudentSubjectTaken', 'disciplina_cursada_id');
  }

  public function professor()
  {
    return $this->belongsTo('App\Models\Teacher', 'professor_id');
  }

  public function matricula()
  {
    return $this->belongsTo('App\Models\Matricula', 'matricula_id');
  }

  public function estudante()
  {
    return $this->belongsTo('App\Models\Student', 'estudante_id');
  }

  public static function processarResultado($result)
  {
    $result = (int)$result;

    // if ($result >= 60 && $result < 70)
    //     return "B";
    //
    // if ($result >= 50 && $result < 60)
    //     return "C";
    //
    // if ($result >= 40 && $result < 50)
    //     return "D";
    //
    // if ($result >= 30 && $result < 40)
    //     return "E";
    //
    // if ($result < 30)
    //     return "F";
    // else
    //     return "A";
    return $result;
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
  protected $table = "classes";

  protected $fillable = [
    'nome', 
  ];

  public function getNomeAttribute($nome)
  {
    return $nome == 0 ? "Pre" : $nome;
  } 
}

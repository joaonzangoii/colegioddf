<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentSubjectTaken extends Model
{
  protected $table = "disciplinas_cursadas";

  protected $fillable = [
    'estudante_id',
    'matricula_id',
    'disciplina_id',
    'periodo_id',
    'frequencia',
    'nota_global',
    'situacao'
  ];

  public function estudante()
  {
    return $this->hasMany('App\Models\Student', 'estudante_id');
  }

  public function matricula()
  {
    return $this->hasMany('App\Models\Matricula', 'id');
  }

  public function disciplina()
  {
    return $this->belongsTo('App\Models\Subject');
  }

  public function resultados()
  {
    return $this->hasMany('App\Models\StudentAssessmentResult' , 'disciplina_cursada_id');
  }

  // public function getNotaGlobalAttribute()
  // {
  //   $notasTotal = 0;
  //   foreach ($this->resultados as $key => $resultado) {
  //     $notasTotal += $resultado->nota;
  //   }
  //   if($notasTotal == 0 ){
  //     return 0;
  //   }
  //
  //   return $notasTotal / count($this->resultados());
  // }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
  protected $table = "disciplinas";

  protected $fillable = [
    'nome',
    'descricao',
  ];
}

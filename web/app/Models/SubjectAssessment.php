<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectAssessment extends Model
{
  protected $table = "avaliacoes";

  protected $fillable = [
    'nome',
    'codigo',
    'slug',
  ];

  public function setCodigoAttribute ($codigo)
  {
    $this->attributes['codigo'] = $codigo;
    $this->attributes['slug'] = str_slug($codigo);
  }

  public function resultados()
  {
    return $this->hasMany('App\Models\StudentAssessmentResult', 'avaliacao_id');
  }
}

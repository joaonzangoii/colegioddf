<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
  protected $table = "professores";

  protected $fillable = [
    'primeiro_nome', 
    'sobrenome', 
    'sexo', 
    'nr_de_reg', 
    'data_de_nascimento', 
    'telefone', 
    'endereco', 
  ];

  public function user()
  {
    return $this->morphOne('App\Models\User', 'userable');
  }

  public function getNomeAttribute()
  {
    return $this->primeiro_nome . " " . $this->sobrenome;
  } 
}

<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trimestres';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'codigo'];

    public function resultados()
    {
     return $this->hasMany('App\Models\StudentAssessmentResult' , 'trimestre_id');
    }  

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Testimonial extends Model
{
  protected $table = "testemunhos";
  protected $fillable = [
    'mensagem',
    'usuario_id'
  ];

  public function user()
  {
    return $this->belongsTo('App\Models\User', 'usuario_id');
  }
}

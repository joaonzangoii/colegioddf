<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
  use HasRoleAndPermission;
  use Notifiable;
  use SoftDeletes;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  protected $fillable = [
    'nome',
    'username',
    'email',
    'password',
    'userable_id',
    'userable_type',
    'remember_token',
    'token',
    'activated',
    'signup_ip_address',
    'signup_confirmation_ip_address',
    'signup_sm_ip_address',
    'admin_ip_address',
    'updated_ip_address',
    'deleted_ip_address'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
    'activated',
    'token',
  ];

  protected $dates = [
    'deleted_at',
  ];

  /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'activated' => 'boolean',
    ];

  /**
   * Build Social Relationships.
   *
   * @var array
   */
  public function social()
  {
    return $this->hasMany('App\Models\Social');
  }

  /**
   * User Profile Relationships.
   *
   * @var array
   */
  public function profile()
  {
    return $this->hasOne('App\Models\Profile', 'usuario_id');
  }

  public function profiles()
  {
    return $this->belongsToMany('App\Models\Profile')
                ->withTimestamps();
  }

  public function hasProfile($name)
  {
    foreach ($this->profiles as $profile) {
        if ($profile->name == $name) {
            return true;
        }
    }

    return false;
  }

  public function assignProfile($profile)
  {
    return $this->profiles()->attach($profile);
  }

  public function removeProfile($profile)
  {
    return $this->profiles()->detach($profile);
  }

  public function userable()
  {
    return $this->morphTo();
  }

  public function admin()
  {
    return $this->roles->first()->name == "Admin";
  }

  public function sendPasswordResetNotification($token)
  {
      $this->notify(new ResetPasswordNotification($token));
  }
}

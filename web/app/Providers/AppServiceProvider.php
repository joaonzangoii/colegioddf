<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Services\GenerateReg;
use Auth;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    Schema::defaultStringLength(191);
    Relation::morphMap([
      'Admin' => \App\Models\Admin::class,
      'Estudante' => \App\Models\Student::class,
      'Professor' => \App\Models\Teacher::class,
      'Encarregado' => \App\Models\Guardian::class,
    ]);
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton('GenerateRegNumber', function ($app) {
      return new GenerateReg();
    });
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nome');
      $table->string('username')->unique();
      $table->string('password', 60)->nullable();
      $table->integer('userable_id');
      $table->string('userable_type');
      $table->unique(['userable_id', 'userable_type']);
      $table->string('token');
      $table->string('email')->unique()->nullable();
      $table->boolean('activated')->default(false);
      $table->ipAddress('signup_ip_address')->nullable();
      $table->ipAddress('signup_confirmation_ip_address')->nullable();
      $table->ipAddress('signup_sm_ip_address')->nullable();
      $table->ipAddress('admin_ip_address')->nullable();
      $table->ipAddress('updated_ip_address')->nullable();
      $table->ipAddress('deleted_ip_address')->nullable();
      $table->rememberToken();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('users');
  }
}

   

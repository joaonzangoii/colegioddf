<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('professores', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('primeiro_nome')->nullable();
      $table->string('sobrenome')->nullable();
			$table->date('data_de_nascimento')->nullable();
			$table->string('sexo', 10);
			$table->string('telefone', 25);
			$table->string('endereco')->nullable();
      $table->string('foto_de_perfil')->default('default.jpg');
			$table->string('nr_de_reg', 9)->unique();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('professores');
	}

}

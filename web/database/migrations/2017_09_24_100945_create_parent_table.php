<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentTable extends Migration {

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('encarregados', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('primeiro_nome');
      $table->string('sobrenome');
			$table->date('data_de_nascimento')->nullable();
			$table->string('sexo', 10);
      $table->string('telefone', 50);
			$table->string('endereco')->nullable();
      $table->string('foto_de_perfil')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('encarregados');
	}

}

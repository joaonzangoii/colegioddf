<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estudantes', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('primeiro_nome')->nullable();
      $table->string('sobrenome')->nullable();
      $table->string('sexo', 10);
      $table->string('bilhete_de_identidade');
      $table->string('nr_de_reg', 9)->unique();
      $table->date('data_de_nascimento');
      $table->string('telefone', 25);
      $table->string('foto_de_perfil')->default('default.jpg');
      $table->string('endereco')->nullable();
      $table->string('profissao');
      $table->string('estado_civil');
      $table->integer('encarregado_id')->unsigned()->index();
      $table->foreign('encarregado_id')->references('id')
            ->on('encarregados')
            ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estudantes');
	}

}

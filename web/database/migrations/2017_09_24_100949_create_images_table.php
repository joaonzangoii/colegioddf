<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imagens', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('titulo', 60);
      $table->string('imagem');
      $table->integer('responsavel_id')->unsigned()->index();
      $table->foreign('responsavel_id')->references('id')
            ->on('administradores')
            ->onUpdate('cascade')
            ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imagens');
	}

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('testemunhos', function (Blueprint $table) {
      $table->increments('id');
      $table->text('mensagem');
      $table->integer('usuario_id')->unsigned()->index();
      $table->foreign('usuario_id')->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('testemunhos');
  }
}

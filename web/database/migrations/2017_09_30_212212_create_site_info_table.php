<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('informacoes', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nome')->nullable();
        $table->string('email')->nullable();
        $table->string('telefone')->nullable();
        $table->string('fax')->nullable();
        $table->text('endereco')->nullable();
        $table->string('coordenadas')->nullable();
        $table->text('descricao')->nullable();
        $table->string('logo_cabecalho')->nullable();
        $table->string('logo_rodape')->nullable();
        $table->text('acerca')->nullable();
        $table->text('foto_de_capa_acerca')->nullable();
        $table->text('termos_e_condicoes')->nullable();
        $table->integer('usuario_id')->unsigned()->index();
        $table->foreign('usuario_id')->references('id')
              ->on('users')
              ->onUpdate('cascade')
              ->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('informacoes');
    }
}

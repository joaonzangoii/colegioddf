<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatriculasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('matriculas', function (Blueprint $table) {
      $table->increments('id');
      $table->double('valor_da_matricula');
      $table->double('propina_mensal');
      $table->double('total_da_propina');
      $table->text('numero_de_matricula');
      $table->text('numero_de_boletim');
      $table->integer('ano_lectivo')->default(2017);
      $table->enum('situacao_da_matricula', ["criada", 
                                              "cancelada", 
                                              "concluida",
                                              "reprovada",
                                              "aprovada"])
            ->default("criada");
      $table->integer('disciplinas_em_curso')->default(0);
      $table->integer('disciplinas_concluidas')->default(0);
      $table->integer('estudante_id')->unsigned()->index();
      $table->foreign('estudante_id')->references('id')
            ->on('estudantes')
            ->onUpdate('cascade')
            ->onDelete('cascade');
      $table->integer('responsavel_id')->unsigned()->index();
      $table->foreign('responsavel_id')->references('id')
            ->on('administradores')
            ->onUpdate('cascade')
            ->onDelete('cascade');
      $table->integer('curso_id')->unsigned()->index();
      $table->foreign('curso_id')->references('id')
            ->on('cursos')
            ->onUpdate('cascade')
            ->onDelete('cascade');
      $table->integer('classe_id')->unsigned()->index();
      $table->foreign('classe_id')->references('id')
            ->on('classes')
            ->onUpdate('cascade')
            ->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('matriculas');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTakensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('disciplinas_cursadas', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('estudante_id')->unsigned()->index();
        $table->foreign('estudante_id')->references('id')
              ->on('estudantes')
              ->onUpdate('cascade')
              ->onDelete('cascade');
        $table->integer('disciplina_id')->unsigned()->index();
        $table->foreign('disciplina_id')->references('id')
              ->on('disciplinas')
              ->onUpdate('cascade')
              ->onDelete('cascade');
        $table->integer('matricula_id')->unsigned()->index();
        $table->foreign('matricula_id')->references('id')
              ->on('matriculas')
              ->onUpdate('cascade')
              ->onDelete('cascade');
        $table->integer('frequencia')->default(0);
        $table->double('nota_global')->default(0);
        $table->string('situacao')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplinas_cursadas');
    }
}

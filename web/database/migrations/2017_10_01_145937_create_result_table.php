<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTable extends Migration {

	/**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('resultados', function(Blueprint $table)
    {
      $table->increments('id');
      $table->float('nota');
      $table->integer('estudante_id')->unsigned()->index();
      $table->foreign('estudante_id')
             ->references('id')
            ->on('estudantes')
            ->onDelete('cascade');
      $table->integer('disciplina_cursada_id')->unsigned()->index();
      $table->foreign('disciplina_cursada_id')
             ->references('id')
            ->on('disciplinas_cursadas')
            ->onDelete('cascade');
      $table->integer('classe_id')->unsigned()->index();
      $table->foreign('classe_id')
             ->references('id')
            ->on('classes')
            ->onDelete('cascade');
      $table->integer('trimestre_id')->unsigned()->index();
      $table->foreign('trimestre_id')
             ->references('id')
            ->on('trimestres')
            ->onDelete('cascade');
      $table->integer('avaliacao_id')->unsigned()->index();
      $table->foreign('avaliacao_id')
             ->references('id')
            ->on('avaliacoes')
            ->onDelete('cascade');
      $table->integer('matricula_id')->unsigned()->index();
      $table->foreign('matricula_id')
             ->references('id')
            ->on('matriculas')
            ->onDelete('cascade');
      $table->integer('professor_id')->unsigned()->index();
      $table->foreign('professor_id')
             ->references('id')
            ->on('professores')
            ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resultados');
	}

}

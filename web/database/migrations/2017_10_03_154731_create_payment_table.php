<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagamentos', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('mes');
      $table->float('valor');
			$table->boolean('aprovado')->default(false);
			$table->text('comprovativo');
      $table->integer('trimestre_id')->unsigned()->index();
      $table->foreign('trimestre_id')
             ->references('id')
            ->on('trimestres')
            ->onDelete('cascade');
      $table->integer('estudante_id')->unsigned()->index();
      $table->foreign('estudante_id')
             ->references('id')
            ->on('estudantes')
            ->onDelete('cascade');
      $table->integer('matricula_id')->unsigned()->index();
      $table->foreign('matricula_id')
             ->references('id')
            ->on('matriculas')
            ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagamentos');
	}
}

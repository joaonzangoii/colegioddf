<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialNetworkLinksToSiteInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('informacoes', function (Blueprint $table) {
        $table->string('facebook_link')->nullable();
        $table->string('twitter_link')->nullable();
        $table->string('linkedin_link')->nullable();
        $table->string('google_plus_link')->nullable();
        $table->string('youtube_link')->nullable();
        $table->string('instagram_link')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('informacoes', function($table)
      {
          $table->dropColumn(['facebook_link',
                              'twitter_link',
                              'linkedin_link',
                              'google_plus_link',
                              'youtube_link',
                              'instagram_link']);
      });
    }
}

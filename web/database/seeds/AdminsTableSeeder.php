<?php

use Illuminate\Database\seeder;
use App\Models\Admin;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;

class AdminsTableSeeder extends Seeder {
  public function run()
  {
    Admin::truncate();
    $faker = Faker\Factory::create();
    $profile = new Profile();
    $seededAdminEmail = 'admin@admin.com';
    $user = User::where('email', '=', $seededAdminEmail)->first();
    $adminRole = Role::whereName('Admin')->first();

    if ($user === null) {
      $admin = Admin::create([
        'primeiro_nome' => "Administrador",
        'sobrenome' => 'Sistema',
        'sexo'=> 'masculino',
        'telefone' => $faker->phoneNumber
      ]);

      $user = User::create([
        'nome' => $admin->nome,
        'email' => $seededAdminEmail,
        'userable_id'=> $admin->id,
        'userable_type'=>"Admin",
        'password' => bcrypt('@colegioddf123'),
        'token'                          => str_random(64),
        'activated'                      => true,
        'signup_confirmation_ip_address' => $faker->ipv4,
        'admin_ip_address'               => $faker->ipv4,
      ]);

      $user->profile()->save(new Profile());
      $user->attachRole($adminRole);
      $user->save();
    }
  }
}

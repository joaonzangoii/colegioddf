<?php

use Illuminate\Database\seeder;
use App\Models\Curso;

class CursosTableSeeder extends Seeder {
  public function run()
  {
    Curso::truncate();
    Curso::create([
      'nome' => 'Ensino Geral',
      'descricao' =>"Da Iniciação à 9 classe"
    ]); 

    Curso::create([
      'nome' => 'Ciencias Humanas',
      'descricao' =>"Humanas"
    ]); 

    Curso::create([
      'nome' => 'Ciencias Fisicas e Biologicas',
      'descricao' =>"Fisicas e Biologicas"
    ]);

    Curso::create([
      'nome' => 'Técnico de Informática',
      'descricao' =>"Informática"
    ]);
  }
}

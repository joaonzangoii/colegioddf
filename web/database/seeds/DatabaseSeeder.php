<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 0;");
        if(env('APP_URL')=== 'http://localhost'){
          $this->call(PermissionsTableSeeder::class);
          $this->call(RolesTableSeeder::class);
          $this->call(ConnectRelationshipsSeeder::class);
          $this->call(ThemesTableSeeder::class);
          $this->call(CursosTableSeeder::class);
          $this->call(TermsTableSeeder::class);
          $this->call(StudentClassesTableSeeder::class);
          $this->call(SubjectsTableSeeder::class);
          $this->call(AdminsTableSeeder::class);
          $this->call(SiteInfoTableSeeder::class);
          $this->call(SubjectsAssessmentsTableSeeder::class);
          $this->call(TeachersTableSeeder::class);
          $this->call(GuardiansTableSeeder::class);
          $this->call(StudentsTableSeeder::class);
          $this->call(EventsTableSeeder::class);
          $this->call(TestimonialsTableSeeder::class);
          $this->call(ChatterTableSeeder::class);
          $this->call(NewsTableSeeder::class);
          $this->call(GalleryTableSeeder::class);
        }
        DB::statement("SET FOREIGN_KEY_CHECKS = 1;");
        Model::reguard();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Event;
use App\Models\User;

class EventsTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
      date_default_timezone_set('Africa/Luanda');
      Event::truncate();
      $user = User::all()->random();
      $evento = Event::create([
        "nome" => "Jornadas Empresariais",
        "descricao" => "Jornadas Empresariais",
        "hora_de_comeco"  => '12:00:00',
        "hora_de_termino" => '16:00:00',
        "lugar" => "Colegio DDF",
        "data" => "2017-11-05",
        "usuario_id" => $user->id
      ]);

      $evento = Event::create([
        "nome" => "Tarde Cultural",
        "descricao" => "Dia da cultura",
        "hora_de_comeco"  => '12:00:00',
        "hora_de_termino" => '18:00:00',
        "lugar" => "Colegio DDF",
        "data" => "2017-11-08",
        "usuario_id" => $user->id
      ]);

      $evento = Event::create([
        "nome" => "Palestra",
        "descricao" => "Palestra sobre Ensino Superior",
        "hora_de_comeco"  => '12:00:00',
        "hora_de_termino" => '14:00:00',
        "lugar" => "UPRA",
        "data" => "2017-11-08",
        "usuario_id" => $user->id
      ]);

      $evento = Event::create([
        "nome" => "Palestra",
        "descricao" => "Palestra sobre o Pais",
        "hora_de_comeco"  => '12:00:00',
        "hora_de_termino" => '15:00:00',
        "lugar" => "Universidade Agostinho Neto",
        "data" => "2017-10-17",
        "usuario_id" => $user->id
      ]);

      $evento = Event::create([
        "nome" => "Partida de Futebol",
        "descricao" => "Partida de Futebol",
        "hora_de_comeco"  => '12:00:00',
        "hora_de_termino" => '15:00:00',
        "lugar" => "Estadio do Tafi",
        "data" =>"2017-10-31",
        "usuario_id" => $user->id
      ]);

      $evento = Event::create([
        "nome" => "Baile dos Finalistas",
        "descricao" => "gala para celebrar os alunos finalistas da instituição",
        "hora_de_comeco"  => '18:00:00',
        "hora_de_termino" => '24:00:00',
        "lugar" => "Chiloango",
        "data" => "2017-11-30",
        "usuario_id" => $user->id
      ]);
    }
}

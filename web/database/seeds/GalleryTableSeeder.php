<?php

use Illuminate\Database\Seeder;
use App\Models\Gallery;

class GalleryTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    Gallery::truncate();
    $imagem = Gallery::create([
      "imagem" => "1507715987.DSCF0200.JPG",
      "titulo" => "Titulo",
    ]);

    $imagem = Gallery::create([
      "imagem" => "1507623235.DSCF0248.JPG",
      "titulo" => "Titulo",
    ]);

    $imagem = Gallery::create([
      "imagem" => "1507624180.DSCF0252.JPG",
      "titulo" => "Titulo",
    ]);

    $imagem = Gallery::create([
      "imagem" => "1507715901.DSCF0259.JPG",
      "titulo" => "Titulo",
    ]);

    $imagem = Gallery::create([
      "imagem" => "1507715910.DSCF0250.JPG",
      "titulo" => "Titulo",
    ]);

    $imagem = Gallery::create([
      "imagem" => "1507715920.DSCF0243.JPG",
      "titulo" => "Titulo",
    ]); 

    $imagem = Gallery::create([
      "imagem" => "1507715971.DSCF0222.JPG",
      "titulo" => "Titulo",
    ]);
  }
}

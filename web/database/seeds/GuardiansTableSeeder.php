<?php

use Illuminate\Database\seeder;
use App\Models\Guardian;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;

class GuardiansTableSeeder extends Seeder {
  public function run()
  {
    Guardian::truncate();
    $faker = Faker\Factory::create();
    $profile = new Profile();
    $encarregadoRole = Role::whereName('Encarregado')->first();

    $guardians = [
      [
        'primeiro_nome' => $faker->firstName,
        'sobrenome' => $faker->lastName,
        'email' => 'encarregado@encarregado.com',
        'telefone' => $faker->phoneNumber,
        'data_de_nascimento' => $faker->date(),
        'sexo'=> 'masculino',
        'endereco' => $faker->address,
      ],
      [
        'primeiro_nome' => $faker->firstName,
        'sobrenome' => $faker->lastName,
        'email' => 'encarregado2@encarregado.com',
        'telefone' => $faker->phoneNumber,
        'data_de_nascimento' => $faker->date(),
        'sexo'=> 'masculino',
        'endereco' => $faker->address,
      ],
    ];

    foreach ($guardians as $key => $g) {
      $guardian = Guardian::create([
        'primeiro_nome' => $g['primeiro_nome'],
        'sobrenome' => $g['sobrenome'],
        'telefone' =>  $g['telefone'],
        'data_de_nascimento' => $g['data_de_nascimento'],
        'sexo'=> $g['sexo'],
        'endereco' => $g['endereco'],
      ]);

      $user = User::create([
        'nome' => $guardian->nome,
        'email' => $g['email'],
        'userable_id'=> $guardian->id,
        'userable_type' => "Encarregado",
        'password' => bcrypt('password'),
        'token'                          => str_random(64),
        'activated'                      => true,
        'signup_confirmation_ip_address' => $faker->ipv4,
        'admin_ip_address'               => $faker->ipv4,
      ]);

      $user->profile()->save(new Profile());
      $user->attachRole($encarregadoRole);
      $user->save();
    }
  }
}

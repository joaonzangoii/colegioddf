<?php

use Illuminate\Database\Seeder;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    News::truncate();
    // factory(News::class, 40)->create();
    $noticia = News::create([
      "imagem" => "DSCF0249.JPG",
      "titulo" => "Directoria satisfeita com o desempenho dos alunos",
      "descricao" => "A directoria do colegio DDF mostrou-se muito satisfeita
      pelo facto dos estudantes do colegio terem tido 100% de aprveitamento",
      "usuario_id" => 1
    ]);

    $noticia = News::create([
      "imagem" => "DSCF0217.JPG",
      "titulo" => "Alunos tendo aulas na sala de aulas",
      "descricao" => "Aulas muito boas que os alunos nao conseguem fugir",
      "usuario_id" => 1
    ]);

    $noticia = News::create([
      "imagem" => "DSCF0211.JPG",
      "titulo" => "Alunos tendo aulas na sala de aulas no dia cultural",
      "descricao" => "Alunos tendo aulas na sala de aulas no dia cultural, bem trajados",
      "usuario_id" => 1
    ]);

    $noticia = News::create([
      "imagem" => "DSCF0200.JPG",
      "titulo" => "Patio do Colegio muito lindo",
      "descricao" => "O patio do colegio bem apetrechado e be lindo, todo
      pintado de azul e com carros a mostra",
      "usuario_id" => 1
    ]);



  }
}

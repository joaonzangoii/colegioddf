<?php

use Illuminate\Database\seeder;
use App\Models\SiteInfo;
use App\Models\User;

class SiteInfoTableSeeder extends Seeder
{
  public function run()
  {
    SiteInfo::truncate();
    $user = User::all()->random();
    SiteInfo::create([
      'nome' => 'Colégio Dom Domingos Franque',
      'email' => 'domingosfranque@yahoo.com.br',
      'telefone' => '0024491382952',
      'fax' => '',
      'endereco' => 'Rua das Forças Armadas, Bairro Povo Grande, Mongo Balança, Cabinda, Angola',
      'coordenadas' => '',
      'descricao' => 'O Colégio Dom Domingos Franque, cuja abreviatura é CDDF,
      é uma instituição de Ensino de iniciativa privada, fundada a 7 de Fevereiro
      de 2000. A designação “Dom Domingos Franque” é oriunda de um antecessor da
      família Franque, um escritor que viveu no século XIX. O mesmo foi um
      impulsionador e mentor da formação de novas gerações. Imbuídos neste espírito
      e visando responder as exigências actuais da escolarização e, no intuito de
      colaborar com o Governo na materialização desta nobre tarefa com vista à dar
      resposta a grande demanda que se fazia sentir em termos de escassez de salas
      de aulas na nossa província, surge a instituição acima refenciada',
      'logo_cabecalho' => '/images/logo_ddf.png',
      'logo_rodape' => '/images/logo_ddf.png',
      'acerca' => '<p style="text-align: center;"><strong>COL&Eacute;GIO DO ENSINO PRIM&Aacute;RIO, I E II CICLO DO ENSINO SECUND&Aacute;RIO DOM DOMINGOS FRANQUE</strong></p>
        <p style="text-align: center;"><strong>HISTORIAL</strong></p>
        <p><strong>O Col&eacute;gio Dom Domingos Franque</strong>, cuja abreviatura &eacute; <strong>CDDF</strong>, &eacute; uma institui&ccedil;&atilde;o de Ensino de iniciativa privada, fundada a 7 de Fevereiro de 2000. A designa&ccedil;&atilde;o &ldquo;Dom Domingos Franque&rdquo; &eacute; oriunda de um antecessor da fam&iacute;lia Franque, um escritor que viveu no s&eacute;culo XIX. O mesmo foi um impulsionador e mentor da forma&ccedil;&atilde;o de novas gera&ccedil;&otilde;es. Imbu&iacute;dos neste esp&iacute;rito e visando responder as exig&ecirc;ncias actuais da escolariza&ccedil;&atilde;o e, no intuito de colaborar com o Governo na materializa&ccedil;&atilde;o desta nobre tarefa com vista &agrave; dar resposta a grande demanda que se fazia sentir em termos de escassez de salas de aulas na nossa prov&iacute;ncia, surge a institui&ccedil;&atilde;o acima refenciada.</p>
        <p>Tem como objecto social Educar e Instruir as novas Gera&ccedil;&otilde;es. Por meio disso, contribuir para o aumento do n&iacute;vel de escolaridade da popula&ccedil;&atilde;o do pa&iacute;s em geral e da prov&iacute;ncia em particular. A sua ac&ccedil;&atilde;o desdobra-se no desenvolvimento do ensino, instru&ccedil;&atilde;o e educa&ccedil;&atilde;o de crian&ccedil;as, adolescentes, jovens e adultos. Suas actividades passam-se de acordo as din&acirc;micas sociais, promovendo o esp&iacute;rito liberal, criativo e cr&iacute;tico, na valoriza&ccedil;&atilde;o da dignidade humana e na promo&ccedil;&atilde;o da liberdade, do respeito e da boa conviv&ecirc;ncia entre os membros da sociedade.</p>
        <p>Sendo o ensino um acto que se desenrola pela participa&ccedil;&atilde;o de v&aacute;rios segmentos da sociedade, o Col&eacute;gio elegeu a fam&iacute;lia, as igrejas e a sociedade civil como parceiros para a materializa&ccedil;&atilde;o do objectivo preconizado. Entretanto, o Col&eacute;gio Dom Domingos Franque, n&atilde;o esta dissociado aos &oacute;rg&atilde;o estaduais, principalmente ao Minist&eacute;rio da Educa&ccedil;&atilde;o. O mesmo,age em fun&ccedil;&atilde;o das directrizes e programas curriculares por este tra&ccedil;ado, n&atilde;o descartando a parceria e a inspec&ccedil;&atilde;o das suas actividades pela Secretaria Provincial da Educa&ccedil;&atilde;o e pela Inspec&ccedil;&atilde;o da Educa&ccedil;&atilde;o Provincial.</p>
        <p>Inicialmente, o Col&eacute;gio funcionava apenas com (3) tr&ecirc;s salas de aulas e com um n&uacute;mero de alunos bastante reduzido. E depois com I e II n&iacute;veis de ensino, divididos em dois per&iacute;odos ou turnos: Matinal ( Pr&eacute;, 1&ordf;, 2&ordf; e 4&ordf; classes) e vespertino (3&ordf;, 5&ordf; e 6&ordf; classes).</p>
        <p>Devido o fluxo, solicita&ccedil;&atilde;o e reconhecimento por parte da popula&ccedil;&atilde;o estudantil nos anos seguintes o Col&eacute;gio foi crescendo n&atilde;o s&oacute; em termos de estruturas f&iacute;sicas, mas tamb&eacute;m em n&uacute;mero de alunos, o que permitiu o acr&eacute;scimo do II e III N&iacute;veis, atingindo assim o n&iacute;vel de desenvolvimento que tem hoje</p>
        <p>Em rela&ccedil;&atilde;o ao seu funcionamento e sobre tudo no capitulo de Ensino e Aprendizagem, o Col&eacute;gio hoje est&aacute; com um ensino muito desenvolvido sendo que, h&aacute; n&iacute;veis diferenciado como, Ensino prim&aacute;rio que compreende a inicia&ccedil;&atilde;o &aacute; 6&ordf; classe; I ciclo do Ensino Secund&aacute;rio (7&ordf;, 8&ordf; e 9&ordf; classes) e II Ciclo do Ensino Secund&aacute;rio (Ensino M&eacute;dio), este &uacute;ltimo com os cursos de Ci&ecirc;ncias Fis&iacute;cas e Biol&oacute;gicas, Humanas e Economicas Juridicas. N&atilde;o menos importante s&atilde;o os dois cursos de Inform&aacute;tica de Gest&atilde;o e Contabilidade de Gest&atilde;o que se encontram na fase experimental.</p>
        <p>Quanto a sua popula&ccedil;&atilde;o estudantil, refira-se que tem ______ alunos no Ensino Prim&aacute;rio, _________ no I&ordm; Ciclo e 711 no II&ordm; Ciclo, ou seja 252 na 10&ordf; Classe, 231 na 11&ordf; Classe e 228 na 12&ordf; Classe. Em rela&ccedil;&atilde;o as promo&ccedil;&otilde;es de finalistas, no ano lectivo passado fez sair a sua 6&ordf; Promo&ccedil;&atilde;o de Ci&ecirc;ncias Humanas e Fisicas-Biol&oacute;gicas. E a 5&ordf; Promo&ccedil;&atilde;o de Ci&ecirc;ncias Econ&oacute;micas e Juridicas.</p>
        <p>O col&eacute;gio possui _______ professores, __________agentes administrativos e ______ da Area Social.</p>
        <p>Importa ainda apontar o impacto social que o Col&eacute;gio Dom Domingos alcan&ccedil;ou nos &uacute;ltimos anos tendo sido a 3&ordf; Melhor Escola Privada de Angola no ano lectivo 2013 no dominio da Forma&ccedil;&atilde;o e Competencia Profissional, conquistou titulos provincias no &acirc;mbito educativo e consequentemente participou de forma vitoriosa nas Olimpiadas de Matem&aacute;tica na Provincia de Malange. No ano lectivo de 2014 venceu o concurso sabadando promovido pela ASSOJUCA tendo um dos seus alunos chegado a finalissima.</p>
        <p>Esta institui&ccedil;&atilde;o de ensino geral realizou tambem actividades de &acirc;mbito social, pedag&oacute;gico e cultural tais como: implatan&ccedil;&atilde;o de arvores, visitas a campos agricolas no &acirc;mbito da disciplina de Geografia Econ&oacute;mica, campanhas de doa&ccedil;&atilde;o de sangue e bens de primeira necessidade, visitas aos org&atilde;os de comunica&ccedil;&atilde;o social, Museu Regional de Cabinda, Tribunal de Cabinda, as Alfandegas, Porto de Cabinda, participa&ccedil;&atilde;o nas Jornadas de Portas Abertas da Faculdade de Medicina da Universidade 11 de Novembro e na campanha de Combate ao HIV-SIDA e rasteio do Cancro da mama. Organizou tamb&eacute;m uma Feira do Livro, tardes culturais, excurs&otilde;es pedag&oacute;gico-recreativas assim como palestras com tem&aacute;ticas voltadas ao Resgate de Valores &eacute;ticos, morais e culturais, importancia da leitura, orienta&ccedil;&atilde;o escolar e profissional.</p>
        <p>&nbsp;</p>
        <p style="text-align: center;">Col&eacute;gio Dom Domingos Franque - Rua das For&ccedil;as Armadas, Bairro Povo Grande -Mongo Balan&ccedil;a - Contribuinte Fiscal n&deg; 5101133841 E-mail: domingosfranque@yahoo.com.br/ domingosfranque@hotmail.com -Telefone &ndash; 913829529 / 930811783 Cabinda - Angola</p>',
      'foto_de_capa_acerca' => '/images/site/1508527367.DSCF0259.JPG',
      'usuario_id' => $user->id,
    ]);
  }
}

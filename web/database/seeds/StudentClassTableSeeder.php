<?php

use Illuminate\Database\seeder;
use App\Models\StudentClass;

class StudentClassesTableSeeder extends Seeder {

  public function run()
  {
    StudentClass::truncate();
  
    for($x=0;$x<=13;$x++){
      $studentClass = StudentClass::create([
        'nome' => $x,
      ]);
    }
  }
}

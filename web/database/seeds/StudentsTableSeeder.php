<?php

use Illuminate\Database\seeder;
use App\Models\Student;
use App\Models\StudentClass;
use App\Models\Admin;
use App\Models\Teacher;
use App\Models\Guardian;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Matricula;
use App\Models\Curso;
use App\Models\Subject;
use App\Models\StudentSubjectTaken;
use App\Models\SubjectAssessment;
use App\Models\StudentAssessmentResult;
use App\Models\Term;
use App\Models\Payment;

class StudentsTableSeeder extends Seeder {

  public function run()
  {
    Student::truncate();
    Matricula::truncate();
    StudentSubjectTaken::truncate();
    StudentAssessmentResult::truncate();
    Payment::truncate();

    $faker = Faker\Factory::create();
    $profile = new Profile();
    $users = [
     [
      'primeiro_nome' => 'Joao',
      'sobrenome' => 'Nzango',
      'sexo' => 'masculino',
      'data_de_nascimento' => '1991-08-14',
      'telefone' => '0791694624',
      'email' => 'joaonzango@gmail.com',
     ],
     [
      'primeiro_nome' => 'Maura',
      'sobrenome' => 'Bernardo',
      'sexo' => 'feminino',
      'data_de_nascimento' => '1993-08-16',
      'telefone' => '0813864756',
      'email' => 'maura.de.jesus2@gmail.com',
     ],
     [
      'primeiro_nome' => 'Atanasio',
      'sobrenome' => 'Nzango',
      'sexo' => 'masculino',
      'data_de_nascimento' => '1997-05-02',
      'telefone' => '0791694624',
      'email' => 'atanasiojr97@gmail.com',
     ],
     [
      'primeiro_nome' => 'Paulino',
      'sobrenome' => 'Nzango',
      'sexo' => 'masculino',
      'data_de_nascimento' => '1993-12-27',
      'telefone' => '0791694624',
      'email' => 'paulinonzango.pn@gmail.com',
     ],
    ];

    foreach($users as $user)
    {
      $userModel   = User::where('email', '=', $user['email'])->first();
      $matriculador = Admin::all()->random();
      $encarregado  = Guardian::all()->random();
      $StudentRole = Role::whereName('Estudante')->first();
      $curso = Curso::first();
      $trimestres =  Term::all();
      $teacher    =  Teacher::all()->random();
      $generator  = App::make('GenerateRegNumber');
      $reg_no = $generator->generateStudentReg();
      $assessments = SubjectAssessment::all();

      if ($userModel === null) {
        $student = Student::create([
          'primeiro_nome' => $user['primeiro_nome'],
          'sobrenome' => $user['sobrenome'],
          'sexo'      => $user['sexo'],
          'nr_de_reg' => $reg_no,
          'data_de_nascimento' => $user['data_de_nascimento'],
          'telefone'  => $user['telefone'],
          'encarregado_id' => $encarregado->id,
        ]);

        $ano_lectivo = 2017;

        $matriculas = [
          Matricula::create([
          'numero_de_matricula'    => $generator->generateMatrReg(),
          'numero_de_boletim'      => '',
          'situacao_da_matricula'  => 'concluida',
          'disciplinas_em_curso'   => 0,
          'disciplinas_concluidas' => 10,
          'valor_da_matricula'     => rand(2000, 2500),
          'propina_mensal'         => rand(5500, 13000),
          'ano_lectivo'            => $ano_lectivo,
          'estudante_id'           => $student->id,
          'responsavel_id'         => $matriculador->id,
          'curso_id'               => $curso->id,
          'classe_id'              => StudentClass::whereNome('12')->first()->id,
        ]),
        Matricula::create([
          'numero_de_matricula'    => $generator->generateMatrReg(),
          'numero_de_boletim'      => '',
          'situacao_da_matricula'  => 'concluida',
          'disciplinas_em_curso'   => 0,
          'disciplinas_concluidas' => 10,
          'valor_da_matricula'     => rand(2000, 2500),
          'propina_mensal'         => rand(5500, 13000),
          'ano_lectivo'            => $ano_lectivo += 1,
          'estudante_id'           => $student->id,
          'responsavel_id'         => $matriculador->id,
          'curso_id'               => $curso->id,
          'classe_id'              => StudentClass::whereNome('13')->first()->id,
        ])];

        foreach ($matriculas as $key => $matricula) {
          $subjects = Subject::all()->random(3);
          $subjectsTaken = [];
          foreach ($subjects as $key => $subject) {
            $result =  rand(9, 20);
            $subjectTaken = StudentSubjectTaken::create([
              'estudante_id'  => $student->id,
              'matricula_id'  => $matricula->id,
              'disciplina_id' => $subject->id,
              'frequencia'    => 0,
              'nota_global'   => $result,
              'situacao'      => 'Null',
            ]);

            $subjectsTaken[] = $subjectTaken;
          }

          $codigo_timestres   = ["I", "II", "III"];
          $codigo_assessments = ["CAP", "CEP/CE", "Exame"];

          foreach ($subjectsTaken as $key => $subject) {
            foreach ($trimestres->take(4) as $key => $trimestre) {
              foreach ($assessments as $key => $assessment) {
                $result =  rand(9, 20);
                if (in_array($trimestre->codigo, $codigo_timestres)
                && !in_array($assessment->codigo, $codigo_assessments)) {
                  StudentAssessmentResult::create([
                    'estudante_id' => $student->id,
                    'disciplina_cursada_id' => $subject->id,
                    'classe_id'    => $matricula->classe_id,
                    'trimestre_id' => $trimestre->id,
                    'matricula_id' => $matricula->id,
                    'avaliacao_id' => $assessment->id,
                    'professor_id' => $teacher->id,
                    'nota' => $result,
                  ]);
                }
              }
            }
          }

          $ultimoTrimestre = $trimestres->last();
          foreach ($subjectsTaken as $key => $subject) {
            foreach ($assessments as $key => $assessment) {
              $result =  rand(9, 20);
              if ($trimestre->codigo  == "CF"
              && in_array($assessment->codigo, $codigo_assessments)) {
                StudentAssessmentResult::create([
                'estudante_id' => $student->id,
                'disciplina_cursada_id' => $subject->id,
                'classe_id'    => $matricula->classe_id,
                'matricula_id' => $matricula->id,
                'trimestre_id' => $ultimoTrimestre->id,
                'avaliacao_id' => $assessment->id,
                'professor_id' => $teacher->id,
                'nota' => $result,
                ]);
              }
            }
          }

          $payment = Payment::create([
            'mes' => 1,
            'valor' => 2000,
            'trimestre_id' => $trimestres->first()->id,
            'estudante_id' => $student->id,
            'matricula_id' => $matricula->id,
            'aprovado' => false
          ]);
        }

        $user = User::create([
          'nome' => $student->nome,
          'email'   => $user['email'],
          'userable_id' => $student->id,
          'userable_type' => "Estudante",
          'password' => bcrypt('password'),
          'activated'                      => true,
          'signup_confirmation_ip_address' => $faker->ipv4,
          'admin_ip_address'               => $faker->ipv4,
          'token'                          => str_random(64),
        ]);

        $user->profile()->save(new Profile());
        $user->attachRole($StudentRole);
        $user->save();
      }
    }
  }
}

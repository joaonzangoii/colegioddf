<?php

use Illuminate\Database\seeder;
use App\Models\SubjectAssessment;

class SubjectsAssessmentsTableSeeder extends Seeder {
  public function run()
  {
    SubjectAssessment::truncate();
    SubjectAssessment::create([
      'nome'   => 'Média das Avaliações continuas',
      'codigo' => 'MAC',
    ]); 

    SubjectAssessment::create([
      'nome'   => 'Classificação da Prova de Professor',
      'codigo' => 'CPP',
    ]); 

    SubjectAssessment::create([
      'nome'   => 'Classificação Trimestral',
      'codigo' => 'CT',
    ]);
 
    SubjectAssessment::create([
      'nome'   => 'Classificação Avaliações do professor',
      'codigo' => 'CAP',
    ]); 

    SubjectAssessment::create([
      'nome'   => 'Classificação Avaliações do professor',
      'codigo' => 'CEP/CE',
    ]);

    SubjectAssessment::create([
      'nome'   => 'Exame',
      'codigo' => 'Exame',
    ]); 
  }
}

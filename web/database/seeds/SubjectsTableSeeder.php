<?php

use Illuminate\Database\seeder;
use App\Models\Subject;

class SubjectsTableSeeder extends Seeder {
  public function run()
  {
    Subject::truncate();
    Subject::create([
      'nome' => 'Filosofia',
      'descricao' =>"Filosofia"
    ]); 

    Subject::create([
      'nome' => 'Sociologia',
      'descricao' =>"Sociologia"
    ]); 

    Subject::create([
      'nome' => 'Matemática',
      'descricao' =>"Ciência dos numeros"
    ]);

    Subject::create([
      'nome' => 'Fisica',
      'descricao' =>"Ciência da Natureza"
    ]);

    Subject::create([
      'nome' => 'Quimica',
      'descricao' =>"Ciência das reacções"
    ]);
  }
}

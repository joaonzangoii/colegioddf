<?php

use Illuminate\Database\seeder;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;
use Faker\Factory as Faker;

class TeachersTableSeeder extends Seeder {

  public function run()
  {
    Teacher::truncate();
    $profile = new Profile();
    $teacherRole = Role::whereName('Professor')->first();
    $generator  = App::make('GenerateRegNumber');
    $teachers = [
      [
        'primeiro_nome' => Faker::create()->firstName,
        'sobrenome' => Faker::create()->lastName,
        'sexo'=> 'masculino',
        'nr_de_reg' =>$generator->generateTeacherReg(),
        'data_de_nascimento' => Faker::create()->date(),
        'telefone' => Faker::create()->phoneNumber,
        'email' => Faker::create()->email,
        'signup_confirmation_ip_address' => Faker::create()->ipv4,
        'admin_ip_address' => Faker::create()->ipv4,
      ],
      [
        'primeiro_nome' => Faker::create()->firstName,
        'sobrenome' => Faker::create()->lastName,
        'sexo'=> 'masculino',
        'nr_de_reg' =>$generator->generateTeacherReg(),
        'data_de_nascimento' => Faker::create()->date(),
        'telefone' => Faker::create()->phoneNumber,
        'email' => Faker::create()->email,
        'signup_confirmation_ip_address' => Faker::create()->ipv4,
        'admin_ip_address' => Faker::create()->ipv4,
      ],
      [
        'primeiro_nome' => Faker::create()->firstName,
        'sobrenome' => Faker::create()->lastName,
        'sexo'=> 'masculino',
        'nr_de_reg' =>$generator->generateTeacherReg(),
        'data_de_nascimento' => Faker::create()->date(),
        'telefone' => Faker::create()->phoneNumber,
        'email' => Faker::create()->email,
        'signup_confirmation_ip_address' => Faker::create()->ipv4,
        'admin_ip_address' => Faker::create()->ipv4,
      ],
      [
        'primeiro_nome' => Faker::create()->firstName,
        'sobrenome' => Faker::create()->lastName,
        'sexo'=> 'masculino',
        'nr_de_reg' =>$generator->generateTeacherReg(),
        'data_de_nascimento' => Faker::create()->date(),
        'telefone' => Faker::create()->phoneNumber,
        'email' => Faker::create()->email,
        'signup_confirmation_ip_address' => Faker::create()->ipv4,
        'admin_ip_address' => Faker::create()->ipv4,
      ]
    ];

    foreach ($teachers as $t) {
      $teacher = Teacher::create([
        'primeiro_nome' => $t['primeiro_nome'],
        'sobrenome' =>$t['sobrenome'],
        'sexo'=> $t['sexo'],
        'nr_de_reg' => $t['nr_de_reg'],
        'data_de_nascimento' => $t['data_de_nascimento'],
        'telefone' => $t['telefone']
      ]);

      $user = User::create([
        'nome' => $teacher->nome,
        'email' => $t['email'],
        'userable_id'=> $teacher->id,
        'userable_type'=> "Professor",
        'password' => bcrypt('password'),
        'token'                          => str_random(64),
        'activated'                      => true,
        'signup_confirmation_ip_address' => $t['signup_confirmation_ip_address'],
        'admin_ip_address'               => $t['admin_ip_address'],
      ]);

      $user->profile()->save(new Profile());
      $user->attachRole($teacherRole);
      $user->save();
    }
  }
}

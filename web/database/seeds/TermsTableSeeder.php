<?php

use Illuminate\Database\Seeder;
use App\Models\Term;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Term::truncate();
      $trimestre = Term::create([
        "nome" => "Primeiro",
        "codigo" => "I"
      ]);

      $trimestre = Term::create([
        "nome" => "Segundo",
        "codigo" => "II"
      ]);

      $trimestre = Term::create([
        "nome" => "Terceiro",
        "codigo" => "III"
      ]);

      $trimestre = Term::create([
        "nome" => "Classificação Final",
        "codigo" => "CF"
      ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Testimonial;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Testimonial::truncate();
      $testemunho = Testimonial::create([
        "mensagem" => "Muito bom essa actividade vai unir os estudantes da escola",
        "usuario_id" => 1
      ]);
    }
}

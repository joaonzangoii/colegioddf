(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/open","name":"debugbar.openhandler","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@handle"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/clockwork\/{id}","name":"debugbar.clockwork","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/stylesheets","name":"debugbar.assets.css","action":"Barryvdh\Debugbar\Controllers\AssetController@css"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/javascript","name":"debugbar.assets.js","action":"Barryvdh\Debugbar\Controllers\AssetController@js"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":"home","action":"App\Http\Controllers\WelcomeController@welcome"},{"host":null,"methods":["GET","HEAD"],"uri":"eventos","name":"eventos","action":"App\Http\Controllers\WelcomeController@eventos"},{"host":null,"methods":["GET","HEAD"],"uri":"contactar","name":"contactar","action":"App\Http\Controllers\WelcomeController@contactar"},{"host":null,"methods":["POST"],"uri":"contactar","name":"contactar.post","action":"App\Http\Controllers\WelcomeController@postContactar"},{"host":null,"methods":["GET","HEAD"],"uri":"sobre-o-colegio","name":"about","action":"App\Http\Controllers\WelcomeController@about"},{"host":null,"methods":["GET","HEAD"],"uri":"informacoes","name":"informacoes","action":"App\Http\Controllers\WelcomeController@informacoes"},{"host":null,"methods":["GET","HEAD"],"uri":"documentos-para-matricula","name":"documentos.matricula","action":"App\Http\Controllers\WelcomeController@documentosMatricula"},{"host":null,"methods":["GET","HEAD"],"uri":"tabela-de-precos","name":"tabela.precos","action":"App\Http\Controllers\WelcomeController@tabelaDePrecos"},{"host":null,"methods":["GET","HEAD"],"uri":"termos-e-condicoes","name":"terms.conditions","action":"App\Http\Controllers\WelcomeController@terms"},{"host":null,"methods":["GET","HEAD"],"uri":"galeria","name":"gallery","action":"App\Http\Controllers\WelcomeController@gallery"},{"host":null,"methods":["GET","HEAD"],"uri":"noticias","name":"noticias.index","action":"App\Http\Controllers\NewsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"noticias\/{noticia}","name":"noticias.show","action":"App\Http\Controllers\NewsController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":null,"action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"activate","name":"activate","action":"App\Http\Controllers\Auth\ActivateController@initial"},{"host":null,"methods":["GET","HEAD"],"uri":"activate\/{token}","name":"authenticated.activate","action":"App\Http\Controllers\Auth\ActivateController@activate"},{"host":null,"methods":["GET","HEAD"],"uri":"activation","name":"authenticated.activation-resend","action":"App\Http\Controllers\Auth\ActivateController@resend"},{"host":null,"methods":["GET","HEAD"],"uri":"exceeded","name":"exceeded","action":"App\Http\Controllers\Auth\ActivateController@exceeded"},{"host":null,"methods":["GET","HEAD"],"uri":"social\/redirect\/{provider}","name":"social.redirect","action":"App\Http\Controllers\Auth\SocialController@getSocialRedirect"},{"host":null,"methods":["GET","HEAD"],"uri":"social\/handle\/{provider}","name":"social.handle","action":"App\Http\Controllers\Auth\SocialController@getSocialHandle"},{"host":null,"methods":["GET","HEAD"],"uri":"re-activate\/{token}","name":"user.reactivate","action":"App\Http\Controllers\RestoreUserController@userReActivate"},{"host":null,"methods":["GET","HEAD"],"uri":"activation-required","name":"activation-required","action":"App\Http\Controllers\Auth\ActivateController@activationRequired"},{"host":null,"methods":["GET","HEAD"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":"dashboard.home","action":"App\Http\Controllers\PagesManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"profile\/{username}","name":"{username}","action":"App\Http\Controllers\ProfilesController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"profile\/create","name":"profile.create","action":"App\Http\Controllers\ProfilesController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"profile\/{profile}","name":"profile.show","action":"App\Http\Controllers\ProfilesController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"profile\/{profile}\/edit","name":"profile.edit","action":"App\Http\Controllers\ProfilesController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"profile\/{profile}","name":"profile.update","action":"App\Http\Controllers\ProfilesController@update"},{"host":null,"methods":["PUT"],"uri":"profile\/{username}\/updateUserAccount","name":"{username}","action":"App\Http\Controllers\ProfilesController@updateUserAccount"},{"host":null,"methods":["PUT"],"uri":"profile\/{username}\/updateUserPassword","name":"{username}","action":"App\Http\Controllers\ProfilesController@updateUserPassword"},{"host":null,"methods":["DELETE"],"uri":"profile\/{username}\/deleteUserAccount","name":"{username}","action":"App\Http\Controllers\ProfilesController@deleteUserAccount"},{"host":null,"methods":["GET","HEAD"],"uri":"images\/profile\/{id}\/avatar\/{image}","name":null,"action":"App\Http\Controllers\ProfilesController@userProfileAvatar"},{"host":null,"methods":["POST"],"uri":"avatar\/upload","name":"avatar.upload","action":"App\Http\Controllers\ProfilesController@upload"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/notas","name":"estudante.notas","action":"App\Http\Controllers\StudentsMarksManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/notas\/{nota}","name":"estudante.notas.show","action":"App\Http\Controllers\StudentsMarksManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/{ano_lectivo}\/notas\/{nota}","name":"estudante.ano_lectivo.notas.show","action":"App\Http\Controllers\StudentsMarksManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/disciplinas\/{disciplina}","name":"estudante.disciplina.show","action":"App\Http\Controllers\StudentsSubjectsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/matriculas","name":"estudante.matriculas","action":"App\Http\Controllers\StudentsMatriculationsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/matriculas\/{matricula}","name":"estudante.matricula.show","action":"App\Http\Controllers\StudentsMatriculationsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/pagamentos","name":"estudante.pagamentos","action":"App\Http\Controllers\StudentsPaymentsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/pagamentos\/{pagamento}\/edit","name":"estudante.pagamento.edit","action":"App\Http\Controllers\StudentsPaymentsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"estudantes\/pagamentos\/{pagamento}","name":"estudante.pagamento.update","action":"App\Http\Controllers\StudentsPaymentsManagementController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/deleted","name":"deleted.index","action":"App\Http\Controllers\SoftDeletesController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/deleted\/{deleted}","name":"deleted.show","action":"App\Http\Controllers\SoftDeletesController@show"},{"host":null,"methods":["PUT","PATCH"],"uri":"usuarios\/deleted\/{deleted}","name":"deleted.update","action":"App\Http\Controllers\SoftDeletesController@update"},{"host":null,"methods":["DELETE"],"uri":"usuarios\/deleted\/{deleted}","name":"deleted.destroy","action":"App\Http\Controllers\SoftDeletesController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios","name":"usuarios","action":"App\Http\Controllers\UsersManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/create","name":"usuarios.create","action":"App\Http\Controllers\UsersManagementController@create"},{"host":null,"methods":["POST"],"uri":"usuarios","name":"usuarios.store","action":"App\Http\Controllers\UsersManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/{usuario}","name":"usuario.show","action":"App\Http\Controllers\UsersManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/{usuario}\/edit","name":"usuario.edit","action":"App\Http\Controllers\UsersManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"usuarios\/{usuario}","name":"usuarios.update","action":"App\Http\Controllers\UsersManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"usuarios\/{usuario}","name":"usuario.destroy","action":"App\Http\Controllers\UsersManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"professores","name":"professores","action":"App\Http\Controllers\UsersManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"professores\/create","name":"professores.create","action":"App\Http\Controllers\UsersManagementController@create"},{"host":null,"methods":["POST"],"uri":"professores","name":"professores.store","action":"App\Http\Controllers\UsersManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"professores\/{professore}","name":"professor.show","action":"App\Http\Controllers\UsersManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"professores\/{professore}\/edit","name":"professor.edit","action":"App\Http\Controllers\UsersManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"professores\/{professore}","name":"professores.update","action":"App\Http\Controllers\UsersManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"professores\/{professore}","name":"professor.destroy","action":"App\Http\Controllers\UsersManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregados","name":"encarregados","action":"App\Http\Controllers\UsersManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregados\/create","name":"encarregados.create","action":"App\Http\Controllers\UsersManagementController@create"},{"host":null,"methods":["POST"],"uri":"encarregados","name":"encarregados.store","action":"App\Http\Controllers\UsersManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregados\/{encarregado}","name":"encarregado.show","action":"App\Http\Controllers\UsersManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregados\/{encarregado}\/edit","name":"encarregado.edit","action":"App\Http\Controllers\UsersManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"encarregados\/{encarregado}","name":"encarregados.update","action":"App\Http\Controllers\UsersManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"encarregados\/{encarregado}","name":"encarregado.destroy","action":"App\Http\Controllers\UsersManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"disciplinas","name":"disciplinas","action":"App\Http\Controllers\SubjectsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"disciplinas\/create","name":"disciplinas.create","action":"App\Http\Controllers\SubjectsManagementController@create"},{"host":null,"methods":["POST"],"uri":"disciplinas","name":"disciplinas.store","action":"App\Http\Controllers\SubjectsManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"disciplinas\/{disciplina}","name":"disciplinas.show","action":"App\Http\Controllers\SubjectsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"disciplinas\/{disciplina}\/edit","name":"disciplina.edit","action":"App\Http\Controllers\SubjectsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"disciplinas\/{disciplina}","name":"disciplinas.update","action":"App\Http\Controllers\SubjectsManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"disciplinas\/{disciplina}","name":"disciplina.destroy","action":"App\Http\Controllers\SubjectsManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"contactos","name":"contactos","action":"App\Http\Controllers\ContactosManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"contactos\/{contacto}","name":"contacto.show","action":"App\Http\Controllers\ContactosManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"contactos\/{contacto}\/edit","name":"contacto.edit","action":"App\Http\Controllers\ContactosManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"contactos\/{contacto}","name":"contactos.update","action":"App\Http\Controllers\ContactosManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"contactos\/{contacto}","name":"contacto.destroy","action":"App\Http\Controllers\ContactosManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/matriculas\/readmitir","name":"estudante.matriculas.readmitir","action":"App\Http\Controllers\StudentsManagementController@getReadmitir"},{"host":null,"methods":["POST"],"uri":"estudantes\/matriculas\/readmitir","name":"estudante.matriculas.readmitir","action":"App\Http\Controllers\StudentsManagementController@postReadmitir"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes","name":"estudantes","action":"App\Http\Controllers\StudentsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/create","name":"estudantes.create","action":"App\Http\Controllers\StudentsManagementController@create"},{"host":null,"methods":["POST"],"uri":"estudantes","name":"estudantes.store","action":"App\Http\Controllers\StudentsManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/{estudante}","name":"estudantes.show","action":"App\Http\Controllers\StudentsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/{estudante}\/edit","name":"estudantes.edit","action":"App\Http\Controllers\StudentsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"estudantes\/{estudante}","name":"estudantes.update","action":"App\Http\Controllers\StudentsManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"estudantes\/{estudante}","name":"estudante.destroy","action":"App\Http\Controllers\StudentsManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"pagamentos","name":"pagamentos","action":"App\Http\Controllers\PaymentsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"pagamentos\/create","name":"pagamentos.create","action":"App\Http\Controllers\PaymentsManagementController@create"},{"host":null,"methods":["POST"],"uri":"pagamentos","name":"pagamentos.store","action":"App\Http\Controllers\PaymentsManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"pagamentos\/{pagamento}","name":"pagamentos.show","action":"App\Http\Controllers\PaymentsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"pagamentos\/{pagamento}\/edit","name":"pagamentos.edit","action":"App\Http\Controllers\PaymentsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"pagamentos\/{pagamento}","name":"pagamentos.update","action":"App\Http\Controllers\PaymentsManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"pagamentos\/{pagamento}","name":"pagamento.destroy","action":"App\Http\Controllers\PaymentsManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas\/create\/{id}","name":"estudante.notas.add.create","action":"App\Http\Controllers\AdminMarksManagementController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas","name":"estudante.notas.add.index","action":"App\Http\Controllers\AdminMarksManagementController@index"},{"host":null,"methods":["POST"],"uri":"estudantes\/adicionar\/notas","name":"estudante.notas.add.store","action":"App\Http\Controllers\AdminMarksManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas\/{nota}","name":"estudante.notas.add.show","action":"App\Http\Controllers\AdminMarksManagementController@show"},{"host":null,"methods":["PUT","PATCH"],"uri":"estudantes\/adicionar\/notas\/{nota}","name":"notas.update","action":"App\Http\Controllers\AdminMarksManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"estudantes\/adicionar\/notas\/{nota}","name":"notas.destroy","action":"App\Http\Controllers\AdminMarksManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas\/ver\/{student}","name":"student.get.marks","action":"App\Http\Controllers\AdminMarksManagementController@getMarks"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas\/ver\/current\/{student}","name":"student.get.current.marks","action":"App\Http\Controllers\AdminMarksManagementController@getCurrentMarks"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/notas\/ver\/current\/{student}\/subject","name":"student.get.current.marks.by.subject","action":"App\Http\Controllers\AdminMarksManagementController@getCurrentMarksBySubject"},{"host":null,"methods":["GET","HEAD"],"uri":"estudantes\/adicionar\/disciplinas\/ver\/{student}","name":"student.get.subjects","action":"App\Http\Controllers\AdminMarksManagementController@getSubjects"},{"host":null,"methods":["GET","HEAD"],"uri":"temas","name":"temas","action":"App\Http\Controllers\ThemesManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"temas\/create","name":"temas.create","action":"App\Http\Controllers\ThemesManagementController@create"},{"host":null,"methods":["POST"],"uri":"temas","name":"temas.store","action":"App\Http\Controllers\ThemesManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"temas\/{tema}","name":"tema.show","action":"App\Http\Controllers\ThemesManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"temas\/{tema}\/edit","name":"tema.edit","action":"App\Http\Controllers\ThemesManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"temas\/{tema}","name":"temas.update","action":"App\Http\Controllers\ThemesManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"temas\/{tema}","name":"tema.destroy","action":"App\Http\Controllers\ThemesManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/logs","name":"admin.logs","action":"\Rap2hpoutre\LaravelLogViewer\LogViewerController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/php","name":"admin.php","action":"App\Http\Controllers\PagesManagementController@listPHPInfo"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/routes","name":"admin.routes","action":"App\Http\Controllers\PagesManagementController@listRoutes"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/details","name":"admin.details","action":"App\Http\Controllers\PagesManagementController@listSiteInfo"},{"host":null,"methods":["POST"],"uri":"admin\/details","name":"admin.details.post","action":"App\Http\Controllers\PagesManagementController@postSiteInfo"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/paginas","name":"paginas","action":"App\Http\Controllers\PagesManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/paginas\/create","name":"paginas.create","action":"App\Http\Controllers\PagesManagementController@create"},{"host":null,"methods":["POST"],"uri":"admin\/paginas","name":"paginas.store","action":"App\Http\Controllers\PagesManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/paginas\/{pagina}","name":"paginas.show","action":"App\Http\Controllers\PagesManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/paginas\/{pagina}\/edit","name":"paginas.edit","action":"App\Http\Controllers\PagesManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"admin\/paginas\/{pagina}","name":"paginas.update","action":"App\Http\Controllers\PagesManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"admin\/paginas\/{pagina}","name":"pagina.destroy","action":"App\Http\Controllers\PagesManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/info","name":"admin.pages.info","action":"App\Http\Controllers\PagesManagementController@getInfoPage"},{"host":null,"methods":["POST"],"uri":"pages\/info","name":"admin.pages.info.post","action":"App\Http\Controllers\PagesManagementController@postInfoPage"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/about-us","name":"admin.pages.about","action":"App\Http\Controllers\PagesManagementController@getAboutUsPage"},{"host":null,"methods":["POST"],"uri":"pages\/about-us","name":"admin.pages.about.post","action":"App\Http\Controllers\PagesManagementController@postAboutUsPage"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/eventos","name":"admin.eventos","action":"App\Http\Controllers\EventsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/eventos\/create","name":"admin.eventos.create","action":"App\Http\Controllers\EventsManagementController@create"},{"host":null,"methods":["POST"],"uri":"pages\/eventos","name":"eventos.store","action":"App\Http\Controllers\EventsManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/eventos\/{evento}\/edit","name":"admin.evento.edit","action":"App\Http\Controllers\EventsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"pages\/eventos\/{evento}","name":"eventos.update","action":"App\Http\Controllers\EventsManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"pages\/eventos\/{evento}","name":"admin.evento.destroy","action":"App\Http\Controllers\EventsManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/noticias","name":"admin.noticias","action":"App\Http\Controllers\NewsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/noticias\/create","name":"admin.noticias.create","action":"App\Http\Controllers\NewsManagementController@create"},{"host":null,"methods":["POST"],"uri":"pages\/noticias","name":"noticias.store","action":"App\Http\Controllers\NewsManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/noticias\/{noticia}","name":"admin.noticia.show","action":"App\Http\Controllers\NewsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"pages\/noticias\/{noticia}\/edit","name":"admin.noticia.edit","action":"App\Http\Controllers\NewsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"pages\/noticias\/{noticia}","name":"noticias.update","action":"App\Http\Controllers\NewsManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"pages\/noticias\/{noticia}","name":"admin.noticia.destroy","action":"App\Http\Controllers\NewsManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"photos\/all","name":"photos.all","action":"App\Http\Controllers\GalleryManagementController@all"},{"host":null,"methods":["POST"],"uri":"photos\/filtered","name":"photos.filtered","action":"App\Http\Controllers\GalleryManagementController@filtered"},{"host":null,"methods":["GET","HEAD"],"uri":"photos","name":"photos","action":"App\Http\Controllers\GalleryManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"photos\/create","name":"photos.create","action":"App\Http\Controllers\GalleryManagementController@create"},{"host":null,"methods":["POST"],"uri":"photos","name":"photos.store","action":"App\Http\Controllers\GalleryManagementController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"photos\/{photo}\/edit","name":"photo.edit","action":"App\Http\Controllers\GalleryManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"photos\/{photo}","name":"photos.update","action":"App\Http\Controllers\GalleryManagementController@update"},{"host":null,"methods":["DELETE"],"uri":"photos\/{photo}","name":"photo.destroy","action":"App\Http\Controllers\GalleryManagementController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregado\/estudantes\/pagamentos","name":"encarregado.estudantes.pagamentos","action":"App\Http\Controllers\GuardianStudentsPaymentsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregado\/estudantes\/pagamentos\/{pagamento}\/edit","name":"encarregado.estudante.pagamento.edit","action":"App\Http\Controllers\GuardianStudentsPaymentsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"encarregado\/estudantes\/pagamentos\/{pagamento}","name":"encarregado.estudante.pagamento.update","action":"App\Http\Controllers\GuardianStudentsPaymentsManagementController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregado\/estudantes","name":"encarregado.estudantes","action":"App\Http\Controllers\GuardianStudentsManagementController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregado\/estudantes\/{estudante}","name":"encarregado.estudantes.show","action":"App\Http\Controllers\GuardianStudentsManagementController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"encarregado\/estudantes\/{estudante}\/edit","name":"encarregado.estudantes.edit","action":"App\Http\Controllers\GuardianStudentsManagementController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"encarregado\/estudantes\/{estudante}","name":"estudantes.update","action":"App\Http\Controllers\GuardianStudentsManagementController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"forums","name":"chatter.home","action":"DevDojo\Chatter\Controllers\ChatterController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/category\/{slug}","name":"chatter.category.show","action":"DevDojo\Chatter\Controllers\ChatterController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/login","name":"chatter.login","action":"DevDojo\Chatter\Controllers\ChatterController@login"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/register","name":"chatter.register","action":"DevDojo\Chatter\Controllers\ChatterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/discussion","name":"chatter.discussion.index","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/discussion\/create","name":"chatter.discussion.create","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@create"},{"host":null,"methods":["POST"],"uri":"forums\/discussion","name":"chatter.discussion.store","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/discussion\/{category}\/{slug}","name":"chatter.discussion.showInCategory","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@show"},{"host":null,"methods":["POST"],"uri":"forums\/discussion\/{category}\/{slug}\/email","name":"chatter.discussion.email","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@toggleEmailNotification"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/discussion\/{discussion}","name":"chatter.discussion.show","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/discussion\/{discussion}\/edit","name":"chatter.discussion.edit","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"forums\/discussion\/{discussion}","name":"chatter.discussion.update","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@update"},{"host":null,"methods":["DELETE"],"uri":"forums\/discussion\/{discussion}","name":"chatter.discussion.destroy","action":"DevDojo\Chatter\Controllers\ChatterDiscussionController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/posts","name":"chatter.posts.index","action":"DevDojo\Chatter\Controllers\ChatterPostController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/posts\/create","name":"chatter.posts.create","action":"DevDojo\Chatter\Controllers\ChatterPostController@create"},{"host":null,"methods":["POST"],"uri":"forums\/posts","name":"chatter.posts.store","action":"DevDojo\Chatter\Controllers\ChatterPostController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/posts\/{post}","name":"chatter.posts.show","action":"DevDojo\Chatter\Controllers\ChatterPostController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"forums\/posts\/{post}\/edit","name":"chatter.posts.edit","action":"DevDojo\Chatter\Controllers\ChatterPostController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"forums\/posts\/{post}","name":"chatter.posts.update","action":"DevDojo\Chatter\Controllers\ChatterPostController@update"},{"host":null,"methods":["DELETE"],"uri":"forums\/posts\/{post}","name":"chatter.posts.destroy","action":"DevDojo\Chatter\Controllers\ChatterPostController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"forums.atom","name":"chatter.atom","action":"DevDojo\Chatter\Controllers\ChatterAtomController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@overview"},{"host":null,"methods":["POST"],"uri":"enveditor\/add","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@add"},{"host":null,"methods":["POST"],"uri":"enveditor\/update","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/createbackup","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@createBackup"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/deletebackup\/{timestamp}","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@deleteBackup"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/restore\/{backuptimestamp}","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@restore"},{"host":null,"methods":["POST"],"uri":"enveditor\/delete","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@delete"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/download\/{filename?}","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@download"},{"host":null,"methods":["POST"],"uri":"enveditor\/upload","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@upload"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/getdetails\/{timestamp?}","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@getDetails"},{"host":null,"methods":["GET","HEAD"],"uri":"enveditor\/test","name":null,"action":"Brotzka\DotenvEditor\Http\Controller\EnvController@test"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);


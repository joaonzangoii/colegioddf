<?php

return [
    // Flash Messages
    'updateSuccess'     => 'Successfully updated contact! ',
    'deleteSuccess'     => 'Successfully deleted contact! ',
    // Show Contact Tab
    'editContact'               => 'Edit Contact',
    'deleteContact'             => 'Delete Contact',
    'contactsBackBtn'           => 'Back to Contacts',
    'contactsPanelTitle'        => 'Contact Information',
    'labelContactName'          => 'Contact name:',
    'labelDeletedAt'         => 'Deleted on',
    'contactsDeletedPanelTitle' => 'Deleted Contact Information',
    'contactsBackDelBtn'        => 'Back to Deleted Contacts',
    'successRestore'    => 'Contact successfully restored.',
    'successDestroy'    => 'Contact record successfully destroyed.',
    'errorContactNotFound' => 'Contact not found.',
    'showContact'  => 'Show Contacts',
    'showAllMarksFor'  => 'Show Marks for',
    'showAllContacts'  => 'All Contacts',
    'showContactsManagementMenu'  => 'Show Contacts Management Menu',
    'backToContact'    => 'Back to Contact',
    'backToContacts'    => 'Back to contacts',
    'labelName'        => 'Name',
    'labelEmail'       => 'Email',
    'labelTelephone'   => 'Telephone',
    'labelMessage'     => 'Message',

];

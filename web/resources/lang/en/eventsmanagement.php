<?php

return [

    // Flash Messages
    'createSuccess'     => 'Successfully created event!',
    'updateSuccess'     => 'Successfully updated event!',
    'deleteSuccess'     => 'Successfully deleted event!',
    'deleteSelfError'   => 'You cannot delete this event!',

    // Show Event Tab
    'createEvent'             => 'Create Event',
    'editEvent'               => 'Edit Event',
    'editEvent'            => 'Edit Event',
    'deleteEvent'          => 'Delete Event',
    'deleteEventMsg'       => 'Are you sure you want to delete this Event?',
    'deleteEvent'             => 'Delete Event',
    'eventsBackBtn'             => 'Back to Events',
    'successDestroy'    => 'Event record successfully destroyed.',
    'errorEventNotFound' => 'Event not found.',

    'backToEvent'    => 'Back to event',
    'backToEvents'    => 'Back to events',
    'showAllEvents'    => 'Show all Events',
    
];

<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email'             => 'Email',
    'create_user_ph_email'                => 'Email',
    'create_user_icon_email'              => 'fa-envelope',

    'create_user_label_username'           => 'Username',
    'create_user_ph_username'              => 'Username',
    'create_user_icon_username'            => 'fa-user-secret',

    'create_user_label_mobile'           => 'Mobile',
    'create_user_ph_mobile'              => 'Mobile',
    'create_user_icon_mobile'            => 'fa-mobile',

    'create_user_label_name'              => 'Name',
    'create_user_label_reg_num'           => 'Registration Number',
    'create_user_label_firstname'         => 'First Name',
    'create_user_ph_firstname'            => 'First Name',
    'create_user_icon_firstname'          => 'fa-user',
    'create_user_ph_select_reg_num'       => 'Select Registration Number',


    'create_user_label_lastname'           => 'Last Name',
    'create_user_ph_lastname'              => 'Last Name',
    'create_user_icon_lastname'            => 'fa-user',

    'create_user_label_password'           => 'Password',
    'create_user_ph_password'              => 'Password',
    'create_user_icon_password'            => 'fa-lock',

    'create_user_label_pw_confirmation'    => 'Confirm Password',
    'create_user_ph_pw_confirmation'       => 'Confirm Password',
    'create_user_icon_pw_confirmation'     => 'fa-lock',

    'create_user_label_location'           => 'User Location',
    'create_user_ph_location'              => 'User Location',
    'create_user_icon_location'            => 'fa-map-marker',

    'create_user_label_bio'                => 'User Biography',
    'create_user_ph_bio'                   => 'User Biography',
    'create_user_icon_bio'                 => 'fa-pencil',

    'create_user_label_twitter_username'   => 'User Twitter Username',
    'create_user_ph_twitter_username'      => 'User Twitter Username',
    'create_user_icon_twitter_username'    => 'fa-twitter',

    'create_user_label_github_username'    => 'User GitHub Username',
    'create_user_ph_github_username'       => 'User GitHub Username',
    'create_user_icon_github_username'     => 'fa-github',

    'create_user_label_career_title'       => 'User Occupation',
    'create_user_ph_career_title'          => 'User Occupation',
    'create_user_icon_career_title'        => 'fa-briefcase',

    'create_user_label_education'         => 'Education',
    'create_user_ph_education'            => 'Education',
    'create_user_icon_education'          => 'fa-graduation-cap',

    'create_user_label_role'               => 'Role',
    'create_user_ph_role'                  => 'Select Role',
    'create_user_icon_role'                => 'fa-shield',

    'create_user_label_sexo'               => 'Gender',
    'create_user_ph_sexo'                  => 'Select Gender',
    'create_user_icon_sexo'                => 'fa-intersex',

    'create_user_label_dob'               => 'Date of Birth',
    'create_user_ph_dob'                  => 'Select date of birth',
    'create_user_icon_dob'                => 'fa-birthday-cake',

    'create_user_label_classe'          => 'Grade',
    'create_user_ph_classe'             => 'Select Grade',
    'create_user_icon_classe'           => 'fa-book',

    'create_user_label_course'          => 'Course',
    'create_user_ph_course'             => 'Select Course',
    'create_user_icon_course'           => 'fa-book',

    'create_user_label_valor_da_matricula' => 'Matriculation fee',
    'create_user_ph_valor_da_matricula'    => 'Matriculation fee',
    'create_user_icon_valor_da_matricula'  => 'fa-money',

    'create_user_label_propina_mensal' => 'Monthly fee',
    'create_user_ph_propina_mensal'    => 'Monthly fee',
    'create_user_icon_propina_mensal'  => 'fa-money',

    'create_user_label_disciplinas' => 'Subjects',
    'create_user_ph_disciplinas'    => 'Select Subjects',
    'create_user_icon_disciplinas'  => 'fa-book',

    'create_user_label_year'          => 'Year',
    'create_user_ph_year'             => 'Select year',
    'create_user_icon_year'           => 'fa-calendar-o',

    'create_user_label_guardian'          => 'Guardian',
    'create_user_ph_guardian'             => 'Select guardian',
    'create_user_icon_guardian'           => 'fa-user',

    'create_subject_label_nome'         => 'Name',
    'create_subject_label_disc_nome'    => 'Subject',
    'create_subject_ph_select_nome'     => 'Select Subject',
    'create_subject_ph_nome'            => 'Name',
    'create_subject_icon_nome'          => 'fa-book',

    'create_mark_label_teacher'         => 'Teacher',
    'create_mark_ph_select_teacher'     => 'Select Teacher',
    'create_mark_label_term'            => 'Term',
    'create_mark_ph_select_term'        => 'Select Term',
    'create_mark_icon_term'             => 'fa-book',

    'create_user_button_text'            => 'Add User',
    'create_subject_button_text'         => 'Add Subject',
    'create_student_button_text'         => 'Add Student',
    'create_mark_button_text'            => 'Add Student Mark',
    'create_payment_button_text'         => 'Add Student Payment',
    'create_event_button_text'           => 'Add Event',
    'create_news_button_text'            => 'Add News',

    'create_system_ph_about'         => 'About',
    'create_system_ph_terms'         => 'Terms',
    'create_system-ph-banner-pic'    => 'Banner picture',
    'create_system-ph-header-logo'    => 'Header picture',
    'create_system-ph-footer-logo'    => 'Footer picture',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'              => 'Edit User Information',

    'label-username'                     => 'Username',
    'ph-username'                        => 'Username',

    'label-useremail'                     => 'User Email',
    'ph-useremail'                        => 'User Email',

    'label-userrole_id'                    => 'User Access Level',
    'option-label'                         => 'Select a Level',
    'option-user'                          => 'User',
    'option-editor'                        => 'Editor',
    'option-admin'                         => 'Administrator',
    'submit-btn-text'                      => 'Edit the User!',

    'submit-btn-icon'                      => 'fa-save',
    'username-icon'                        => 'fa-user',
    'mobile-icon'                        => 'fa-mobile',
    'useremail-icon'                     => 'fa-envelope-o',

    'create_event_label_name'           => 'Name',
    'create_event_ph_name'              => 'Name',
    'create_event_icon_name'            => 'fa-tag',
    'create_event_label_starting_time'  => 'Starting Time',
    'create_event_ph_starting_time'     => 'Starting Time',
    'create_event_icon_starting_time'   => 'fa-clock-o',
    'create_event_label_ending_time'    => 'Ending Time',
    'create_event_ph_ending_time'       => 'Ending Time',
    'create_event_icon_ending_time'     => 'fa-clock-o',
    'create_event_label_place'          => 'Place',
    'create_event_ph_place'             => 'Place',
    'create_event_icon_place'           => 'fa-home',
    'create_event_label_date'          => 'Date',
    'create_event_ph_date'             => 'Date',
    'create_event_icon_date'           => 'fa-calendar',
    'create_event_label_description'   => 'Description',
    'create_event_ph_description'      => 'Description',
    'create_event_icon_description'    => 'fa-info',

    'ph-system-name'                    => 'School name',
    'ph-system-telephone'               => 'School telephone',
    'ph-system-address'                 => 'School address',
    'ph-system-email'                   => 'School email',
    'ph-system-description'             => 'School description',
    'ph-system-fax'                     => 'Fax',
    'ph-system-coordinates'             => 'Coordinates',

    'save'                              => 'Save',
    'save_changes'                      => 'Save Changes',

    'create_payment_ph_student'     => 'Select Student',
    'create_payment_ph_approved'    => 'Select State',
    'create_payment_ph_amount'      => 'Amount',
    'create_payment_label_approved' =>  'State',
    'create_payment_label_attachment' =>  'Attachment',
    'create_payment_label_amount'   =>  'Amount',
    'create_payment_label_student'  =>  'Student',
    'create_payment_icon_approved'  =>  'fa-tags',
    'create_payment_icon_student'   =>  'fa-user',
    'create_payment_icon_amount'    =>  'fa-money',
    'create_payment_label_month'    =>  'Month',
    'create_payment_icon_month'     =>  'fa-calendar',
    'create_payment_ph_select_month' => 'Select Month',
    'payments_ph_attachment'        =>  'Boderom',

    'create_news_label_title'        => 'Title',
    'create_news_label_description'  => 'Description',
    'create_news_label_image'  => 'Image',
    'create_news_ph_title'        => 'Title',
    'create_news_ph_description'  => 'Description',
    'create_news_icon_title'  => 'fa-tags',
    'create_news_icon_description'  => 'fa-info',


];

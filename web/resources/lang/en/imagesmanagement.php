<?php

return [
    // Flash Messages
    'updateError'       => 'Unable to update this image!',
    'wrongDateRange'    => 'Wrong Date Range.. Choose a right date range.',
    'noImages'          => 'Sorry! No images are available for this date range..',

];

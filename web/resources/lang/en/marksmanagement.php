<?php

return [

    // Flash Messages
    'createSuccess'     => 'Successfully created mark!',
    'updateSuccess'     => 'Successfully updated mark!',
    'deleteSuccess'     => 'Successfully deleted mark!',
    'createNoMatriculation' => 'Student not matriculated yet!',

    // Show mark Tab
    'editmark'               => 'Edit mark',
    'deletemark'             => 'Delete mark',
    'marksBackBtn'           => 'Back to marks',
    'marksPanelTitle'        => 'Mark Information',
    'labelmarkName'          => 'Mark name:',
    'labelCreatedAt'         => 'Created At:',
    'labelUpdatedAt'         => 'Updated At:',
    'labelDeletedAt'         => 'Deleted on',
    'marksDeletedPanelTitle' => 'Deleted mark Information',
    'marksBackDelBtn'        => 'Back to Deleted marks',

    'successRestore'    => 'mark successfully restored.',
    'successDestroy'    => 'mark record successfully destroyed.',
    'errormarkNotFound' => 'mark not found.',

];

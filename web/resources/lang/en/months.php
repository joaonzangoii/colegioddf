<?php

return [
  'months' => [
    "01" => "January",
    "02" => "February",
    "03" => "March",
    "04" => "April",
    "05" => "May",
    "06" => "June",
    "07" => "July",
    "08" => "August",
    "09" => "September",
    "10" => "October",
    "11" => "November",
    "12" => "December "
  ],
  'monthsShort' => [
    "01" => "Jan",
    "02" => "Fe",
    "03" => "Mar",
    "04" => "Apr",
    "05" => "May",
    "06" => "Jun",
    "07" => "Jul",
    "08" => "Aug",
    "09" => "Sep",
    "10" => "Oct",
    "11" => "Nov",
    "12" => "Dec "
  ],
  'days' => [
    "01" => "Monday",
    "02" => "Tuesday",
    "03" => "Wednesday",
    "04" => "Thursday",
    "05" => "Friday",
    "06" => "Saturday",
    "07" => "Sunday",
  ],
  'daysShort' => [
    "01" => "Mon",
    "02" => "Tue",
    "03" => "Wed",
    "04" => "Thur",
    "05" => "Fri",
    "06" => "Sat",
    "07" => "Sun",
  ]
];

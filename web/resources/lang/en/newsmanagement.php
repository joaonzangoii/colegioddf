<?php

return [

    // Flash Messages
    'createSuccess'     => 'Successfully created news!',
    'updateSuccess'     => 'Successfully updated news!',
    'deleteSuccess'     => 'Successfully deleted news!',
    'deleteSelfError'   => 'You cannot delete this news!',

    // Show News Tab
    'createNews'             => 'Create News',
    'editNews'               => 'Edit News',
    'editNews'            => 'Edit News',
    'deleteNews'          => 'Delete News',
    'deleteNewsMsg'       => 'Are you sure you want to delete this News?',
    'deleteNews'             => 'Delete News',
    'newsBackBtn'             => 'Back to News',
    'successDestroy'    => 'News record successfully destroyed.',
    'errorNewsNotFound' => 'News not found.',

    'backToNews'    => 'Back to news',
    'backToNews'    => 'Back to news',
    'showAllNews'    => 'Show all News',
    'panelTitle'  => 'News',

];

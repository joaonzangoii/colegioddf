<?php

return [

    // Flash Messages
    'createSuccess'     => 'Successfully created payment!',
    'updateSuccess'     => 'Successfully updated payment!',
    'deleteSuccess'     => 'Successfully deleted payment!',
    'deleteSelfError'   => 'You cannot delete yourself!',
    'alreadyAdded'      => 'Payments already Created',

    // Show Payment Tab
    'createPayment'             => 'Create Payment',
    'editPayment'               => 'Edit Payment',
    'deletePayment'             => 'Delete Payment',
    'subjectsBackBtn'           => 'Back to Payments',
    'subjectsPanelTitle'        => 'Payment Information',
    'labelPaymentName'          => 'Payment type:',
    'labelName'              => 'Name:',
    'labelAmount'            => 'Amount:',
    'labelTerm'              => 'Term:',
    'labelMonth'             => 'Month:',
    'labelYear'             => 'Year:',
    'labelStatus'            => 'Status:',
    'labelAccessLevel'       => 'Access',
    'labelCreatedAt'         => 'Created At:',
    'labelUpdatedAt'         => 'Updated At:',
    'subjectsDeletedPanelTitle' => 'Deleted Payment Information',
    'subjectsBackDelBtn'        => 'Back to Deleted Payments',

    'successDestroy'    => 'Payment record successfully destroyed.',
    'errorPaymentNotFound' => 'Payment not found.',

    'labelPaymentLevel'     => 'Level',
    'labelPaymentLevels'    => 'Levels',
    'backToPayment'    => 'Back to payment',
    'backToPayments'    => 'Back to payments',
    'showAllPayments'    => 'Show all Payments',

];

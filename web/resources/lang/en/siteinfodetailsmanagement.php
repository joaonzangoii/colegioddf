<?php

return [
    // Flash Messages
    'createSuccess'     => 'Successfully created site information!',
    'updateSuccess'     => 'Successfully updated site information!',
    'deleteSuccess'     => 'Successfully deleted site information!',
];

<?php

return [

  // Flash Messages
  'createSuccess'     => 'Successfully created student! ',
  'updateSuccess'     => 'Successfully updated student! ',
  'deleteSuccess'     => 'Successfully deleted student! ',
  'deleteSelfError'   => 'You cannot delete yourself! ',

  // Show Student Tab
  'viewProfile'            => 'View Profile',
  'editStudent'            => 'Edit Student',
  'deleteStudent'          => 'Delete Student',
  'deleteStudentMsg'       => 'Are you sure you want to delete this Student?',
  'studentsBackBtn'        => 'Back to Students',
  'studentsPanelTitle'     => 'Student Information',
  'labelStudentName'       => 'Student name:',
  'labelName'              => 'Name:',
  'labelFirstName'         => 'First Name:',
  'labelLastName'          => 'Last Name:',
  'labelEmail'             => 'Email:',
  'labelRole'              => 'Role:',
  'labelStatus'            => 'Status:',
  'labelAccessLevel'       => 'Access',
  'labelPermissions'       => 'Permissions:',
  'labelCreatedAt'         => 'Created At:',
  'labelUpdatedAt'         => 'Updated At:',
  'labelIpEmail'           => 'Email Signup IP:',
  'labelIpConfirm'         => 'Confirmation IP:',
  'labelIpSocial'          => 'Socialite Signup IP:',
  'labelIpAdmin'           => 'Admin Signup IP:',
  'labelIpUpdate'          => 'Last Update IP:',
  'labelDeletedAt'         => 'Deleted on',
  'labelIpDeleted'         => 'Deleted IP:',
  'studentsDeletedPanelTitle' => 'Deleted Student Information',
  'studentsMarksPanelTitle'   => 'Marks',
  'studentsBackDelBtn'        => 'Back to Deleted Students',
  'studentsSubjectsBackBtn'   => 'Back to Subjects',
  'studentsMatriculationsBackBtn' => 'Back to Registration',
  'studentsPaymentsBackBtn' => 'Back to Payments',

  'successRestore'    => 'Student successfully restored.',
  'successDestroy'    => 'Student record successfully destroyed.',
  'errorStudentNotFound' => 'Student not found.',

  'labelStudentLevel'  => 'Level',
  'labelStudentLevels' => 'Levels',

  'attrStdNumber'         => 'Student Number',
  'attrName'              => 'Name',
  'attrFirstName'         => 'First Name',
  'attrLastName'          => 'Last Name',
  'attrRole'              => 'Role',
  'attrEmail'             => 'Email',
  'attrGuardianName'      => 'Guardian',
  'attrStudentsNumber'    => 'Students',

  'changePassword'       => 'Change Password',

];

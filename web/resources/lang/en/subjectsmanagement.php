<?php

return [

    // Flash Messages
    'createSuccess'     => 'Successfully created subject! ',
    'updateSuccess'     => 'Successfully updated subject! ',
    'deleteSuccess'     => 'Successfully deleted subject! ',

    // Show Subject Tab
    'editSubject'               => 'Edit Subject',
    'deleteSubject'             => 'Delete Subject',
    'subjectsBackBtn'           => 'Back to Subjects',
    'subjectsPanelTitle'        => 'Subject Information',
    'labelSubjectName'          => 'Subject name:',
    'labelDeletedAt'         => 'Deleted on',
    'subjectsDeletedPanelTitle' => 'Deleted Subject Information',
    'subjectsBackDelBtn'        => 'Back to Deleted Subjects',

    'successRestore'    => 'Subject successfully restored.',
    'successDestroy'    => 'Subject record successfully destroyed.',
    'errorSubjectNotFound' => 'Subject not found.',

    'showSubject'  => 'Show Subjects',
    'showAllMarksFor'  => 'Show Marks for',
    'showAllSubjects'  => 'All Subjects',
    'showSubjectsManagementMenu'  => 'Show Subjects Management Menu',

    'backToSubject'    => 'Back to Subject',
    'backToSubjects'    => 'Back to subjects',

];

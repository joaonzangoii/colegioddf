<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'As informações de login não foram encontradas.',
    'throttle' => 'Muitas tentativas de login. Por favor tente novamente em :seconds segundos.',

    // Activation items
    'sentEmail'         => 'Enviamos um e-mail para :email.',
    'clickInEmail'      => 'Por favor clique no link no email enviado para activar a sua conta.',
    'anEmailWasSent'    => 'Um email foi enviado para :email no dia :date.',
    'clickHereResend'   => 'Clique aqui para re-enviar o email.',
    'successActivated'  => 'Sucesso, a sua conta foi activada.',
    'unsuccessful'      => 'A sua conta não foi activada; tente de novo.',
    'notCreated'        => 'Sua conta não pôde ser criada; Por favor, tente novamente.',
    'tooManyEmails'     => 'Muitos e-mails de ativação foram enviados para :email. <br />Tente de novo em <span class="label label-danger">:hours horas</span>.',
    'regThanks'         => 'Obrigado por se registrar, ',
    'invalidToken'      => 'Token de ativação inválido. ',
    'activationSent'    => 'E-mail de ativação enviado. ',
    'alreadyActivated'  => 'Já ativado.',

    // Labels
    'whoops'            => 'Ups! ',
    'someProblems'      => 'Houve alguns problemas com os dados fornecidos.',
    'email'             => 'E-Mail',
    'password'          => 'Palavra-passe',
    'rememberMe'        => 'Lembre-se de Mim',
    'login'             => 'Entrar',
    'forgot'            => 'Esqueceu a sua senha?',
    'forgot_message'    => 'Ploblemas com a palavra passe?',
    'name'              => 'Nome de Usuário',
    'first_name'        => 'Primeiro Nome',
    'last_name'         => 'Último Nome',
    'confirmPassword'   => 'Confirmar Palavra passe',
    'register'          => 'Registrar',

    // Placeholders
    'ph_name'           => 'Nome de Usuário',
    'ph_email'          => 'E-mail',
    'ph_firstname'      => 'Primeiro Nome',
    'ph_lastname'       => 'Último Nome',
    'ph_password'       => 'Palavra passe',
    'ph_password_conf'  => 'Confirmar Palavra passe',

    // User flash messages
    'sendResetLink'     => 'Enviar link de redefinição de senha',
    'resetPassword'     => 'Trocar a senha',
    'loggedIn'          => 'Você está logado!',

    // email links
    'pleaseActivate'    => 'Ative sua conta.',
    'clickHereReset'    => 'Clique aqui para redefinir sua senha: ',
    'clickHereActivate' => 'Clique aqui para ativar sua conta: ',

    // Validators
    'userNameTaken'     => 'Nome de Usuário indisponível',
    'userNameRequired'  => 'Nome do Usuário requerida',
    'fNameRequired'     => 'Primeiro Nome requerido',
    'lNameRequired'     => 'Último Nome requerido',
    'emailRequired'     => 'Email requerido',
    'emailInvalid'      => 'Email inválido',
    'passwordRequired'  => 'Senha requerida',
    'PasswordMin'       => 'A senha precisa ter pelo menos 6 caracteres',
    'PasswordMax'       => 'O comprimento máximo da senha é de 20 caracteres',
    'captchaRequire'    => 'Captcha é obrigatório',
    'CaptchaWrong'      => 'Captcha errado, tente novamente.',
    'roleRequired'      => 'Função de usuário é necessária.',

    'activatedUserAttemptedToVisit' => 'O usuário ativado tentou visitar ',

];

<?php

return [
    // Flash Messages
    'updateSuccess'     => 'Contacto modificada com sucesso! ',
    'deleteSuccess'     => 'Contacto excluida com sucesso! ',
    // Show Contact Tab
    'editContact'               => 'Modificar Contacto',
    'deleteContact'             => 'Excluir Contacto',
    'contactsBackBtn'           => 'Voltar as Contactos',
    'contactsPanelTitle'        => 'Informação do Contacto',
    'labelContactName'          => 'Nome da Contacto:',
    'labelDeletedAt'         => 'Excluido em',
    'contactsDeletedPanelTitle' => 'Informação da Contacto Excluida',
    'contactsBackDelBtn'        => 'Voltar as Contactos Excluidas',
    'successRestore'    => 'Contact successfully restored.',
    'successDestroy'    => 'Contact record successfully destroyed.',
    'errorContactNotFound' => 'Contacto não encontrada.',
    'showContact'  => 'Mostrar Contacto',
    'showAllMarksFor'  => 'Mostrar Notas de',
    'showAllContacts'  => 'Mostrar todas Contactos',
    'showContactsManagementMenu'  => 'Mostrar enu de Gerência das Contactos',
    'backToContact'    => 'Voltar a Contacto',
    'backToContacts'    => 'Voltar as Contactos',
    'labelName'        => 'Nome',
    'labelEmail'       => 'Email',
    'labelTelephone'   => 'Telefone',
    'labelMessage'     => 'Mensagem',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Emails Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various emails that
    | we need to display to the user. You are free to modify these
    | language lines according to your application's requirements.
    |
    */

    /*
     * Activate new user account email.
     *
     */

    'activationSubject'  => 'Necessita Autorização',
    'activationGreeting' => 'Bem-vindo!',
    'activationMessage'  => 'Você precisa ativar seu e-mail antes de começar a usar todos os nossos serviços.',
    'activationButton'   => 'Activar',
    'activationThanks'   => 'Muito obrigado por usar a nossa aplicação!',

    /*
     * Goobye email.
     *
     */
    'goodbyeSubject'    => 'Lamentamos ver você ir...',
    'goodbyeGreeting'   => 'Olá :username,',
    'goodbyeMessage'    => 'Lamentamos ver você ir. Queríamos deixar você saber que sua conta foi excluída. Agradeço o tempo que compartilhamos. Você tem '.config (' settings.restoreUserCutoff ').' dias para restaurar sua conta.',
    'goodbyeButton'     => 'Restaurar Conta',
    'goodbyeThanks'     => 'Esperamos vê-lo novamente!',

    /*
     * Reset Password.
     *
     */
    'resetPassword' => 'Trocar a Senha',
    'resetPasswordMessage' => 'Você está recebendo este e-mail porque recebemos um pedido de redefinição de senha para sua conta.',
    'resetPasswordThanks' => 'Se você não solicitou uma reinicialização da senha, nenhuma ação adicional será necessária.',


];

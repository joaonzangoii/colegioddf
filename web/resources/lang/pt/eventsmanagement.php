<?php

return [

    // Flash Messages
    'createSuccess'     => 'Evento adicionado com sucesso! ',
    'updateSuccess'     => 'Evento actualizado com sucesso! ',
    'deleteSuccess'     => 'Evento excluido com sucesso! ',
    'deleteSelfError'   => 'Infelizmente não podes excluir esse evento! ',

    // Show Event Tab
    'createEvent'             => 'Adicionar Evento',
    'editEvent'               => 'Modificar Evento',
    'deleteEvent'          => 'Excluit Evento',
    'deleteEventMsg'       => 'Tens a certeza que queres excluir este evento?',
    'deleteEvent'             => 'Excluir Evento',
    'eventsBackBtn'           => 'Voltar aos Eventos',
    'successDestroy'     => 'Evento excluido com sucesso.',
    'errorEventNotFound' => 'Evento não encontrado.',

    'backToEvent'      => 'Voltar ao evento',
    'backToEvents'     => 'Voltar aos eventos',
    'showAllEvents'    => 'Mostrar todos eventos',

];

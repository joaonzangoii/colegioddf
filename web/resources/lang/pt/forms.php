<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email'             => 'E-mail',
    'create_user_ph_email'                => 'E-mail',
    'create_user_icon_email'              => 'fa-envelope',

    'create_user_label_username'           => 'Nome de usuário',
    'create_user_ph_username'              => 'Nome de usuário',
    'create_user_icon_username'            => 'fa-user-secret',

    'create_user_label_mobile'           => 'Telemóvel',
    'create_user_ph_mobile'              => 'Móvel',
    'create_user_icon_mobile'            => 'fa-mobile',

    'create_user_label_name'              => 'Nome',
    'create_user_label_reg_num'           => 'Número do Registro',
    'create_user_label_firstname'         => 'Primeiro Nome',
    'create_user_ph_firstname'            => 'Primeiro Nome',
    'create_user_icon_firstname'          => 'fa-user',
    'create_user_ph_select_reg_num'       => 'Selecionador Número do Registro',


    'create_user_label_lastname'           => 'Último Nome',
    'create_user_ph_lastname'              => 'Último Nome',
    'create_user_icon_lastname'            => 'fa-user',

    'create_user_label_password'           => 'Palavra passe',
    'create_user_ph_password'              => 'Palavra passe',
    'create_user_icon_password'            => 'fa-lock',

    'create_user_label_pw_confirmation'    => 'Confirmar Palavra passe',
    'create_user_ph_pw_confirmation'       => 'Confirmar Palavra passe',
    'create_user_icon_pw_confirmation'     => 'fa-lock',

    'create_user_label_location'           => 'Localização do usuário',
    'create_user_ph_location'              => 'Localização do usuário',
    'create_user_icon_location'            => 'fa-map-marker',

    'create_user_label_bio'                => 'Biografia usuário',
    'create_user_ph_bio'                   => 'Biografia usuário',
    'create_user_icon_bio'                 => 'fa-pencil',

    'create_user_label_twitter_username'   => 'Nome de Usuário do Twitter',
    'create_user_ph_twitter_username'      => 'Nome de Usuário do Twitter',
    'create_user_icon_twitter_username'    => 'fa-twitter',

    'create_user_label_github_username'    => 'Nome de usuário do GitHub',
    'create_user_ph_github_username'       => 'Nome de usuário do GitHub',
    'create_user_icon_github_username'     => 'fa-github',

    'create_user_label_career_title'       => 'Ocupação do usuário',
    'create_user_ph_career_title'          => 'Ocupação do usuário',
    'create_user_icon_career_title'        => 'fa-briefcase',

    'create_user_label_education'         => 'Educação',
    'create_user_ph_education'            => 'Educação',
    'create_user_icon_education'          => 'fa-graduation-cap',

    'create_user_label_role'               => 'Função',
    'create_user_ph_role'                  => 'Selecionar Função',
    'create_user_icon_role'                => 'fa-shield',

    'create_user_label_sexo'               => 'Gênero',
    'create_user_ph_sexo'                  => 'Selecionar Gênero',
    'create_user_icon_sexo'                => 'fa-intersex',

    'create_user_label_dob'               => 'Data de nascimento',
    'create_user_ph_dob'                  => 'Selecionar Data de nascimento',
    'create_user_icon_dob'                => 'fa-birthday-cake',

    'create_user_label_classe'          => 'Classe',
    'create_user_ph_classe'             => 'Selecionar Classe',
    'create_user_icon_classe'           => 'fa-book',

    'create_user_label_course'          => 'Curso',
    'create_user_ph_course'             => 'Selecionar Curso',
    'create_user_icon_course'           => 'fa-book',

    'create_user_label_valor_da_matricula' => 'Valor da matricula',
    'create_user_ph_valor_da_matricula'    => 'Valor da matricula',
    'create_user_icon_valor_da_matricula'  => 'fa-money',

    'create_user_label_propina_mensal' => 'Propina mensal',
    'create_user_ph_propina_mensal'    => 'Propina mensal',
    'create_user_icon_propina_mensal'  => 'fa-money',

    'create_user_label_disciplinas' => 'Disciplinas',
    'create_user_ph_disciplinas'    => 'Selecionar Disciplinas',
    'create_user_icon_disciplinas'  => 'fa-book',

    'create_user_label_year'          => 'Ano Lectivo',
    'create_user_ph_year'             => 'Selecionar Ano Lectivo',
    'create_user_icon_year'           => 'fa-calendar-o',

    'create_user_label_guardian'          => 'Encarregado',
    'create_user_ph_guardian'             => 'Selecionar Encarregado',
    'create_user_icon_guardian'           => 'fa-user',

    'create_subject_label_nome'         => 'Nome',
    'create_subject_label_disc_nome'    => 'Disciplina',
    'create_subject_ph_select_nome'     => 'Selecionar Disciplina',
    'create_subject_ph_nome'            => 'Nome',
    'create_subject_icon_nome'          => 'fa-book',

    'create_mark_label_teacher'         => 'Professor',
    'create_mark_ph_select_teacher'     => 'Selecionar Professor',
    'create_mark_label_term'            => 'Trmestre',
    'create_mark_ph_select_term'        => 'Selecionar Trimestre',
    'create_mark_icon_term'             => 'fa-book',

    'create_user_button_text'            => 'Adicionar Usuário',
    'create_subject_button_text'         => 'Adicionar nova Disciplina',
    'create_student_button_text'         => 'Adicionar Estudante',
    'create_mark_button_text'            => 'Adicionar nova nota do Estudante',
    'create_payment_button_text'         => 'Adicionar Pagamento',
    'create_event_button_text'           => 'Adicionar Evento',
    'create_news_button_text'           => 'Adicionar Notícia',

    'create_system_ph_about'         => 'Acerca',
    'create_system_ph_terms'         => 'Termos',
    'create_system-ph-banner-pic'    => 'Foto de capa',
    'create_system-ph-header-logo'    => 'Foto de Cabeçalho',
    'create_system-ph-footer-logo'    => 'Foto de Rodapé',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'              => 'Editar informações do usuário',

    'label-username'                     => 'Nome de usuário',
    'ph-username'                        => 'Nome de usuário',

    'label-useremail'                     => 'E-mail do usuário',
    'ph-useremail'                        => 'E-mail do usuário',

    'label-userrole_id'                    => 'Nível de acesso do usuário',
    'option-label'                         => 'Selecionar um nível',
    'option-user'                          => 'Usuário',
    'option-editor'                        => 'Editor',
    'option-admin'                         => 'Administrador',
    'submit-btn-text'                      => 'Modificar o usuário!',

    'submit-btn-icon'                      => 'fa-save',
    'username-icon'                        => 'fa-user',
    'mobile-icon'                        => 'fa-mobile',
    'useremail-icon'                     => 'fa-envelope-o',

    'create_event_label_name'           => 'Nome',
    'create_event_ph_name'              => 'Nome',
    'create_event_icon_name'            => 'fa-tag',
    'create_event_label_starting_time'  => 'Hora de começo',
    'create_event_ph_starting_time'     => 'Hora de começo',
    'create_event_icon_starting_time'   => 'fa-clock-o',
    'create_event_label_ending_time'    => 'Hora de termino',
    'create_event_ph_ending_time'       => 'Hora de termino',
    'create_event_icon_ending_time'     => 'fa-clock-o',
    'create_event_label_place'          => 'Lugar',
    'create_event_ph_place'             => 'Lugar',
    'create_event_icon_place'           => 'fa-home',
    'create_event_label_date'          => 'Data',
    'create_event_ph_date'             => 'Data',
    'create_event_icon_date'           => 'fa-calendar',
    'create_event_label_description'   => 'Descrição',
    'create_event_ph_description'      => 'Descrição',
    'create_event_icon_description'    => 'fa-info',

    'ph-system-name'                    => 'Nome da escola',
    'ph-system-telephone'               => 'Telefone da escola',
    'ph-system-address'                 => 'Endereço da escola',
    'ph-system-email'                   => 'E-mail da escola',
    'ph-system-description'             => 'Descrição da escola',
    'ph-system-fax'                     => 'Fax',
    'ph-system-coordinates'             => 'Coordenadas',

    'save'                              => 'Salvar',
    'save_changes'                      => 'Salvar Actualizações',

    'create_payment_ph_student'     => 'Selecionar estudante',
    'create_payment_ph_approved'    => 'Selecionar estado',
    'create_payment_ph_amount'      => 'Valor',
    'create_payment_label_approved' =>  'Estado',
    'create_payment_label_attachment' =>  'Comprovativo',
    'create_payment_label_amount'   =>  'Valor',
    'create_payment_label_student'  =>  'Estudante',
    'create_payment_icon_approved'  =>  'fa-tags',
    'create_payment_icon_student'   =>  'fa-user',
    'create_payment_icon_amount'    =>  'fa-money',
    'create_payment_label_month'    =>  'Mês',
    'create_payment_icon_month'     =>  'fa-calendar',
    'create_payment_ph_select_month' => 'Selecionar mês',
    'payments_ph_attachment'        =>  'Comprovativo',

    'create_news_label_title'        => 'Titulo',
    'create_news_label_description'  => 'Descrição',
    'create_news_label_image'  => 'Image',
    'create_news_ph_title'        => 'Titulo',
    'create_news_ph_description'  => 'Descrição',
    'create_news_icon_title'  => 'fa-tags',
    'create_news_icon_description'  => 'fa-info',

];

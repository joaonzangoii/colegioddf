<?php

return [
    // Flash Messages
    'updateError'       => 'Infelizmente não podemos modificar essa imagem!',
    'wrongDateRange'    => 'Série Data Errada.. Escolha um série de datas correctas .',
    'noImages'          => 'Desculpe! Nenhuma imagem encontrada para esta série..',

];

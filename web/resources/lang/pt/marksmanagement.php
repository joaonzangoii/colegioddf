<?php

return [

    // Flash Messages
    'createSuccess'     => 'Nota adicionada com sucesso!',
    'updateSuccess'     => 'Nota atualizada com sucesso!',
    'deleteSuccess'     => 'Nota excluída com sucesso!',
    'createNoMatriculation' => 'Student not matriculated yet!',

    // Show mark Tab
    'editmark'               => 'Modificar nota',
    'deletemark'             => 'Excluir nota',
    'marksBackBtn'           => 'Voltar para notas',
    'marksPanelTitle'        => 'Informação da nota',
    'labelmarkName'          => 'Tipo de nota:',
    'labelCreatedAt'         => 'Criado em:',
    'labelUpdatedAt'         => 'Atualizado em:',
    'labelDeletedAt'         => 'Excluido em',
    'marksDeletedPanelTitle' => 'Informação da nota excluida',
    'marksBackDelBtn'        => 'Voltar para notas excluídas',

    'successRestore'    => 'nota restaurada com sucesso.',
    'successDestroy'    => 'Registro da nota destruído com sucesso.',
    'errormarkNotFound' => 'Nota não encontrada.',

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modals Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which modals share.
    |
    */

    // Default Save Modal;
    'confirm_modal_title_text'                  => 'Confirmar Salvamento',
    'confirm_modal_title_std_msg'               => 'Confirme o seu pedido..',

    // Confirm Save Modal;
    'confirm_modal_button_save_text'            => 'Salvar Alterações',
    'confirm_modal_button_save_icon'            => 'fa-save',
    'confirm_modal_button_cancel_text'          => 'Cancelar',
    'confirm_modal_button_cancel_icon'          => 'fa-times',
    'edit_user__modal_text_confirm_title'       => 'Confirmar Salvamento',
    'edit_user__modal_text_confirm_message'     => 'Confirm as suas Alterações.',

    // Form Modal
    'form_modal_default_title'                  => 'Confirmar',
    'form_modal_default_message'                => 'Por favor confirme',
    'form_modal_default_btn_cancel'             => 'Cancelar',
    'form_modal_default_btn_submit'             => 'Confirmar',

    'delete_modal_title_text'                  => 'Confirmar Exclusão',
    'delete_modal_title_std_msg'               => 'Confirme o seu pedido..',

    'edit_modal_title_text'                  => 'Confirmar Edição',
    'edit_modal_title_std_msg'               => 'Confirme o seu pedido..',
];

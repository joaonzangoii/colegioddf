<?php

return [
  'months' => [
    "01" => "Janeiro",
    "02" => "Fevereiro",
    "03" => "Março",
    "04" => "Abril",
    "05" => "Maio",
    "06" => "Junho",
    "07" => "Julho",
    "08" => "Agosto",
    "09" => "Setembro",
    "10" => "Outubro",
    "11" => "Novembro",
    "12" => "Dezembro "
  ],
  'monthsShort' => [
    "01" => "Jan",
    "02" => "Fev",
    "03" => "Mar",
    "04" => "Abr",
    "05" => "Mai",
    "06" => "Jun",
    "07" => "Jul",
    "08" => "Ago",
    "09" => "Set",
    "10" => "Out",
    "11" => "No",
    "12" => "Dez "
  ],
  'days' => [
    "01" => "Segunda",
    "02" => "Terça",
    "03" => "Quarta",
    "04" => "Quinta",
    "05" => "Sexta",
    "06" => "Sábado",
    "07" => "Domingo",
  ],
  'daysShort' => [
    "01" => "Seg",
    "02" => "Ter",
    "03" => "Qua",
    "04" => "Qui",
    "05" => "Sex",
    "06" => "Sáb",
    "07" => "Dom",
  ]
];

<?php

return [

    // Flash Messages
    'createSuccess'     => 'Notícia adicionado com sucesso!',
    'updateSuccess'     => 'Notícia actualizado com sucesso!',
    'deleteSuccess'     => 'Notícia excluido com sucesso!',
    'deleteSelfError'   => 'Infelizmente não podes excluir esse noticia!',

    // Show News Tab
    'createNews'             => 'Adicionar Notícia',
    'editNews'               => 'Modificar Notícia',
    'deleteNews'          => 'Excluit Notícia',
    'deleteNewsMsg'       => 'Tens a certeza que queres excluir esta noticia?',
    'deleteNews'             => 'Excluir Notícia',
    'newsBackBtn'           => 'Voltar as Notícias',
    'successDestroy'     => 'Notícia excluida com sucesso.',
    'errorNewsNotFound' => 'Notícia não Encontrada.',

    'backToNews'      => 'Voltar a Noticia',
    'backToNews'     => 'Voltar as Noticias',
    'showAllNews'    => 'Mostrar todas Noticias',
    'panelTitle'  => 'Noticia',

];

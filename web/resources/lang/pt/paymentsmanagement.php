<?php

return [

    // Flash Messages
    'createSuccess'     => 'Pagamento adicionado com sucesso! ',
    'updateSuccess'     => 'Pagamento actualizado com sucesso! ',
    'deleteSuccess'     => 'Pagamento excluido com sucesso! ',
    'deleteSelfError'   => 'Infelizmente não podes excluir esse pagamento! ',
    'alreadyAdded'      => 'Pagamentos já criados',

    // Show Payment Tab
    'createPayment'               => 'Adicionar Pagamento',
    'editPayment'               => 'Modificar Pagamento',
    'deletePayment'             => 'Excluir Pagamento',
    'subjectsBackBtn'           => 'Voltar aos Pagamentos',
    'subjectsPanelTitle'        => 'Informação do Pagamento',
    'labelPaymentName'          => 'Tipo de pagamento:',
    'labelName'              => 'Nome:',
    'labelAmount'            => 'Valor:',
    'labelTerm'              => 'T:rimestre',
    'labelMonth'             => 'Mês:',
    'labelYear'             => 'Ano:',
    'labelStatus'            => 'Status:',
    'labelAccessLevel'       => 'Access',
    'labelCreatedAt'         => 'Created At:',
    'labelUpdatedAt'         => 'Updated At:',
    'subjectsDeletedPanelTitle' => 'Deleted Payment Information',
    'subjectsBackDelBtn'        => 'Back to Deleted Payments',

    'successDestroy'    => 'Pagamento excluido com sucesso.',
    'errorPaymentNotFound' => 'Pagamento não encontrado.',

    'labelPaymentLevel'     => 'Level',
    'labelPaymentLevels'    => 'Levels',
    'backToPayment'    => 'Voltar ao pagamento',
    'backToPayments'    => 'Voltar aos pagamentos',
    'showAllPayments'    => 'Mostrar todos pagamentos',

];

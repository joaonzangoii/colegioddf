<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
    */

    'templateTitle'                => 'Modificar perfil',

    // profile erros
    'notYourProfile'              => 'Este não é o perfil que você está procurando.',
    'notYourProfileTitle'         => 'Hmmm, algo deu errado...',
    'noProfileYet'                => 'Ainda não há perfil.',

    // USER profile title
    'showProfileUsername'          => 'Nome de usuário',
    'showProfileFirstName'         => 'Primeiro nome',
    'showProfileLastName'          => 'Último nome',
    'showProfileEmail'             => 'Endereço de e-mail',
    'showProfileLocation'          => 'Localização',
    'showProfileBio'               => 'Biografia',
    'showProfileTheme'             => 'Tema',
    'showProfileTwitterUsername'   => 'Nome de usuário do Twitter',
    'showProfileGitHubUsername'    => 'Nome de usuário do Github',

    // USER profile page
    'showProfileTitle'            => 'Perfil de :username',

    // USER EDIT profile page
    'editProfileTitle'            => 'Configurações de perfil',

    // User edit profile form
    'label-theme'                 => 'Seu tema:',
    'ph-theme'                    => 'Selecionar o seu tema',

    'label-location'             => 'Sua localização:',
    'ph-location'                => 'Insira sua localização',

    'label-bio'                    => 'Sua biografia:',
    'ph-bio'                       => 'Digite sua biografia',

    'label-github_username'        => 'Seu nome de usuário GitHub:',
    'ph-github_username'           => 'Digite seu nome de usuário do GitHub',

    'label-twitter_username'     => 'Seu nome de usuário do Twitter:',
    'ph-twitter_username'        => 'Digite seu nome de usuário do Twitter',

    // User Account Settings Tab
    'editTriggerAlt'               => 'Alternar menu de usuário',
    'editAccountTitle'             => 'Configurações da conta',
    'editAccountAdminTitle'        => 'Administração de conta',
    'updateAccountSuccess'         => 'Sua conta foi atualizada com sucesso',
    'submitProfileButton'          => 'Salvar alterações',

    // User Account Admin Tab
    'submitPWButton'               => 'Atualizar Senha',
    'changePwTitle'                => 'Mudar senha',
    'changePwPill'                 => 'Mudar senha',
    'deleteAccountPill'            => 'Excluir conta',
    'updatePWSuccess'              => 'Sua senha foi atualizada com sucesso',

    // Delete Account Tab
    'deleteAccountTitle'           => 'Excluir conta',
    'deleteAccountBtn'             => 'Excluir minha conta',
    'deleteAccountBtnConfirm'      => 'Excluir minha conta',
    'deleteAccountConfirmTitle'    => 'Confirmar exclusão da conta',
    'deleteAccountConfirmMsg'      => 'Tem certeza de que deseja excluir a sua conta?',
    'confirmDeleteRequired'        => 'É necessária confirmar a exclusão da conta',

    'errorDeleteNotYour'        => 'Você só pode deletar seu próprio perfil',
    'successUserAccountDeleted' => 'Sua conta foi deletada',

    // Messages
    'updateSuccess'                => 'Seu perfil foi atualizado com sucesso',
    'submitButton'                 => 'Salvar alterações',

    // Restore User Account
    'errorRestoreUserTime'        => 'Desculpe, a conta não pode ser restaurada',
    'successUserRestore'          => 'Bem-vindo de volta :username! Conta restaurada com sucesso',
];

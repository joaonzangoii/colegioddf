<?php

return [
    // Flash Messages
    'createSuccess'     => 'Informações do site criadas com sucesso!',
    'updateSuccess'     => 'Informações atualizadas do site atualizadas!',
    'deleteSuccess'     => 'Informações do site excluídas com sucesso!',
];

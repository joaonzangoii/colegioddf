<?php

return [

  // Flash Messages
  'createSuccess'     => 'Estudante adicionado com sucesso!',
  'updateSuccess'     => 'Estudante modificado com sucesso!',
  'deleteSuccess'     => 'Estudante excluido com sucesso!',
  'deleteSelfError'   => 'Não podes excluir a ti próprio!',

  // Show Student Tab
  'viewProfile'            => 'Ver Perfil',
  'editStudent'            => 'Modificar estudante',
  'deleteStudent'          => 'Excluir estudante',
  'deleteStudentMsg'       => 'Tens a certeza que queres excluir esse Estudante?',
  'studentsBackBtn'        => 'Voltar aos estudantes',
  'studentsPanelTitle'     => 'Informação do estudante',
  'labelStudentName'       => 'Nome do estudante:',
  'labelName'              => 'Nome:',
  'labelFirstName'         => 'Primeiro nome:',
  'labelLastName'          => 'Último Nome:',
  'labelEmail'             => 'Email:',
  'labelRole'              => 'Função:',
  'labelStatus'            => 'Estado:',
  'labelAccessLevel'       => 'Acesso',
  'labelPermissions'       => 'Permissões:',
  'labelCreatedAt'         => 'Criado em:',
  'labelUpdatedAt'         => 'Atualizado em:',
  'labelIpEmail'           => 'E-mail de Registro IP:',
  'labelIpConfirm'         => 'IP de Confirmação:',
  'labelIpSocial'          => 'IP do registro social:',
  'labelIpAdmin'           => 'IP de inscrição do administrador:',
  'labelIpUpdate'          => 'IP da Última atualização:',
  'labelDeletedAt'         => 'Excluido em',
  'labelIpDeleted'         => 'IP excluido em:',
  'studentsDeletedPanelTitle' => 'Informação do estudante excluido',
  'studentsMarksPanelTitle'   => 'Notas',
  'studentsBackDelBtn'        => 'Voltar aos estudantes excluidos',
  'studentsSubjectsBackBtn'   => 'Voltar às disciplinas',
  'studentsMatriculationsBackBtn' => 'Voltar à matricula',
  'studentsPaymentsBackBtn' => 'Voltar aos pagamentos',

  'successRestore'    => 'Estudante restaurado com sucesso.',
  'successDestroy'    => 'Registro de estudantes destruído com sucesso.',
  'errorStudentNotFound' => 'Estudante não encontrado.',

  'labelStudentLevel'  => 'Niveis',
  'labelStudentLevels' => 'Niveis',

  'attrStdNumber'         => 'Número de Estudante',
  'attrName'              => 'Nome',
  'attrFirstName'         => 'Primeiro nome',
  'attrLastName'          => 'Último Nome',
  'attrEmail'             => 'Email',
  'attrRole'              => 'Função',
  'attrGuardianName'      => 'Encarregado',
  'attrStudentsNumber'    => 'Estudantes',

  'changePassword'       => 'Modificar Palavra passe',
];

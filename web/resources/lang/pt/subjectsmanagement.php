<?php

return [

    // Flash Messages
    'createSuccess'     => 'Disciplina adicionada com sucesso! ',
    'updateSuccess'     => 'Disciplina modificada com sucesso! ',
    'deleteSuccess'     => 'Disciplina excluida com sucesso! ',

    // Show Subject Tab
    'editSubject'               => 'Modificar Disciplina',
    'deleteSubject'             => 'Excluir Disciplina',
    'subjectsBackBtn'           => 'Voltar as Disciplinas',
    'subjectsPanelTitle'        => 'Informação da Disciplina',
    'labelSubjectName'          => 'Nome da Disciplina:',
    'labelDeletedAt'         => 'Excluido em',
    'subjectsDeletedPanelTitle' => 'Informação da Disciplina Excluida',
    'subjectsBackDelBtn'        => 'Voltar as Disciplinas Excluidas',

    'successRestore'    => 'Subject successfully restored.',
    'successDestroy'    => 'Subject record successfully destroyed.',
    'errorSubjectNotFound' => 'Disciplina não encontrada.',

    'showSubject'  => 'Mostrar Disciplina',
    'showAllMarksFor'  => 'Mostrar Notas de',
    'showAllSubjects'  => 'Mostrar todas Disciplinas',
    'showSubjectsManagementMenu'  => 'Mostrar enu de Gerência das Disciplinas',

    'backToSubject'    => 'Voltar a Disciplina',
    'backToSubjects'    => 'Voltar as Disciplinas',

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Themes Management Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // Messages
    'createSuccess'     => 'Tema criado! ',
    'updateSuccess'     => 'Tema actualizado! ',
    'deleteSuccess'     => 'Tema excluido! ',
    'deleteSelfError'   => 'Você não pode excluir o tema padrão. ',

    // Shared
    'statusLabel'       => 'Estado do tema',
    'statusEnabled'     => 'Ativado',
    'statusDisabled'    => 'Desativado',

    'nameLabel'            => 'Nome do tema*',
    'namePlaceholder'      => 'Inserir Nome do tema',

    'linkLabel'            => 'CSS Link do tema *',
    'linkPlaceholder'      => 'Inserir CSS Link',

    'notesLabel'        => 'Notas do Tema',
    'notesPlaceholder'  => 'Inserir notas do tema',

    'themes'            => 'Temas',

    // Add Theme
    'btnAddTheme'        => 'Adicionar tema',

    // Edit Theme
    'editTitle'            => 'Editando o tema:',
    'editSave'             => 'Salvar alterações de tema',

    // Show Theme
    'showHeadTitle'        => 'Tema',
    'showTitle'            => 'Informação do tema',
    'showBackBtn'          => 'Voltar aos temas',
    'showUsers'            => 'Úsuarios do tema',
    'showStatus'           => 'Estado',
    'showLink'             => 'CSS Link',
    'showNotes'            => 'Notas',
    'showAdded'            => 'Adicionado',
    'showUpdated'          => 'Actualizado',
    'confirmDeleteHdr'     => 'Excluir tema',
    'confirmDelete'        => 'Tem certeza de que deseja excluir este tema?',

    // Show Themes
    'themesTitle'          => 'Mostrar Todos Temas',
    'themesStatus'         => 'Estado',
    'themesUsers'          => 'Úsuarios',
    'themesName'           => 'Nome',
    'themesLink'           => 'CSS Link',
    'themesActions'        => 'Acções',
    'themesBtnShow'        => 'Mostrar tema',
    'themesBtnEdit'        => 'Modificar tema',
    'themesBtnDelete'      => '',
    'themesBtnEdits'       => '',

];

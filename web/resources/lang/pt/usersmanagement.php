<?php

return [

    // Flash Messages
    'createSuccess'     => 'Úsuario adicionado com sucesso!',
    'updateSuccess'     => 'Úsuario modificado com sucesso!',
    'deleteSuccess'     => 'Úsuario excluido com sucesso!',
    'deleteSelfError'   => 'Não podes excluir a ti próprio!',

    // Show User Tab
    'viewProfile'            => 'Ver Perfil',
    'editUser'               => 'Modificar Úsuario',
    'deleteUser'             => 'Excluir Úsuario',
    'usersBackBtn'           => 'Voltar aos úsuarios',
    'usersPanelTitle'        => 'User Information',
    'labelUserName'          => 'Nome de Úsuario:',
    'labelEmail'             => 'E-mail:',
    'labelFirstName'         => 'Primeiro nome:',
    'labelLastName'          => 'Último nome:',
    'labelRole'              => 'Função:',
    'labelStatus'            => 'Estado:',
    'labelAccessLevel'       => 'Access',
    'labelPermissions'       => 'Permissões:',
    'labelCreatedAt'         => 'Criado em:',
    'labelUpdatedAt'         => 'Atualizado em:',
    'labelIpEmail'           => 'E-mail de Registro IP:',
    'labelIpConfirm'         => 'IP de Confirmação:',
    'labelIpSocial'          => 'IP do registro social:',
    'labelIpAdmin'           => 'IP de inscrição do administrador:',
    'labelIpUpdate'          => 'IP da Última atualização:',
    'labelDeletedAt'         => 'Excluido em',
    'labelIpDeleted'         => 'IP excluido em:',
    'usersDeletedPanelTitle' => 'Informação do Úsuario Excluido',
    'usersBackDelBtn'        => 'Voltar para Úsuarios Excluidos',

    'successRestore'    => 'User successfully restored.',
    'successDestroy'    => 'User record successfully destroyed.',
    'errorUserNotFound' => 'Úsuario não encontrado.',

    'labelUserLevel'     => 'Niveis',
    'labelUserLevels'    => 'Niveis',
    'createUser'       => 'Criar Úsuario',
    'createTeacher'       => 'Criar Professor',
    'createGuardian'       => 'Criar Encarregado',
    'showUser'  => 'Mostrar Úsuario',
    'showUsers'  => 'Mostrar Úsuarios',
    'createTeacher'   => 'Adicionar Professor',
    'showAllTeachers'  => 'Todos Professores',
    'createGuardian'   => 'Adicionar Encarregado',
    'showAllGuardians'  => 'Todos Encarregados',
    'showAllUsers'  => 'Mostrar todos Úsuarios',
    'showDeletedUsers'  => 'Mostrar Úsuarios Excluidos',
    'restoreUser'  => 'Restaurar Úsuario',
    'deleteUser'  => 'Excluir Úsuario',
    'deleteUserMsg'  => 'Tens a certeza que queres excluir esse Úsuario?',
    'permanentlyDeleteUser'  => 'Excluir Úsuario Permanentemente',
    'permanentlyDeleteUserMsg'  => 'Tens a certeza que desejas excluir permanentemente a esse Úsuario?',
    'backToUser'    => 'Voltar ao Úsuario',
    'backToUsers'    => 'Voltar aos Úsuarios',

    'changePassword'       => 'Modificar Palavra passe',

];

@extends('layouts.auth')
@section('content')
  @php
    $siteinfo = App\Models\SiteInfo::latest()->first();
  @endphp
  <div class="main-content-wrapper">
			<div class="login-area">
				<div class="login-header">
					<a href="{{route('home')}}" class="logo">
						<img src="{{asset($siteinfo->logo_cabecalho)}}" height="60" alt="">
					</a>
					<h2 class="title">{{$siteinfo->nome}}</h2>
				</div>
				<div class="login-content">
				  <div class="row">
  					<form method="post"
                  role="form"
                  id="form_login"
                  action="{{ route('login') }}">
              {{ csrf_field() }}
  						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
  							<input type="email"
                       class="input-field"
                       name="email"
                       placeholder="{{trans('auth.email')}}"
                       autocomplete="on"
                       id="email">
                 @if ($errors->has('email'))
                   <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                   </span>
                 @endif
  						</div>
  						<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
  							<input type="password"
                       class="input-field"
                       name="password"
                       placeholder="{{trans('auth.password')}}"
                       id="password">
               @if ($errors->has('password'))
                 <span class="help-block">
                   <strong>{{ $errors->first('password') }}</strong>
                 </span>
               @endif
  						</div>
  						<button type="submit" class="btn btn-primary btn-no-padding">
                {{trans('auth.login')}}<i class="fa fa-lock"></i>
              </button>

              <div class="row" style="margin-bottom: 20px;">
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"
                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                              @lang('auth.rememberMe')
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <a class="btn btn-link btn-no-padding btn-forgot-password"
                       href="{{ route('password.request') }}">
                       @lang('auth.forgot')
                    </a>
                  </div>
                </div>
                {{-- <p class="text-center margin-bottom-3">
                  Or Login with
                </p>
                @include('partials.admin.socials-icons') --}}
              </div>
  					</form>
          </div>
          {{-- <div class="login-bottom-links">
            <a href="index.php?login/forgot_password" class="link">?</a>
          </div> --}}
          {{-- @include('partials.admin.login-buttons') --}}
				</div>
			</div>
			<div class="image-area"></div>
		</div>
@endsection

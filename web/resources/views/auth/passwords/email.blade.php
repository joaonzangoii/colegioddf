@extends('layouts.auth')
@section('template_linked_css')
  <style type="type/css">

  </style>
@endsection
@section('content')
  @php
    $siteinfo = App\Models\SiteInfo::latest()->first();
  @endphp
  <div class="main-content-wrapper">
			<div class="login-area">
				<div class="login-header">
					<a href="{{route('home')}}" class="logo">
						<img src="{{asset($siteinfo->logo_cabecalho)}}" height="60" alt="">
					</a>
					<h2 class="title">{{$siteinfo->nome}}</h2>
				</div>
				<div class="login-content">
				  <div class="row">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif
  					<form method="post"
                  role="form"
                  id="form_login"
                  action="{{ route('password.email') }}">
              {{ csrf_field() }}
  						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
  							<input type="email"
                       class="input-field"
                       name="email"
                       placeholder="{{trans('auth.email')}}"
                       autocomplete="on"
                       id="email">
                 @if ($errors->has('email'))
                   <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                   </span>
                 @endif
  						</div>
  						<button type="submit" class="btn btn-primary btn-no-padding">
                @lang('auth.sendResetLink')<i class="fa fa-envelope"></i>
              </button>

              <div class="row" style="margin-bottom: 20px;">
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="login" type="button" class="btn btn-primary"
                       href="{{ route('login') }}">
                      @lang('auth.login')<i class="fa fa-lock"></i>
                    </button>
                  </div>
                </div>
              </div>
  					</form>
          </div>
				</div>
			</div>
			<div class="image-area"></div>
		</div>
@endsection
@section('footer_scripts')
  <script type="text/javascript">
  $(document).ready(function() {
    $('#login').on('click', function(){
      window.location = $('#login').attr('href');
    });
  });
</script>
@endsection

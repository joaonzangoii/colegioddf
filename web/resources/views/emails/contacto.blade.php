@component('emails.partials.layout')
    {{-- Header --}}
    @slot('header')
      @component('emails.partials.header', ['url' => config('app.url')])
        {{ config('app.name') }}
      @endcomponent
    @endslot

    @slot('body')
      Muito obrigado por nos contactar entraremos em
      contacto o mais rápido possivel
    @endslot

    {{-- Footer --}}
    @slot('footer')
      @component('emails.partials.footer')
        {{ date('Y') }}©  {{ config('app.name') }}. @lang('titles.allRightsReserved')
      @endcomponent
    @endslot
@endcomponent

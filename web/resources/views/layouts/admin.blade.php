<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  {{-- CSRF Token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
  <meta name="description" content="">
  <meta name="author" content="Jeremy Kenedy">
  <link rel="shortcut icon" href="/favicon.ico">
  {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  {{-- Fonts --}}
  @yield('template_linked_fonts')
  {{-- Styles --}}
  <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
  @yield('template_linked_css')
  <style type="text/css">
    @yield('template_fastload_css')
    @if(Auth::User()
      && (Auth::User()->profile)
      && (Auth::User()->profile->avatar_status == 0))
        .user-avatar-nav {
          background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
          background-size: auto 100%;
        }
    @endif

    .navbar-default .navbar-nav>.active>a,
    .navbar-default .navbar-nav>.active>a:focus,
    .navbar-default .navbar-nav>.active>a:hover {
      color: #fff;
      background-color: #333;
    }
  </style>
  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>

  @if (Auth::User() && (Auth::User()->profile) && $theme->link != null && $theme->link != 'null')
      <link rel="stylesheet" type="text/css" href="{{ $theme->link }}">
  @endif
  <link href="{{mix('/css/admin.css')}}" rel="stylesheet" type="text/css">
  {{-- <link href="{{asset('/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css">
  <link href="{{ asset('/css/select2.min.css')}}" rel="stylesheet" />
  <link href="{{ asset('/css/select2-bootstrap.css')}}" rel="stylesheet" />
  <link href="{{ asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" /> --}}
  @yield('head')
  @include('styles.sidebar')
</head>
<body>
  <div id="app">
    @if (!Auth::guest())
      @include('partials.admin.sidebar')
    @endif
    <div class="container-radim" style="@if (Auth::guest()) margin-left: 0px; @endif">
      @include('partials.admin.nav')
      <div id="page-content-wrapper">
        <div class="container-fluid">
          @include('partials.admin.form-status')
        </div>
        {{-- <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a> --}}
        @yield('content')
      </div>
    </div>
  </div>
  {!! HTML::script('https://maps.googleapis.com/maps/api/js?key='.
                    env("GOOGLEMAPS_API_KEY").'&libraries=places&dummy=.js',
                    ['type' => 'text/javascript']) !!}
  <script src="{{ mix('/js/app.js') }}"></script>
  <script src="{{ mix('/js/admin.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      // $("select" ).select2({
      //   theme: "bootstrap"
      // });
      $('.select2-container--bootstrap').attr("style", "");
      $(":file").filestyle({text: "Find file", buttonBefore: true});
    });
  </script>
  @yield('footer_scripts')
  @include('scripts.sidebar')
</body>
</html>

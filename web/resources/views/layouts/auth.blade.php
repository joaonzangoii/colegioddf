<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
  {{ trans('app.appTitle') }}
  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta  id="token" name="_token" content="{{ csrf_token() }}"/>
  <!-- Scripts -->
  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
  ]) !!};
  </script>
  <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="{{asset('website/login_page/img/favicon.png')}}">
  <link rel="stylesheet"    href="{{asset('website/css/bootstrap.css')}}">
  <link rel="stylesheet"    href="{{asset('website/login_page/css/bootstrap-social.css')}}">
  <link rel="stylesheet"    href="{{asset('website/css/font-awesome.min.css')}}">
  <link rel="stylesheet"    href="{{asset('website/login_page/css/normalize.css')}}">
  <link rel="stylesheet"    href="{{asset('website/login_page/css/main.css')}}">
  <link rel="stylesheet"    href="{{asset('website/login_page/css/style.css')}}">
  <script src="{{asset('website/js/modernizr-2.8.3.min.js')}}"></script>
  <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  @yield('stylesheets')
  <style>
    .login-area {
      padding-top: 50px;
    }

    .login-area span
    {
      text-align: left;
    }

    .col-md-6 .col-md-offset-4 {
      padding-left: 0px;
    }

    .btn-no-padding {
      padding: 0px;
    }

    .btn-forgot-password {
      color: #ffffff;
    }

    .btn-forgot-password:hover {
      color: #ff0000;
    }

    .btn-primary,
    .btn-primary:hover {
       width: 100%;
       padding: 14px 10px;
       background: #2191bf;
       box-shadow: none;
       color: #ffffff;
       /*border: 1px solid #2d86ab;*/
       margin-bottom: 30px;
     }

    .btn-primary:active {
      color: #fff;
      background-color: #0b0d0f;
      border: 1px solid #2d86ab;
      outline: thin dotted;
      outline: 5px auto -webkit-focus-ring-color;
      outline-offset: -2px;
    }
  </style>
</head>
<body>
  <div>
    @yield('content')
  </div>
  {!! Form::open(['route' => 'logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
  <button type="submit">Logout</button>
  {!! Form::close() !!}
  <script src="{{asset('website/js/jquery-1.12.0.min.js')}}"></script>
  <script src="{{asset('website/js/bootstrap-notify.js')}}"></script>
  @yield('footer_scripts')
</body>
</html>

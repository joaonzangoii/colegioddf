<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>
  {{ trans('app.appTitle') }}
  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta  id="token" name="_token" content="{{ csrf_token() }}"/>
  <!-- Scripts -->
  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
  ]) !!};
  </script>
  <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/print.css')}}" type="text/css" media="print"  />
  <link rel="stylesheet" href="{{asset('website/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/magnific-popup.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/owl.theme.default.min.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/main.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/animate.css')}}">
  <link rel="stylesheet" href="{{asset('website/frontend/css/responsive.css')}}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"
        rel="stylesheet"
        media="all">
  <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600,600i,700,700i|Lato:400,700"
        rel="stylesheet">
  <script src="{{asset('website/js/jquery-2.1.4.min.js')}}"></script>
  <script src="{{asset('website/js/modernizr-2.8.3.min.js')}}"></script>
  {{-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="{{mix('css/vendor.css')}}">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]--> --}}
  @yield('stylesheets')
  @yield('css')
  <style type="text/css">
    .icon-small
    {
      height:18px;
      margin:0;
      padding:0;
    }

    .icon-medium
    {
      height:22px;
      margin:0;
      padding:0;
    }

    ul.list-inline li a:hover,
    ul.list-inline li a:focus,
    ul.list-inline li a:active,
    ul.list-inline li.active {
     color: #ffffff;
    }

    ul.list-inline li a {
      color: #b1cbe2;
    }
    ul.list-inline li.active a {
      color: #ffffff;
    }

    .logo-container a {
      background-color: #F5F5F5;
      color: #555;
      text-align: center;
      font-size: 2em;
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  		text-shadow: 5px 5px 3px #333;
      font-weight: 900;
      text-shadow: 0.075em 0.08em 0.1em rgba(0, 0, 0, 1);
  	}

    ul.list-inline li a {
      font-weight: 900;
      padding: 10px;
      box-shadow: 5px 5px 3px #333;
      background: #961e26;
    }
  </style>
  @if( Request::is( Config::get('chatter.routes.home')) )
      <title>Title for your forum homepage -  Website Name</title>
  @elseif( Request::is( Config::get('chatter.routes.home') . '/' . Config::get('chatter.routes.category') . '/*' ) && isset( $discussion ) )
      <title>{{ $discussion->category->name }} - Website Name</title>
  @elseif( Request::is( Config::get('chatter.routes.home') . '/*' ) && isset($discussion->title))
      <title>{{ $discussion->title }} - Website Name</title>
  @endif
</head>
<body>
  <div>
    @include('partials.landing.header')
    @yield('content')
  </div>
  {!! Form::open(['route' => 'logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
  <button type="submit">Logout</button>
  {!! Form::close() !!}
  @include('partials.landing.footer')
</body>
</html>

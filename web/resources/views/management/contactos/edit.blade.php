@extends('layouts.admin')
@section('template_title')
  @lang('paymentsmanagement.editPayment') {{ $payment->estudante->nome }}
@endsection
@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }

    #comprovativo_emb{
      width: 500px;
      height: 600px;
    }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>@lang('paymentsmanagement.editPayment'): </strong>  {{ $payment->estudante->nome }}
            <a href="{{ route('pagamentos.show', $payment->id) }}" class="btn btn-primary btn-xs pull-right" style="margin-left: 1em;">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('paymentsmanagement.backToPayment')
            </a>
            <a href="{{ route('pagamentos') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('paymentsmanagement.backToPayments')
            </a>
          </div>
            {!! Form::model($payment, ['action' => ['PaymentsManagementController@update', $payment->id],
              'method' => 'PUT',
              'files' => true]) !!}
              {!! csrf_field() !!}
              <div class="panel-body">
                <div class="form-group has-feedback row {{ $errors->has('estudante_id') ? ' has-error ' : '' }}">
                  {!! Form::label('estudante_id', trans('forms.create_payment_label_student'),
                    array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      <select class="form-control" name="estudante_id" id="estudante_id">
                        <option value="{{ old('estudante_id') }}">{{ trans('forms.create_payment_ph_student') }}</option>
                        @if ($students->count())
                          @foreach($students as $key_=>$student)
                            <option value="{{ $student->id }}" {{ $currentStudent ==  $student->id ? 'selected="selected"' : '' }}>
                              {{ $student->nome }}
                            </option>
                          @endforeach
                        @endif
                      </select>
                      <label class="input-group-addon" for="estudante_id">
                        <i class="fa fa-fw {{ trans('forms.create_payment_icon_student') }}"
                           aria-hidden="true"></i>
                      </label>
                    </div>
                    @if ($errors->has('estudante_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('estudante_id') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group has-feedback row {{ $errors->has('aprovado') ? ' has-error ' : '' }}">
                  {!! Form::label('aprovado', trans('forms.create_payment_label_approved'), array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      <select class="form-control" name="aprovado" id="aprovado">
                        <option value="{{ old('aprovado') }}">{{ trans('forms.create_payment_ph_approved') }}</option>
                        @if ($approveds->count())
                          @foreach($approveds as $key=>$approved)
                            <option value="{{ $key }}" {{ $currentState ==  $key ? 'selected="selected"' : '' }}>{{ $approved }}</option>
                          @endforeach
                        @endif
                      </select>
                      <label class="input-group-addon" for="approved">
                        <i class="fa fa-fw {{ trans('forms.create_payment_icon_approved') }}" aria-hidden="true"></i>
                      </label>
                    </div>
                    @if ($errors->has('aprovado'))
                      <span class="help-block">
                        <strong>{{ $errors->first('aprovado') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group has-feedback row {{ $errors->has('mes') ? ' has-error ' : '' }}">
                  {!! Form::label('mes', trans('forms.create_payment_label_month'),
                                  array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      <select name ='mes' class = "form-control">
                        <option value="{{ old('mes') }}">
                          {{ trans('forms.create_payment_ph_select_month') }}
                        </option>
                        @foreach(trans('months.months') as $key => $mes)
                          @php
                          // intval(ltrim($key, '0'));
                          $month = "";
                          if($payment->mes === trans('months.months')[$key] ) {
                            $month = $key;
                          }
                          if(old('mes') === $key OR  $month === $key)
                          {
                            $selected = 'selected="selected"';
                          }
                          else {
                            $selected = '';
                          }
                          @endphp
                          <option value="{{ $key}}" {{ $selected }}>
                            {{$mes}}
                          </option>
                        @endforeach
                      </select>
                      <label class="input-group-addon" for="mes">
                        <i class="fa fa-fw {{ trans('forms.create_payment_icon_month') }}"
                           aria-hidden="true"></i>
                      </label>
                    </div>
                    @if ($errors->has('mes'))
                      <span class="help-block">
                         <strong>{{ $errors->first('mes') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group has-feedback row {{ $errors->has('comprovativo') ? ' has-error ' : '' }}">
   								{!! Form::label('comprovativo',  trans('forms.create_payment_label_attachment'),
                    array('class' => 'col-md-3 control-label')); !!}
   								<div class="col-md-9">
   									<div class="image-preview">
                      <embed id="comprovativo_emb"
                              src="{{ asset($payment->comprovativo)}}"
                              type="application/pdf">
                          <a href="{{ asset($payment->comprovativo) }}">{{ $payment->comprovativo }}</a>
                      </embed>
   									</div>
   									<div class="input-group">
   										{!! Form::file('comprovativo', ['id' => 'comprovativo',
   																	  'class' => 'form-control required borrowerImageFile',
   																		'data-errormsg' => 'PhotoUploadErrorMsg',
   																		'placeholder' => trans('forms.create_payment-ph-comprovativo')
   																		]) !!}
   										<label class="input-group-addon" for="comprovativo">
   											<i class="fa fa-fw fa-file " aria-hidden="true"></i>
   										</label>
   									</div>
   									@if ($errors->has('comprovativo'))
   										<span class="help-block">
   											<strong>{{ $errors->first('comprovativo') }}</strong>
   										</span>
   									@endif
   								</div>
   							</div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes',
                     array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save',
                     'type' => 'button', 'data-toggle' => 'modal',
                     'data-target' => '#confirmSave',
                     'data-title' => trans('modals.edit_user__modal_text_confirm_title'),
                     'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-save')
  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
  @include('scripts.file-preview')
@endsection

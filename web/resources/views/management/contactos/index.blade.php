@extends('layouts.admin')

@section('template_title')
  @lang('contactsmanagement.showAllContacts')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .contactos-table {
        border: 0;
    }
    .contactos-table tr td:first-child {
        padding-left: 15px;
    }
    .contactos-table tr td:last-child {
        padding-right: 15px;
    }
    .contactos-table.table-responsive,
    .contactos-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('contactsmanagement.showAllContacts')
              <div class="btn-group pull-right btn-group-xs">
                <button type="button"
                        class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                  <span class="sr-only">
                      @lang('titles.showingStudentsManagementMenu')
                  </span>
                </button>
                <ul class="dropdown-menu">
                  @role('admin')

                 @endrole
                </ul>
              </div>
            </div>
          </div>
          <div class="panel-body">
            @if(count($contactos) === 0)
              <tr>
                <p class="text-center margin-half">
                  @lang('titles.noItemsFound')
                </p>
              </tr>
            @else
              <div class="table-responsive contactos-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>@lang('titles.name')</th>
                      <th>@lang('titles.telephone')</th>
                      <th>@lang('titles.email')</th>
                      <th>@lang('titles.CreatedAt')</th>
                      <th>@lang('titles.UpdatedAt')</th>
                      <th>@lang('titles.actions')</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($contactos as $contacto)
                      <tr>
                        <td>{{$contacto->id}}</td>
                        <td>{{$contacto->nome}}</td>
                        <td>{{$contacto->telefone}}</td>
                        <td>{{$contacto->email}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$contacto->created_at}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$contacto->updated_at}}</td>
                        <td>
                          {!! Form::open(['url' => route('contacto.destroy', $contacto->id),
                            'class' => '',
                            'data-toggle' => 'tooltip',
                            'title' => trans('titles.delete')]) !!}
                            {!! Form::hidden('_method', 'DELETE') !!}
                            @php
                              $label = "<i class=\"fa fa-trash-o fa-fw\" aria-hidden=\"true\"></i>" .
                                     "<span class=\"hidden-xs hidden-sm\">" . trans('titles.delete') .
                                     "</span>";
                            @endphp
                            {!! Form::button($label,
                            array('class' => 'btn btn-danger btn-sm',
                            'type' => 'button', 'style' =>'width: 100%;' ,
                            'data-toggle' => 'modal', 'data-target' => '#confirmDelete',
                            'data-title' => 'Delete Subject',
                            'data-message' => 'Are you sure you want to delete this payment ?')) !!}
                          {!! Form::close() !!}
                        </td>
                        <td>
                          <a class="btn btn-sm btn-success btn-block"
                             href="{{ route('contacto.show', $contacto->id) }}"
                             data-toggle="tooltip"
                             title="@lang('titles.show')">
                              <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                              <span class="hidden-xs hidden-sm">@lang('titles.show')</span>
                          </a>
                        </td>
                        {{-- <td>
                          <a class="btn btn-sm btn-info btn-block"
                             href="{{ route('contacto.edit', $contacto->id) }}"
                             data-toggle="tooltip"
                             title="@lang('titles.edit')">
                              <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                              <span class="hidden-xs hidden-sm">@lang('titles.edit')</span>
                          </a>
                        </td> --}}
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @if (count($contactos) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.tooltips')
@endsection

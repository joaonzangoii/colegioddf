@extends('layouts.admin')

@section('template_title')
  @lang('contactsmanagement.showContact') {{ $contacto->nome }}
@endsection

@php

@endphp

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel  panel-success">
          <div class="panel-heading">
            <a href="{{ route('contactos') }}" class="btn btn-primary btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">{{ trans('contactsmanagement.contactsBackBtn') }}</span>
            </a>
            {{ trans('contactsmanagement.contactsPanelTitle') }}
          </div>
          <div class="panel-body">
            <div class="well">
              <div class="row">
                @if ($contacto->nome)
                  <div class="col-sm-5 col-xs-6 text-larger">
                    <strong>
                      {{ trans('contactsmanagement.labelName') }}
                    </strong>
                  </div>
                  <div class="col-sm-7">
                    {{ $contacto->nome }}
                  </div>
                  <div class="clearfix"></div>
                  <div class="border-bottom"></div>
                @endif
                @if ($contacto->email)
                  <div class="col-sm-5 col-xs-6 text-larger">
                    <strong>
                      {{ trans('contactsmanagement.labelEmail') }}
                    </strong>
                  </div>
                  <div class="col-sm-7">
                    {{ $contacto->email }}
                  </div>
                  <div class="clearfix"></div>
                  <div class="border-bottom"></div>
                @endif
                @if ($contacto->telefone)
                  <div class="col-sm-5 col-xs-6 text-larger">
                    <strong>
                      {{ trans('contactsmanagement.labelTelephone') }}
                    </strong>
                  </div>
                  <div class="col-sm-7">
                    {{ $contacto->telefone }}
                  </div>
                  <div class="clearfix"></div>
                  <div class="border-bottom"></div>
                @endif
                @if ($contacto->mensagem)
                  <div class="col-sm-5 col-xs-6 text-larger">
                    <strong>
                      {{ trans('contactsmanagement.labelMessage') }}
                    </strong>
                  </div>
                  <div class="col-sm-7">
                    {{ $contacto->mensagem }}
                  </div>
                  <div class="clearfix"></div>
                  <div class="border-bottom"></div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection

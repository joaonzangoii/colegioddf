@extends('layouts.admin')

@section('template_title')
  @lang('titles.uploadPhoto')
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('titles.uploadPhoto')
            <a href="{{ route('photos') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('titles.back') <span class="hidden-xs">@lang('titles.to')</span>
              <span class="hidden-xs"> @lang('titles.gallery')</span>
            </a>
          </div>

          <div class="panel-body">
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                {{$errors->first()}}
              </div>
            @elseif(isset($message))
              <div class="alert alert-success">
                {{$message}}
              </div>
            @endif
            <form class="form-horizontal"
                  role="form"
                  method="POST"
                  enctype="multipart/form-data"
                  action="{{ route('photos') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="responsavel_id" value="{{ Auth::user()->id }}">
              <div class="form-group  has-feedback row {{ $errors->has('titulo') ? ' has-error ' : '' }}">
                <label class="col-md-3 control-label">Title</label>
                <div class="col-md-9">
                  <input type="text"
                         class="form-control"
                         name="titulo" value="{{ old('titulo') }}">
                   @if ($errors->has('titulo'))
                     <span class="help-block">
                       <strong>{{ $errors->first('titulo') }}</strong>
                     </span>
                   @endif
                </div>
              </div>
              <div class="form-group  has-feedback row {{ $errors->has('imagem') ? ' has-error ' : '' }}">
                <label class="col-md-3 control-label">Image</label>
                <div class="col-md-9">
                  <div class="image-preview">
                    <img id="imagem_img"
                         class="image"
                         src=""
                         alt="{{ trans('titles.noImage') }}"
                         width="100%"
                         height="400px"/>
 									</div>
                  <div class="input-group" style="width: 100%;">
                    {!! Form::file('imagem', ['id' => 'imagem',
 																	  'class' => 'form-control required borrowerImageFile',
 																		'data-errormsg' => 'PhotoUploadErrorMsg',
 																		'placeholder' => trans('forms.create_news_ph_image')
 																		]) !!}
                     <label class="input-group-addon" for="foto_de_capa_acerca">
                       <i class="fa fa-fw fa-file " aria-hidden="true"></i>
                     </label>
                   </div>
                  @if ($errors->has('imagem'))
                    <span class="help-block">
                      <strong>{{ $errors->first('imagem') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">Upload</button>
                  {{--<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>--}}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer_scripts')
  @include('scripts.image-preview')
@endsection

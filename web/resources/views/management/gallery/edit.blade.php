@extends('layouts.admin')
@section('template_title')
  @lang('titles.editPhoto') {{ $image->titulo }}
@endsection
@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">@lang('titles.editPhoto')</div>
          <div class="panel-body">
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                {{$errors->first()}}
              </div>
            @endif
            {!! Form::open(array('action' => ['GalleryManagementController@update', $image->id],
              'method' => 'PUT',
              'role' => 'form',
              'files' => true)) !!}
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT" />
              <input type="hidden" name="responsavel_id" value="{{ Auth::user()->id }}">

              <div class="col-md-10 col-md-offset-1">
                <p><img src="{{asset($image->imagem)}}"></p>
              </div>

              <div class="form-group">
                  <label class="col-md-3 control-label">Title</label>
                  <div class="col-md-9">
                      <input type="text"
                             class="form-control"
                             name="titulo"
                             value="{{ $image->titulo }}">
                  </div>
              </div>

              <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes', array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('modals.edit_user__modal_text_confirm_title'), 'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div><!-- /col-lg-12 -->
    </div>
  </div><!-- /row -->
  @include('modals.modal-save')
  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
@endsection

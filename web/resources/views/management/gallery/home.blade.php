@extends('layouts.admin')

@section('template_title')
  @lang('titles.managePhotos')
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('titles.managePhotos') (Choose a Date Range)
            <div class="btn-group pull-right btn-group-xs">
              <button type="button"
                      class="btn btn-default dropdown-toggle"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                <span class="sr-only">
                  @lang('titles.managePhotos')
                </span>
              </button>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{ route('photos.create') }}">
                    <i class="fa fa-fw fa-photo" aria-hidden="true"></i>
                      @lang('titles.adminGalleryImageCreate')
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-panel">
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  {{$errors->first()}}
                </div>
              @endif
              <form class="form-horizontal" role="form" method="POST" action="{{ route('photos.filtered') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <label class="col-md-4 control-label">@lang('titles.year')</label>
                  <div class="col-md-6">
                    <select name ='news_year_from' class = "form-control">
                      @foreach(range(15, 50) as $range)
                        <option value="{{"20".$range}}">{{"20".$range}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">@lang('titles.month')</label>
                  <div class="col-md-6">
                    <select name ='news_month_from' class = "form-control">
                      @foreach(trans('months.months') as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">@lang('titles.year')</label>
                  <div class="col-md-6">
                    <select name ='news_year_to' class = "form-control">
                      @foreach(range(15, 50) as $range)
                        <option value="{{"20".$range}}">{{"20".$range}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label">@lang('titles.month')</label>
                  <div class="col-md-6">
                    <select name ='news_month_to' class = "form-control">
                      @foreach(trans('months.months') as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">@lang('titles.manage')</button>
                    <a class="btn btn-danger" href="{{ route('photos.all') }}">
                      @lang('titles.manageAllPhotos')
                    </a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@extends('layouts.admin')

@section('template_title')
  @lang('titles.allPhotos')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .images-table {
        border: 0;
    }
    .images-table tr td:first-child {
        padding-left: 15px;
    }
    .images-table tr td:last-child {
        padding-right: 15px;
    }
    .images-table.table-responsive,
    .images-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('titles.allPhotos')
            <div class="btn-group pull-right btn-group-xs">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v fa-fw" aria-hidden="false"></i>
                <span class="sr-only">
                  @lang('titles.allPhotos')
                </span>
              </button>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{ route('photos.create') }}">
                    <i class="fa fa-fw fa-photo" aria-hidden="true"></i>
                      @lang('titles.adminGalleryImageCreate')
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="panel-body">
            <div class="table-responsive images-table">
                @if ($images)
                  @foreach($images as $item)
                    <div  class="col-md-3">
                      <div>
                        <h4>
                          {{ str_limit($item->titulo, $limit = 30, $end = "...")}}
                        </h4>
                        <img src="{{asset($item->imagem)}}"
                             class="img-responsive">
                        {{$item->created_at->diffForHumans()}}
                        <br>
                        {{$item->updated_at->diffForHumans()}}
                      </div>
                      <div>
                        {!! Form::open(['url' => route('photo.destroy', $item->id),
                          'class' => '', 'data-toggle' => 'tooltip',
                          'title' => trans('titles.delete')]) !!}
                          {!! Form::hidden('_method', 'DELETE') !!}
                          {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>
                          <span class="hidden-xs hidden-sm"></span>',
                          array('class' => 'btn btn-danger btn-xs','type' => 'button',
                          'data-toggle' => 'modal',
                          'data-target' => '#confirmDelete',
                          'data-title' => 'Delete Image',
                          'data-message' => 'Are you sure you want to delete this image ?')) !!}
                        {{-- {!! Form::close() !!} --}}
                        <a class="btn btn-xs btn-info"
                           href="{{ route('photo.edit', $item->id) }}"
                           data-toggle="tooltip" title="{{trans('titles.edit')}}">
                            <i class="fa fa-pencil fa-fw"
                               aria-hidden="true"></i> <span class="hidden-xs hidden-sm"></span>
                        </a>
                        {!! Form::close() !!}
                      </div>
                    </div>
                  @endforeach
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @if (count($images) > 10)
      @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{-- @include('scripts.tooltips')--}}
@endsection

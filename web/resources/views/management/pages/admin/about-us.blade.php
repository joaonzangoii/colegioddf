@extends('layouts.admin')

@section('template_title')
  Welcome {{ Auth::user()->nome }}
@endsection

@section('head')

@endsection

@section('content')
  @include('tinymce::load')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('titles.adminAboutUs')
          </div>
          <div class="panel-body">
             {!! Form::model($siteinfo,
    					             ['action' => ['PagesManagementController@postAboutUsPage'],
    											 'method' => 'POST',
    										   'files' => true]) !!}
              {!! csrf_field() !!}
              {!! Form::hidden('usuario_id', Auth::user()->id) !!}
              <div class="form-group has-feedback row {{ $errors->has('acerca') ? ' has-error ' : '' }}">
                {!! Form::label('acerca', trans('forms.create_subject_label_nome'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::textarea('acerca', NULL, array('id' => 'nome',
                      'class' => 'form-control tinymce',
                      'placeholder' => trans('forms.create_subject_ph_nome'))) !!}
                    <label class="input-group-addon" for="acerca">
                      <i class="fa fa-fw {{ trans('forms.create_subject_icon_nome') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('acerca'))
                    <span class="help-block">
                      <strong>{{ $errors->first('acerca') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('foto_de_capa_acerca') ? ' has-error ' : '' }}">
 								{!! Form::label('foto_de_capa_acerca',
                  trans('forms.create_system-ph-banner-pic'),
                  array('class' => 'col-md-3 control-label')); !!}
 								<div class="col-md-9">
 									<div class="image-preview">
                    <img id="foto_de_capa_acerca_img"
                         class="image"
                         src="{{ $siteinfo->foto_de_capa_acerca }}"
                         alt="{{ trans('titles.noImage') }}"
                         width="100%"
                         height="500px"/>
 									</div>
 									<div class="input-group" style="width: 100%;">
 										{!! Form::file('foto_de_capa_acerca', ['id' => 'foto_de_capa_acerca',
 																	  'class' => 'form-control required borrowerImageFile',
 																		'data-errormsg' => 'PhotoUploadErrorMsg',
 																		'placeholder' => trans('forms.create_system-ph-footer-logo')
 																		]) !!}
 										<label class="input-group-addon" for="foto_de_capa_acerca">
 											<i class="fa fa-fw fa-file " aria-hidden="true"></i>
 										</label>
 									</div>
 									@if ($errors->has('foto_de_capa_acerca'))
 										<span class="help-block">
 											<strong>{{ $errors->first('foto_de_capa_acerca') }}</strong>
 										</span>
 									@endif
 								</div>
 							</div>
              {!! Form::button('<i class="fa fa-save" aria-hidden="true"></i>&nbsp;' . trans('forms.save_changes'),
                               array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right',
                               'type' => 'submit', )) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')
@include('scripts.image-preview')
@endsection

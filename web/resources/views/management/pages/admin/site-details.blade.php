@extends('layouts.admin')

@section('template_title')
	@lang("titles.adminGeneralSettings")
@endsection

@section('head')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::model($siteinfo,
					             ['action' => ['PagesManagementController@postSiteInfo'],
											 'method' => 'POST',
										   'files' => true]) !!}
				  {!! Form::hidden('usuario_id', Auth::user()->id) !!}
					<div class="panel panel-default">
						<div class="panel-heading">
							@lang("titles.adminSiteDetails")
						</div>
						<div class="panel-body">
							<div class="form-group has-feedback row {{ $errors->has('nome') ? ' has-error ' : '' }}">
                {!! Form::label('nome', trans('forms.ph-system-name'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('nome', NULL, array('id' => 'nome', 'class' => 'form-control',
											'placeholder' => trans('forms.ph-system-name'))) !!}
                    <label class="input-group-addon" for="nome">
											<i class="fa fa-fw fa-envelope " aria-hidden="true"></i>
										</label>
                  </div>
                  @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
							<div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
								{!! Form::label('email', trans('forms.ph-system-email'), array('class' => 'col-md-3 control-label')); !!}
								<div class="col-md-9">
									<div class="input-group">
										{!! Form::text('email', NULL, array('id' => 'email', 'class' => 'form-control',
											'placeholder' => trans('forms.ph-system-email'))) !!}
										<label class="input-group-addon" for="email"><i class="fa fa-fw fa-envelope " aria-hidden="true"></i></label>
									</div>
									@if ($errors->has('email'))
										<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="form-group has-feedback row {{ $errors->has('telefone') ? ' has-error ' : '' }}">
                {!! Form::label('telefone', trans('forms.ph-system-telephone'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('telefone', NULL, array('id' => 'telefone', 'class' => 'form-control',
											'placeholder' => trans('forms.ph-system-telephone'))) !!}
                    <label class="input-group-addon" for="telefone"><i class="fa fa-fw fa-mobile " aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('telefone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telefone') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
							<div class="form-group has-feedback row {{ $errors->has('fax') ? ' has-error ' : '' }}">
                {!! Form::label('fax', trans('forms.ph-system-fax'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('fax', NULL, array('id' => 'fax', 'class' => 'form-control',
											 'placeholder' => trans('forms.ph-system-fax'))) !!}
                    <label class="input-group-addon" for="fax">
											<i class="fa fa-fw fa-fax " aria-hidden="true"></i>
										</label>
                  </div>
                  @if ($errors->has('fax'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fax') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('endereco') ? ' has-error ' : '' }}">
                {!! Form::label('endereco', trans('forms.ph-system-address'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('endereco', NULL, array('id' => 'endereco', 'class' => 'form-control',
											 'placeholder' => trans('forms.ph-system-address'))) !!}
                    <label class="input-group-addon" for="endereco">
											<i class="fa fa-fw fa-home " aria-hidden="true"></i>
										</label>
                  </div>
                  @if ($errors->has('endereco'))
                    <span class="help-block">
                        <strong>{{ $errors->first('endereco') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
							<div class="form-group has-feedback row {{ $errors->has('coordenadas') ? ' has-error ' : '' }}">
								{!! Form::label('coordenadas', trans('forms.ph-system-coordinates'), array('class' => 'col-md-3 control-label')); !!}
								<div class="col-md-9">
									<div class="input-group">
										{!! Form::text('coordenadas', NULL, array('id' => 'coordenadas', 'class' => 'form-control',
											 'placeholder' => trans('forms.ph-system-coordinates'))) !!}
										<label class="input-group-addon" for="coordenadas">
											<i class="fa fa-fw fa-location-arrow " aria-hidden="true"></i>
										</label>
									</div>
									@if ($errors->has('coordenadas'))
										<span class="help-block">
												<strong>{{ $errors->first('coordenadas') }}</strong>
										</span>
									@endif
								</div>
							</div>
              <div class="form-group has-feedback row {{ $errors->has('descricao') ? ' has-error ' : '' }}">
                {!! Form::label('descricao', trans('forms.ph-system-description'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::textarea('descricao', NULL, array('id' => 'descricao', 'class' => 'form-control',
											'placeholder' => trans('forms.ph-system-description'))) !!}
                    <label class="input-group-addon" for="descricao">
											<i class="fa fa-fw fa-info " aria-hidden="true"></i>
										</label>
                  </div>
                  @if ($errors->has('descricao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
							<div class="form-group has-feedback row {{ $errors->has('logo_cabecalho') ? ' has-error ' : '' }}">
								{!! Form::label('logo_cabecalho', trans('forms.create_system-ph-header-logo'), array('class' => 'col-md-3 control-label')); !!}
								<div class="col-md-9">
									<div class="image-preview">
										<img id="logo_cabecalho_img"
										   class="image"
										   src="{{ $siteinfo->logo_cabecalho }}"
										   alt="{{ trans('titles.noImage') }}"
										   width="80px"
											 height="50px"/>
									</div>
									<div class="input-group" style="width: 100%">
										{!! Form::file('logo_cabecalho', ['id' => 'logo_cabecalho', 'class' => 'form-control',
											'placeholder' => trans('forms.create_system-ph-header-logo')]) !!}
										<label class="input-group-addon" for="logo_cabecalho">
											<i class="fa fa-fw fa-file " aria-hidden="true"></i>
										</label>
									</div>
									@if ($errors->has('logo_cabecalho'))
									<span class="help-block">
										<strong>{{ $errors->first('logo_cabecalho') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group has-feedback row {{ $errors->has('logo_rodape') ? ' has-error ' : '' }}">
								{!! Form::label('logo_rodape', trans('forms.create_system-ph-footer-logo'), array('class' => 'col-md-3 control-label')); !!}
								<div class="col-md-9">
									<div class="image-preview">
										<img id="logo_rodape_img"
										     class="image"
										     src="{{ $siteinfo->logo_rodape }}"
										     alt="Uploaded Image Preview Holder"
										     width="80px"
												 height="50px"/>
									</div>
									<div class="input-group" style="width: 100%">
										{!! Form::file('logo_rodape', ['id' => 'logo_rodape',
																	  'class' => 'form-control required borrowerImageFile',
																		'data-errormsg' => 'PhotoUploadErrorMsg',
																		'placeholder' => trans('forms.create_system-ph-footer-logo')
																		]) !!}
										<label class="input-group-addon" for="logo_rodape">
											<i class="fa fa-fw fa-file " aria-hidden="true"></i>
										</label>
									</div>
									@if ($errors->has('logo_rodape'))
										<span class="help-block">
											<strong>{{ $errors->first('logo_rodape') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
						<div class="panel-footer">
              <div class="row">
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('forms.save_changes'),
										array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save',
										      'type' => 'button',
													'data-toggle' => 'modal',
													'data-target' => '#confirmSave',
													'data-title' => trans('modals.edit_user__modal_text_confirm_title'),
													'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	@include('modals.modal-save')
@endsection

@section('footer_scripts')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
  @include('scripts.image-preview')
	<script type="text/javascript"></script>
@endsection

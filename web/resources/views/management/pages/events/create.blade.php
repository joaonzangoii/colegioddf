@extends('layouts.admin')

@section('template_title')
  Welcome {{ Auth::user()->nome }}
@endsection

@section('head')

@endsection

@section('content')
  @include('tinymce::load')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>@lang('eventsmanagement.createEvent')</strong>
            <a href="{{ route('admin.eventos') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('eventsmanagement.backToEvents')
            </a>
          </div>

          <div class="panel-body">
             {!! Form::open(['action' => ['EventsManagementController@store'],
    											 'method' => 'POST',
    										   'files' => true]) !!}
              {!! csrf_field() !!}
              {!! Form::hidden('usuario_id', Auth::user()->id) !!}
              <div class="form-group has-feedback row {{ $errors->has('nome') ? ' has-error ' : '' }}">
                {!! Form::label('nome', trans('forms.create_event_label_name'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('nome', NULL, array('id' => 'nome',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_event_ph_name'))) !!}
                    <label class="input-group-addon" for="nome">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_name') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('hora_de_comeco') ? ' has-error ' : '' }}">
                {!! Form::label('hora_de_comeco', trans('forms.create_event_label_starting_time'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('hora_de_comeco', NULL, array('id' => 'hora_de_comeco',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_event_ph_starting_time'))) !!}
                    <label class="input-group-addon" for="hora_de_comeco">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_starting_time') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('hora_de_comeco'))
                    <span class="help-block">
                        <strong>{{ $errors->first('hora_de_comeco') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('hora_de_termino') ? ' has-error ' : '' }}">
                {!! Form::label('hora_de_termino', trans('forms.create_event_label_ending_time'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('hora_de_termino', NULL, array('id' => 'hora_de_termino',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_event_ph_ending_time'))) !!}
                    <label class="input-group-addon" for="hora_de_termino">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_ending_time') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('hora_de_termino'))
                    <span class="help-block">
                        <strong>{{ $errors->first('hora_de_termino') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('lugar') ? ' has-error ' : '' }}">
                {!! Form::label('lugar', trans('forms.create_event_label_place'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('lugar', NULL, array('id' => 'lugar',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_event_ph_place'))) !!}
                    <label class="input-group-addon" for="lugar">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_place') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('lugar'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lugar') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('data') ? ' has-error ' : '' }}">
                {!! Form::label('data', trans('forms.create_event_label_date'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('data', NULL, array('id' => 'data',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_event_ph_date'))) !!}
                    <label class="input-group-addon" for="name">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_date') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('data'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('descricao') ? ' has-error ' : '' }}">
                {!! Form::label('descricao', trans('forms.create_event_label_description'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::textarea('descricao', NULL, array('id' => 'descricao',
                      'class' => 'form-control tinymce',
                      'placeholder' => trans('forms.create_event_ph_description'))) !!}
                    <label class="input-group-addon" for="descricao">
                      <i class="fa fa-fw {{ trans('forms.create_event_icon_description') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('descricao'))
                    <span class="help-block">
                      <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              {!! Form::button('<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_event_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}
              {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')
@include('scripts.image-preview')
@endsection

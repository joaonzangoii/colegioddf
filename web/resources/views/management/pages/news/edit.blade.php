@extends('layouts.admin')

@section('template_title')
  @lang('newsmanagement.editNews') {{ $news->titulo }}
@endsection

@section('head')

@endsection

@section('content')
  @include('tinymce::load')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>@lang('newsmanagement.editNews'): </strong>  {{ $news->titulo }}
            <a href="{{ route('admin.noticias') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('newsmanagement.backToNews')
            </a>
          </div>

          <div class="panel-body">
             {!! Form::model($news,
    					             ['action' => ['NewsManagementController@update', $news->id],
    											 'method' => 'PUT',
    										   'files' => true]) !!}
              {!! csrf_field() !!}
              {!! Form::hidden('usuario_id', Auth::user()->id) !!}
              <div class="form-group has-feedback row {{ $errors->has('titulo') ? ' has-error ' : '' }}">
                {!! Form::label('titulo', trans('forms.create_news_label_title'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('titulo', NULL, array('id' => 'titulo',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_news_ph_title'))) !!}
                    <label class="input-group-addon" for="titulo">
                      <i class="fa fa-fw {{ trans('forms.create_news_icon_title') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('titulo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('titulo') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('descricao') ? ' has-error ' : '' }}">
                {!! Form::label('descricao', trans('forms.create_news_label_description'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::textarea('descricao', NULL, array('id' => 'descricao',
                      'class' => 'form-control tinymce',
                      'placeholder' => trans('forms.create_news_ph_description'))) !!}
                    <label class="input-group-addon" for="descricao">
                      <i class="fa fa-fw {{ trans('forms.create_news_icon_description') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('descricao'))
                    <span class="help-block">
                      <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group  has-feedback row {{ $errors->has('imagem') ? ' has-error ' : '' }}">
                {!! Form::label('imagem', trans('forms.create_news_label_image'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="image-preview">
                    <img id="imagem_img"
                         class="image"
                         src="{{ asset($news->imagem) }}"
                         alt="{{ trans('titles.noImage') }}"
                         width="100%"
                         height="400px"/>
 									</div>
                  <div class="input-group" style="width: 100%;">
                    {!! Form::file('imagem', ['id' => 'imagem',
 																	  'class' => 'form-control required borrowerImageFile',
 																		'data-errormsg' => 'PhotoUploadErrorMsg',
 																		'placeholder' => trans('forms.create_news_ph_image')
 																		]) !!}
                     <label class="input-group-addon" for="imagem">
                       <i class="fa fa-fw fa-file " aria-hidden="true"></i>
                     </label>
                   </div>
                   @if ($errors->has('imagem'))
                     <span class="help-block">
                       <strong>{{ $errors->first('imagem') }}</strong>
                     </span>
                   @endif
                </div>
              </div>
              {!! Form::button('<i class="fa fa-save" aria-hidden="true"></i>&nbsp;' . trans('forms.save_changes'),
                               array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right',
                               'type' => 'submit', )) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')
@include('scripts.image-preview')
@endsection

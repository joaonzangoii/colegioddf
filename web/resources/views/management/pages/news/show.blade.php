@extends('layouts.admin')

@section('template_title')
  @lang('newsmanagement.editNews') {{ $news->titulo }}
@endsection

@php

@endphp

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-success">
          <div class="panel-heading">
            <a href="{{ route('admin.noticias', $news->id) }}"
               class="btn btn-primary btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">{{ trans('newsmanagement.backToNews') }}</span>
            </a>
            {{ $news->titulo }}
          </div>
          <div class="panel-body">
            <div class="image-preview">
              <img id="imagem_img"
                   class="image"
                   src="{{ asset($news->imagem) }}"
                   alt="{{ trans('titles.noImage') }}"
                   width="100%"
                   height="400px"/>
            </div>

            {!!  $news->descricao !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')

@endsection

@extends('layouts.admin')

@section('template_title')
  @lang('titles.welcome') {{ Auth::user()->nome }}
@endsection

@section('content')
  <div class="container">
    <div>
      <div class="col-md-12">
        @include('panels.welcome-panel')
      </div>
    </div>
  </div>
@endsection

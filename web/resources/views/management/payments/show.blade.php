@extends('layouts.admin')

@section('template_title')
  @lang('studentsmanagement.editPayment') {{ $payment->estudante->nome }}
@endsection

@php

@endphp

@section('content')

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')

  @include('scripts.delete-modal-script')

@endsection

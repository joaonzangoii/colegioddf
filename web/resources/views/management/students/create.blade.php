@extends('layouts.admin')

@section('template_title')
  @lang('titles.adminCreateStudent')
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        {!! Form::open(array('action' => 'StudentsManagementController@store',
          'method' => 'POST', 'role' => 'form')) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('titles.adminCreateStudent')
            <a href="{{ route('estudantes') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('titles.back')
              <span class="hidden-xs">@lang('titles.to')</span>
              <span class="hidden-xs">@lang('titles.students')</span>
            </a>
          </div>
          <div class="panel-body">
            {!! csrf_field() !!}
            <div class="form-group has-feedback row {{ $errors->has('primeiro_nome') ? ' has-error ' : '' }}">
              {!! Form::label('primeiro_nome', trans('forms.create_user_label_firstname'),
                              array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('primeiro_nome', NULL, array('id' => 'primeiro_nome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                  <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('primeiro_nome'))
                  <span class="help-block">
                      <strong>{{ $errors->first('primeiro_nome') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('sobrenome') ? ' has-error ' : '' }}">
              {!! Form::label('sobrenome', trans('forms.create_user_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('sobrenome', NULL, array('id' => 'sobrenome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                  <label class="input-group-addon" for="sobrenome"><i class="fa fa-fw {{ trans('forms.create_user_icon_lastname') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('sobrenome'))
                  <span class="help-block">
                      <strong>{{ $errors->first('sobrenome') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
              {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('email', NULL, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                  <label class="input-group-addon" for="email"><i class="fa fa-fw {{ trans('forms.create_user_icon_email') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('sexo') ? ' has-error ' : '' }}">
              {!! Form::label('sexo', trans('forms.create_user_label_sexo'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  <select class="form-control" name="sexo" id="sexo">
                    <option value="{{ old('sexo') }}">{{ trans('forms.create_user_ph_sexo') }}</option>
                    @if (count($sexos))
                      @foreach($sexos as $sKey =>$sexo)
                        <option value="{{ $sKey}}" {{ old('sexo') == $sKey ? 'selected="selected"' : '' }}>{{ $sexo }}</option>
                      @endforeach
                    @endif
                  </select>
                  <label class="input-group-addon" for="sexo">
                    <i class="fa fa-fw {{ trans('forms.create_user_icon_sexo') }}" aria-hidden="true"></i>
                  </label>
                </div>
                @if ($errors->has('sexo'))
                  <span class="help-block">
                     <strong>{{ $errors->first('sexo') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('data_de_nascimento') ? ' has-error ' : '' }}">
              {!! Form::label('data_de_nascimento', trans('forms.create_user_label_dob'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('data_de_nascimento', NULL, array('id' => 'data_de_nascimento', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_dob'))) !!}
                  <label class="input-group-addon" for="data_de_nascimento"><i class="fa fa-fw {{ trans('forms.create_user_icon_dob') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('data_de_nascimento'))
                  <span class="help-block">
                      <strong>{{ $errors->first('data_de_nascimento') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group has-feedback row {{ $errors->has('telefone') ? ' has-error ' : '' }}">
              {!! Form::label('telefone', trans('forms.create_user_label_mobile'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('telefone', NULL, array('id' => 'telefone', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_mobile'))) !!}
                  <label class="input-group-addon" for="telefone">
                    <i class="fa fa-fw {{ trans('forms.create_user_icon_mobile') }}" aria-hidden="true"></i>
                  </label>
                </div>
                @if ($errors->has('telefone'))
                  <span class="help-block">
                      <strong>{{ $errors->first('telefone') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group has-feedback row {{ $errors->has('encarregado_id') ? ' has-error ' : '' }}">
              {!! Form::label('encarregado_id', trans('forms.create_user_label_guardian'),
              ['class' => 'col-md-3 control-label']) !!}
              <div class="col-md-9">
                <div class="input-group">
                  <select class="form-control" name="encarregado_id" id="encarregado_id">
                    <option value="">@lang('forms.create_user_ph_guardian')</option>
                    @if ($encarregados->count())
                      @foreach($encarregados as $encarregado)
                        <option value="{{ $encarregado->id }}" {{ old('encarregado_id') == $encarregado->id ? 'selected="selected"' : '' }}>
                          {{ $encarregado->nome }}
                        </option>
                      @endforeach
                    @endif
                  </select>
                  <label class="input-group-addon" for="encarregado_id">
                    <i class="fa fa-fw {{ trans('forms.create_user_icon_guardian') }}" aria-hidden="true"></i>
                  </label>
                </div>
                @if ($errors->has('encarregado_id'))
                  <span class="help-block">
                    <strong>{{ $errors->first('encarregado_id') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
              {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'))) !!}
                  <label class="input-group-addon" for="password"><i class="fa fa-fw {{ trans('forms.create_user_icon_password') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
              {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                  <label class="input-group-addon" for="password_confirmation"><i class="fa fa-fw {{ trans('forms.create_user_icon_pw_confirmation') }}" aria-hidden="true"></i></label>
                </div>
                @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                @endif
              </div>
            </div>
          </div>
        </div>
        @include('partials.admin.matriculation')
        {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_student_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>

@endsection

@section('footer_scripts')
  @include('scripts.date-picker')
@endsection

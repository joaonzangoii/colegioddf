@extends('layouts.admin')

@section('template_title')
  @lang('subjectsmanagement.showAllSubjects')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .subjects-table {
        border: 0;
    }
    .subjects-table tr td:first-child {
        padding-left: 15px;
    }
    .subjects-table tr td:last-child {
        padding-right: 15px;
    }
    .subjects-table.table-responsive,
    .subjects-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('subjectsmanagement.showAllSubjects')
              <div class="btn-group pull-right btn-group-xs">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                  <span class="sr-only">
                    @lang('subjectsmanagement.showAllSubjects')
                  </span>
                </button>
                <ul class="dropdown-menu">
                  <li>
                    <a href="{{ route('estudante.notas.show', Auth::user()->nome) }}">
                      <i class="fa fa-fw fa-check" aria-hidden="true"></i>
                      @lang('titles.studentsNotasList')
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="panel-body">
            @if(count($subjects) === 0)
              <tr>
                <p class="text-center margin-half">
                  @lang('titles.noItemsFound')
                </p>
              </tr>
            @else
              <div class="table-responsive subjects-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th class="hidden-xs">Subject</th>
                      <th class="hidden-sm hidden-xs hidden-md">Nota Global</th>
                      <th class="hidden-sm hidden-xs hidden-md">Year</th>
                      <th class="hidden-sm hidden-xs hidden-md">Created</th>
                      <th class="hidden-sm hidden-xs hidden-md">Updated</th>
                      <th>Actions</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($subjects as $subject)
                      <tr>
                        <td>{{$subject->id}}</td>
                        @php
                          $disciplina = \App\Models\Subject::find($subject->disciplina_id);
                          $matricula = \App\Models\Matricula::find($subject->matricula_id);
                        @endphp
                        <td class="hidden-xs">{{$disciplina->nome}}</td>
                        <td class="hidden-xs">{{$subject->nota_global}}</td>
                        <td class="hidden-xs">{{$matricula->ano_lectivo}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$subject->created_at}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$subject->updated_at}}</td>
                        <td>
                          <a class="btn btn-sm btn-success btn-block"
                             href="{{ route('estudante.disciplina.show', $subject->id) }}"
                             data-toggle="tooltip"
                             title="Show">
                            <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm">Show</span>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($subjects) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{--@include('scripts.tooltips')--}}
@endsection

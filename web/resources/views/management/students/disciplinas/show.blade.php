@extends('layouts.admin')

@php
  if(!is_null($subject)){
    $disciplina = App\Models\Subject::find($subject->disciplina_id);
  }
  else {
    $disciplina = null;
  }
@endphp

@section('template_title')
  @lang('subjectsmanagement.showAllMarksFor') :
  @if(!is_null($disciplina)) {{ $disciplina->nome }} @endif
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            @lang('subjectsmanagement.showAllMarksFor') :
            @if(!is_null($disciplina)) {{ $disciplina->nome }} @endif
          </div>
          <div class="panel-body">
            @if(!is_null($disciplina))
              <div>
              {{-- <ul class="list-group list-group-responsive payments-details-list margin-bottom-3"> --}}
                @foreach ($resultadoGroups as $keyG => $resultadoGroup)
                  @php
                    $assessment = App\Models\SubjectAssessment::find($keyG)
                  @endphp
                  <h5>
                    <strong>{{ $assessment->nome }}</strong>
                  </h5>
                  <div>
                    @foreach ($resultadoGroup as $keyGk => $resultado)
                    <td>
                      {{ $resultado->nota }}
                    </td>
                    {{-- <li class="list-group-item">
                      {{ $resultado->avaliacao->nome }}
                      <br/>
                      {{ $resultado->nota }}
                    </li> --}}
                    @endforeach
                  </div>
                @endforeach
              {{-- </ul> --}}
              </div>
            @endif
          </div>
        </div>
        {{-- @php
        {"id":3,"nota":11,
        "estudante_id":1,
        "disciplina_cursada_id":1,
        "classe_id":13,
        "trimestre_id":1,
        "avaliacao_id":3,
        "professor_id":1,
        "created_at":"2017-10-22 17:58:49",
        "updated_at":"2017-10-22 17:58:49"}

        $user = $student;
        $terms = \App\Models\Term::all();
        $disciplinas_cursadas = $student->disciplinas_cursadas;
        $avaliacoes = \App\Models\SubjectAssessment::all();
        @endphp

        @include("studentsmanagement.notas._marks" , [
          'user' => $user,
          'student' => $student,
          'terms' => $terms,
          'disciplinas_cursadas' => $disciplinas_cursadas,
          'avaliacoes' => $avaliacoes,
        ]) --}}
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')
@endsection

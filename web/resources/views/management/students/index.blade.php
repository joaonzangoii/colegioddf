@extends('layouts.admin')

@section('template_title')
  @lang('titles.showingAllStudents')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .students-table {
        border: 0;
    }
    .students-table tr td:first-child {
        padding-left: 15px;
    }
    .students-table tr td:last-child {
        padding-right: 15px;
    }
    .students-table.table-responsive,
    .students-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('titles.showingAllStudents')
              <div class="btn-group pull-right btn-group-xs">
                <button type="button"
                        class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                  <span class="sr-only">
                      @lang('titles.showingStudentsManagementMenu')
                  </span>
                </button>
                <ul class="dropdown-menu">
                  @role('admin')
                  <li>
                    <a href="{{ route('estudantes.create') }}">
                      <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                        @lang('titles.adminCreateStudent')
                    </a>
                  </li>
                  <li>
                    <a href="{{ route($deleted_route) }}">
                      <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                        @lang('titles.showDeletedStudent')
                    </a>
                  </li>
                  <li>
                    <a href="{{ route($notas_add) }}">
                      <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                        @lang('titles.addStudentsMarks')
                    </a>
                  </li>
                 @else
                   <li>
                     <a href="{{ route($payments_route) }}">
                       <i class="fa fa-fw fa-money" aria-hidden="true"></i>
                         @lang('titles.adminPaymentsList')
                     </a>
                   </li>
                 @endrole
                </ul>
              </div>
            </div>
          </div>

          <div class="panel-body">
            @if(count($students) === 0)
              <tr>
                <p class="text-center margin-half">
                  @lang('titles.noItemsFound')
                </p>
              </tr>
            @else
              <div class="table-responsive students-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>@lang('studentsmanagement.attrStdNumber')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrName')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrEmail')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrGuardianName')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrRole')</th>
                      <th class="hidden-xs">@lang('titles.CreatedAt')</th>
                      <th class="hidden-xs">@lang('titles.UpdatedAt')</th>
                      <th>@lang('titles.actions')</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($students as $student)
                      @if($student->user)
                      <tr>
                        <td>{{$student->id}}</td>
                        <td>{{$student->nr_de_reg}}</td>
                        <td>{{$student->nome}}</td>
                        <td class="hidden-xs">
                          <a href="mailto:{{ $student->user->email }}"
                            title="email {{ $student->user->email }}">
                            {{ $student->user->email }}
                          </a>
                        </td>
                        <td>{{$student->encarregado->nome}}</td>
                        <td>
                          @include('partials.admin.user_label_class',
                            ["user" => $student->user
                          ])
                        </td>
                        <td class="hidden-sm hidden-xs hidden-md">
                          {{$student->created_at->format('d-m-Y')}}
                        </td>
                        <td>{{$student->updated_at->format('d-m-Y')}}</td>
                        <td>
                          @role('admin')
                          {!! Form::open(['url' => route($destroy_route, $student->id),
                                          'class' => '',
                                          'data-toggle' => 'tooltip',
                                          'title' => trans('titles.delete')]) !!}
                            {!! Form::hidden('_method', 'DELETE') !!}
                            @php
                              $label = "<i class='fa fa-trash-o fa-fw' aria-hidden='true'></i>" .
                                       "<span class='hidden-xs hidden-sm'></span>";
                            @endphp
                            {!! Form::button($label,
                            array('class' => 'btn btn-danger btn-sm',
                                  'type' => 'button',
                                  'style' =>'width: 100%;' ,
                                  'data-toggle' => 'modal',
                                  'data-target' => '#confirmDelete',
                                  'data-title' => trans('studentsmanagement.deleteStudent'),
                                  'data-message' => trans('studentsmanagement.deleteStudentMsg'))) !!}
                           {!! Form::close() !!}
                           @endrole
                        </td>
                        <td>
                          <a class="btn btn-sm btn-success btn-block"
                             href="{{ route($show_route, $student->id) }}"
                             data-toggle="tooltip"
                             title="{{trans('titles.show')}}">
                            <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm"></span>
                          </a>
                        </td>
                        <td>
                          <a class="btn btn-sm btn-info btn-block"
                          href="{{ route($edit_route, $student->id) }}"
                             data-toggle="tooltip"
                             title="{{trans('titles.edit')}}">
                            <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm"></span>
                          </a>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrStdNumber')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrName')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrEmail')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrGuardianName')</th>
                      <th class="hidden-xs">@lang('studentsmanagement.attrRole')</th>
                      <th class="hidden-xs">@lang('titles.CreatedAt')</th>
                      <th class="hidden-xs">@lang('titles.UpdatedAt')</th>
                      <th>@lang('titles.actions')</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($students) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{-- @include('scripts.tooltips') --}}
@endsection

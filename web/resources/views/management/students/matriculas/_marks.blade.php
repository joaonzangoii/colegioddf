@if(count($disciplinas_cursadas) > 0)
<div class="table-responsive students-table">
  <table border="1"
         class="table table-striped table-condensed data-table">
    <thead>
      <tr>
        <th style="text-align: center;vertical-align: middle;"
            rowspan="2">Disciplina
        </th>
        @foreach($terms as $term)
          @php
            if(in_array($term->codigo, ["I", "II", "III"])) {
              $colspan = 3;
            }else{
              $colspan = 3;
            }
          @endphp
          <th colspan="{{ $colspan }}">{{  $term->codigo }}</th>
        @endforeach
      </tr>
      <tr>
        @foreach($terms as $term)
          @foreach($avaliacoes as $avaliacao)
            @if(in_array($avaliacao->codigo, ["MAC", "CPP", "CT"])
               && $term->codigo != "CF")
             @php
                $code =  $avaliacao->codigo;
                $show = true;
              @endphp
            @elseif(!in_array($avaliacao->codigo, ["MAC", "CPP", "CT"])
                   && $term->codigo == "CF")
              @php
                $code =  $avaliacao->codigo;
                $show = true;
              @endphp
            @else
              @php
                $code = "";
                $show = false;
              @endphp
            @endif
            @if($show)
              <th>{{ $code }} </th>
            @endif
          @endforeach
        @endforeach
      </tr>
    </thead>
    <tbody>
      @foreach($disciplinas_cursadas as $disciplina_cursada)
        <tr>
          @if(!is_null($disciplina_cursada))
            @php
              $disciplina = App\Models\Subject::find($disciplina_cursada->disciplina_id);
            @endphp
            <td>{{ $disciplina->nome }}</td>
            @foreach($disciplina_cursada->resultados as $key=> $resultado)
              <td>{{ $resultado->nota }}</td>
            @endforeach
          @endif
        </tr>
      @endforeach
      </tbody>
  </table>
  @foreach($avaliacoes as $avaliacao)
    <b>{{ $avaliacao->codigo }} </b> - {{  $avaliacao->nome}}
    <br/>
  @endforeach
</div>
@else
  @if(!$student->matriculas)
    <p>@lang('marksmanagement.createNoMatriculation')</p>
  @else
    <p>No Marks Yet</p>
  @endif
@endif

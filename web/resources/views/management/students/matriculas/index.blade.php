@extends('layouts.admin')

@section('template_title')
  @lang('titles.showingAllMatriculations')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .matriculas-table {
        border: 0;
    }
    .matriculas-table tr td:first-child {
        padding-left: 15px;
    }
    .matriculas-table tr td:last-child {
        padding-right: 15px;
    }
    .matriculas-table.table-responsive,
    .matriculas-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('titles.showingAllMatriculations')
            </div>
          </div>

          <div class="panel-body">
            <div class="table-responsive matriculas-table">
              <table class="table table-striped table-condensed data-table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th class="hidden-xs">Grade</th>
                    <th class="hidden-xs">Course</th>
                    <th class="hidden-xs">Status</th>
                    <th class="hidden-sm hidden-xs hidden-md">Year</th>
                    <th class="hidden-sm hidden-xs hidden-md">Created</th>
                    <th class="hidden-sm hidden-xs hidden-md">Updated</th>
                    <th>Actions</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($matriculas as $matricula)
                    <tr>
                      <td>{{$matricula->id}}</td>
                      @php
                        $classe = \App\Models\StudentClass::find($matricula->classe_id);
                        $curso = \App\Models\Curso::find($matricula->curso_id);
                      @endphp
                      <td class="hidden-xs">{{$classe->nome}}</td>
                      <td class="hidden-xs">{{$curso->nome}}</td>
                      <td class="hidden-xs">{{$matricula->situacao_da_matricula}}</td>
                      <td class="hidden-xs">{{$matricula->ano_lectivo}}</td>
                      <td class="hidden-sm hidden-xs hidden-md">{{$matricula->created_at}}</td>
                      <td class="hidden-sm hidden-xs hidden-md">{{$matricula->updated_at}}</td>
                      <td>
                        <a class="btn btn-sm btn-success btn-block"
                           href="{{ route('estudante.matricula.show', $matricula->id) }}"
                           data-toggle="tooltip"
                           title="Show">
                          <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                          <span class="hidden-xs hidden-sm">Show</span>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($matriculas) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{--@include('scripts.tooltips')--}}
@endsection

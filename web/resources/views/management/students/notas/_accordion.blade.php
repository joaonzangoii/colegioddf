<div class="panel-group" id="accordion">
  @foreach ($matriculas as $key => $matricula)
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h1 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#{{$key}}">
             @php
               $classe = App\Models\StudentClass::find($matricula->classe_id);
               $resultados = App\Models\StudentAssessmentResult::where('matricula_id', $matricula->id)
                                                               ->get();
             @endphp
            {{ $classe->nome }}
          </a>
          <a class="pdsa-panel-toggle"></a>
        </h1>
      </div>
      <div id="{{$key}}" class="panel-collapse collapse {{$key == 0 ? 'in' : ''}}">
        <div class="panel-body">
          @include("management.students.matriculas._marks" , [
            'user' => $user,
            'student' => $student,
            'terms' => $terms,
            'disciplinas_cursadas' => $matricula->disciplinas_cursadas,
            'avaliacoes' => $avaliacoes,
          ])
        </div>
      </div>
    </div>
  @endforeach

<option value="{{ old('disciplina_id') }}">
  {{ trans('forms.create_subject_ph_select_nome') }}
</option>
@if (count($subjects))
  @foreach($subjects as $sKey =>$subject)
    @php
     $selected =  old('disciplina_id') == $subject->id
     ? 'selected="selected"'
     : '' ;
    @endphp
    <option value="{{ $subject->id}}" {{ $selected }}>
      {{ $subject->nome }}
    </option>
  @endforeach
@endif

@foreach($terms as $term)
  @php
    $resultados = $term->resultados;
    $resultados = $resultados->sortBy('category.name');
    $resultados = $resultados->groupBy(function ($item, $key) {
      return $item->disciplina_cursada_id;
    });
  @endphp
  <h1>Trimestre {{ $term->codigo}}</h1>
  <table class="table table-striped table-condensed data-table">
    <thead>
      <tr>
        <th>Disciplina</th>
        @foreach($avaliacoes as $avaliacao)
          @if(in_array($avaliacao->codigo, ["MAC", "CPP", "CT"]))
          <th>{{ $avaliacao->codigo  . " "}} </th>
          @elseif(!in_array($avaliacao->codigo, ["MAC", "CPP", "CT"])
                 && $term->codigo == "III")
             <th>{{ $avaliacao->codigo  . " "}} </th>
          @endif
        @endforeach
      </tr>
    </thead>
    <tr>
      @foreach($resultados as $key=> $resultado)
        <tr>
          <td>
            {{ App\Models\Subject::find($key)->nome }}
          </td>
          @foreach($resultado as $res)
            <td>{{ $res->nota }}</td>
          @endforeach
        </tr>
      @endforeach
    </tr>
  </table>
@endforeach

@extends('layouts.admin')

@section('template_title')
  @lang('titles.addStudentsMarks')
@endsection

@php

@endphp

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <strong>@lang('titles.addStudentsMarks')</strong>
            <a href="{{ route('estudantes') }}"
               class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">@lang('titles.back') @lang('titles.to') </span>
              @lang('titles.students')
            </a>
          </div>
          <div class="panel-body">
            {!! Form::open(array('action' => 'AdminMarksManagementController@store',
            'method' => 'POST', 'role' => 'form')) !!}
              {!! csrf_field() !!}
              {!! Form::hidden("final_assessments", "", ["id" => "final_assessments"]) !!}
              <div class="form-group has-feedback row {{ $errors->has('estudante_id') ? ' has-error ' : '' }}">
                  {!! Form::label('estudante_id', trans('forms.create_user_label_reg_num'),
                  array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control"
                            name="estudante_id"
                            id="estudante_id">
                      <option value="{{ old('estudante_id') }}">
                        {{ trans('forms.create_user_ph_select_reg_num') }}
                      </option>
                      @if (count($students))
                        @foreach($students as $sKey =>$student)
                          @php
                           $selected =  old('estudante_id') == $student->id
                           ? 'selected="selected"'
                           : '' ;
                          @endphp
                          <option value="{{ $student->id}}" {{ $selected }}>
                            {{ $student->nr_de_reg }}
                          </option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="estudante_id">
                      <i class="fa fa-fw {{ trans('forms.create_user_icon_username') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('estudante_id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('estudante_id') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('professor_id') ? ' has-error ' : '' }}">
                {!! Form::label('professor_id', trans('forms.create_mark_label_teacher'),
                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control"
                            name="professor_id"
                            id="professor_id">
                      <option value="{{ old('professor_id') }}">
                        {{ trans('forms.create_mark_ph_select_teacher') }}
                      </option>
                      @if (count($teachers))
                        @foreach($teachers as $sKey =>$teacher)
                          @php
                           $selected =  old('professor_id') == $teacher->id
                           ? 'selected="selected"'
                           : '' ;
                          @endphp
                          <option value="{{ $teacher->id}}" {{ $selected }}>
                            {{ $teacher->nome }}
                          </option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="professor_id">
                      <i class="fa fa-fw {{ trans('forms.create_subject_icon_nome') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('professor_id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('professor_id') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('disciplina_id') ? ' has-error ' : '' }}">
                {!! Form::label('disciplina_id', trans('forms.create_subject_label_disc_nome'),
                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control"
                            name="disciplina_id"
                            id="disciplina_id">
                      <option value="{{ old('disciplina_id') }}">
                        {{ trans('forms.create_subject_ph_select_nome') }}
                      </option>
                    </select>
                    <label class="input-group-addon" for="disciplina_id">
                      <i class="fa fa-fw {{ trans('forms.create_subject_icon_nome') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('disciplina_id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('disciplina_id') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('trimestre_id') ? ' has-error ' : '' }}">
                {!! Form::label('trimestre_id', trans('forms.create_mark_label_term'),
                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control" name="trimestre_id"
                            id="trimestre_id">
                      <option value="{{ old('trimestre_id') }}">
                        {{ trans('forms.create_mark_ph_select_term') }}
                      </option>
                      @if (count($terms))
                        @foreach($terms as $sKey =>$term)
                          @php
                           $selected =  old('trimestre_id') == $term->id
                           ? 'selected="selected"'
                           : '' ;
                          @endphp
                          <option value="{{ $term->id}}" {{ $selected }}>
                            {{ $term->nome }}
                          </option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="trimestre_id">
                      <i class="fa fa-fw {{ trans('forms.create_mark_icon_term') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('trimestre_id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('trimestre_id') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div>
                @foreach($assessments as $sKey => $assessment)
                <div id="{{ $assessment->slug }}-frm"
                     class="form-group has-feedback row {{ $errors->has($assessment->slug) ? ' has-error ' : '' }}">
                  {!! Form::label($assessment->slug, $assessment->codigo,
                  array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::text($assessment->slug, NULL,
                       ['id' => $assessment->slug, 'class' => 'form-control',
                      'placeholder' => $assessment->codigo]) !!}
                      <label class="input-group-addon" for="name">
                        <i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i>
                      </label>
                    </div>
                    @if ($errors->has($assessment->slug))
                      <span class="help-block">
                          <strong>{{ $errors->first($assessment->slug) }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                @endforeach
              </div>

              {!! Form::button('<i class="fa fa-book" aria-hidden="true"></i>&nbsp;' . trans('forms.create_mark_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}
              {!! Form::close() !!}
            </div>
        </div>
        <div id="panel-notas"></div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')
 <script src="{{ asset('/js/laroute.js') }}"></script>
 <script type="text/javascript">
  $(document).ready(function () {
    var mac = $("#mac");
    var cpp = $("#cpp");
    var ct = $("#ct");
    var cap = $("#cap");
    var cepce = $("#cepce");
    var exame = $("#exame");
    var final_assessments = $("#final_assessments");

    if($('select[name="trimestre_id"]').val()) {
      var trimestre_id = $(this).find('option:selected');
      hideOnTerm(trimestre_id.text().trim());
    }
            
    $('select[name="trimestre_id"]').change(function() {
        var trimestre_id = $(this).find('option:selected');
        hideOnTerm(trimestre_id.text().trim());
    });

    $("#estudante_id" ).change(function () {
      var estudante_id = $(this).find('option:selected');
      var trimestre_id = $('#trimestre_id').find('option:selected');

      loadMarksGrid(estudante_id.text().trim(),
                   trimestre_id.val().trim());
      loadSubjects(estudante_id.text().trim());     
    });

    $("#disciplina_id" ).change(function () {
      var disciplina_id = $(this).find('option:selected');
      var estudante_id = $('#estudante_id').find('option:selected');
      var professor_id = $('#professor_id').find('option:selected');
      var trimestre_id = $('#trimestre_id').find('option:selected');
      
      loadMarksGrid(estudante_id.text().trim(),
                   trimestre_id.val().trim());

      loadCurrentMarks(estudante_id.val().trim(),
                      trimestre_id.val().trim(), 
                      professor_id.val().trim(), 
                      disciplina_id.val().trim());
    });

    $("#trimestre_id" ).change(function () {
      var trimestre_id = $(this).find('option:selected');
      var estudante_id = $('#estudante_id').find('option:selected');
      var professor_id = $('#professor_id').find('option:selected');
      var disciplina_id = $('#disciplina_id').find('option:selected');
      loadMarksGrid(estudante_id.text().trim(),
                   trimestre_id.val().trim());
      loadCurrentMarks(estudante_id.val().trim(),
                      trimestre_id.val().trim(), 
                      professor_id.val().trim(), 
                      disciplina_id.val().trim());
      hideOnTerm(trimestre_id.text().trim());
    });



    function hideOnTerm(term)
    {
      if(term == "Primeiro" ||
         term == "Segundo"  ||
         term == "Terceiro") {
        showHideBasedOnTerm(true);
      }else{
        showHideBasedOnTerm(false);
      }
    }

    function showHideBasedOnTerm(status)
    {
      mac.prop("disabled", !status);
      cpp.prop("disabled", !status);;
      ct.prop("disabled",  !status);
      cap.prop("disabled",   status);
      cepce.prop("disabled", status);
      exame.prop("disabled", status);
      final_assessments.val(!status);
    }

    function clearAll()
    {
      mac.val("");
      cpp.val("");;
      ct.val("");
      cap.val("");
      cepce.val("");
      exame.val("");
    }

    function loadMarksGrid(student, 
                          trimestre_id) {
      var request = $.ajax({
        url: laroute.route('student.get.marks',
        {
          'student': student,
          'trimestre_id': trimestre_id,
        }),
        method: "GET",
      }).done(function(data) {
        $('#panel-notas').html(data)
      }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: ", textStatus );
      });
    }

    function loadSubjects(student) {
      
      var request2 = $.ajax({
        url: laroute.route('student.get.subjects', {'student':student}),
        method: "GET",
      }).done(function(data) {
        $('#disciplina_id').html(data)
      }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: ", textStatus );
      });
    }

    function loadCurrentMarks(student, 
                              trimestre_id,
                              professor_id,
                              disciplina_id) {
      var request_get_marks = $.ajax({
        url: laroute.route('student.get.current.marks.by.subject',
        {
          'student': student,
          'trimestre_id': trimestre_id,
          'professor_id': professor_id,
          'disciplina_id': disciplina_id,
        }),
        method: "GET",
      }).done(function(data) {
        clearAll();
        data.forEach(function(resultado){
          var avaliacao = resultado.avaliacao;
          $('#' + avaliacao.slug).val(resultado.nota);
        });
      }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: ", textStatus );
      });
    }
  });
 </script>
@endsection

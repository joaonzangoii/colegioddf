@extends('layouts.admin')

@section('template_title')
  @lang('subjectsmanagement.showAllMarksFor') : {{ $student->full_nome }}
@endsection

@php

@endphp

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel @if ($student->activated == 1) panel-success @else panel-danger @endif">
          <div class="panel-heading">
            <a href="{{ route('estudante.matricula.show', $matricula_id) }}"
               class="btn btn-primary btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">{{ trans('studentsmanagement.studentsMatriculationsBackBtn') }}</span>
            </a>
            {{ trans('studentsmanagement.studentsPanelTitle') }}
          </div>
          <div class="panel-body">
            @php
             $terms = App\Models\Term::all();
             $avaliacoes = App\Models\SubjectAssessment::all();
            @endphp
            <div class="table-responsive students-table">
              <table border="1"
                     class="table table-striped table-condensed data-table">
                <thead>
                  <tr>
                    <th style="text-align: center;vertical-align: middle;"
                        rowspan="2">Disciplina
                    </th>
                    @foreach($terms as $term)
                      @php
                        if(in_array($term->codigo, ["I", "II", "III"])) {
                          $colspan = 3;
                        }else{
                          $colspan = 3; // $avaliacoes->count();
                        }
                      @endphp
                      <th colspan="{{ $colspan }}">{{  $term->codigo }}</th>
                    @endforeach
                  </tr>
                  <tr>
                    @foreach($terms as $term)
                      @foreach($avaliacoes as $avaliacao)
                        @if(in_array($avaliacao->codigo, ["MAC", "CPP", "CT"])
                           && $term->codigo != "CF")
                         @php
                            $code =  $avaliacao->codigo;
                            $show = true;
                          @endphp
                        @elseif(!in_array($avaliacao->codigo, ["MAC", "CPP", "CT"])
                               && $term->codigo == "CF")
                          @php
                            $code =  $avaliacao->codigo;
                            $show = true;
                          @endphp
                        @else
                          @php
                            $code = "";
                            $show = false;
                          @endphp
                        @endif
                        @if($show)
                          <th>{{ $code }} </th>
                        @endif
                      @endforeach
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                  @foreach($disciplinas_cursadas as $disciplina_cursada)
                    @php
                      $disciplina = App\Models\Subject::find($disciplina_cursada->disciplina_id);
                    @endphp
                    <tr>
                      @if(!is_null($disciplina))
                        <td>{{ $disciplina->nome }}</td>
                        @foreach($disciplina_cursada->resultados as $key=> $resultado)
                          <td>{{ $resultado->nota }}</td>
                        @endforeach
                      @endif
                    </tr>
                  @endforeach
                  </tbody>
              </table>
              @foreach($avaliacoes as $avaliacao)
                <b>{{ $avaliacao->codigo }} </b> - {{  $avaliacao->nome}}
                <br/>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer_scripts')

@endsection

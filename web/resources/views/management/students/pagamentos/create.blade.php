@extends('layouts.admin')

@section('template_title')
    @lang('paymentsmanagement.createPayment')
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
              @lang('paymentsmanagement.createPayment')
            <a href="{{ route('estudante.pagamentos') }}"
               class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              {{ trans('studentsmanagement.studentsPaymentsBackBtn') }}
            </a>
          </div>
          <div class="panel-body">
            {!! Form::open(array('action' => 'StudentsPaymentsManagementController@store',
              'method' => 'POST',
              'role' => 'form')) !!}
              {!! csrf_field() !!}
              {{ Form::hidden('estudante_id', $estudante_id) }}
              {{ Form::hidden('matricula_id', $matricula_id) }}
              <div class="form-group has-feedback row {{ $errors->has('trimestre_id') ? ' has-error ' : '' }}">
                {!! Form::label('trimestre_id', trans('forms.create_mark_label_term'),
                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control"
                            name="trimestre_id"
                            id="trimestre_id">
                      <option value="{{ old('trimestre_id') }}">
                        {{ trans('forms.create_mark_ph_select_term') }}
                      </option>
                      @if (count($terms))
                        @foreach($terms as $sKey =>$term)
                          @php
                           $selected =  old('trimestre_id') == $term->id
                           ? 'selected="selected"'
                           : '' ;
                          @endphp
                          <option value="{{ $term->id}}" {{ $selected }}>
                            {{ $term->nome }}
                          </option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="trimestre_id">
                      <i class="fa fa-fw {{ trans('forms.create_mark_icon_term') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('trimestre_id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('trimestre_id') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('mes') ? ' has-error ' : '' }}">
                {!! Form::label('mes', trans('forms.create_payment_label_month'),
                                array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select name ='mes' class = "form-control">
                      <option value="{{ old('mes') }}">
                        {{ trans('forms.create_payment_ph_select_month') }}
                      </option>
                      @foreach(trans('months.months') as $key => $value)
                        @php
                         $selected =  old('mes') == $key
                         ? 'selected="selected"'
                         : '' ;
                        @endphp
                        <option value="{{ $key}}" {{ $selected }}>
                          {{$value}}
                        </option>
                      @endforeach
                    </select>
                    <label class="input-group-addon" for="mes">
                      <i class="fa fa-fw {{ trans('forms.create_payment_icon_month') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('mes'))
                    <span class="help-block">
                       <strong>{{ $errors->first('mes') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('valor') ? ' has-error ' : '' }}">
                {!! Form::label('valor', trans('forms.create_payment_label_amount'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('valor', NULL, array('id' => 'valor',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.create_payment_ph_amount'))) !!}
                    <label class="input-group-addon" for="valor">
                      <i class="fa fa-fw {{ trans('forms.create_payment_icon_amount') }}"
                         aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('valor'))
                    <span class="help-block">
                      <strong>{{ $errors->first('valor') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
            {!! Form::button('<i class="fa fa-money" aria-hidden="true"></i>&nbsp;'
              . trans('forms.create_payment_button_text'),
              array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right',
              'type' => 'submit', )) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('footer_scripts')
@endsection

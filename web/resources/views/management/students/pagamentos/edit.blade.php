@extends('layouts.admin')
@section('template_title')
  @lang('paymentsmanagement.editPayment') {{ $payment->estudante->nome }}
@endsection
@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
    #comprovativo_emb{
      width: 500px;
      height: 600px;
    }
  </style>
@endsection
@section('template_fastload_css')
	.list-group-responsive span:not(.label) {
		display: block;
		overflow-y: auto;
	}
	.list-group-responsive span.label {
		margin-left: 7.25em;
	}

	.payments-details-list strong {
		width: 5.5em;
		display: inline-block;
		position: absolute;
	}

	.payments-details-list span {
	  margin-left: 5.5em;
	}

@endsection
@php
    $status = [
      'name'  => trans('titles.notApproved'),
      'class' => 'danger'
    ];
    if($payment->aprovado == 1) {
      $status = [
        'name'  => trans('titles.approved'),
        'class' => 'success'
      ];
    }
@endphp
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>@lang('paymentsmanagement.editPayment'): </strong>  {{ $payment->estudante->nome }}
            <a href="{{ route('estudante.pagamentos') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">Back to </span>Payments
            </a>
          </div>
            {!! Form::model($payment, ['action' => ['StudentsPaymentsManagementController@update', $payment->id],
              'method' => 'PUT',
              'files' => true]) !!}
              {!! csrf_field() !!}
              <div class="panel-body">
                <div>
                  <ul class="list-group list-group-responsive payments-details-list margin-bottom-3">
                    <li class="list-group-item">
                      <strong>{{ trans('paymentsmanagement.labelStatus') }}</strong>
                        <span class="label label-{{ $status['class'] }}">
                            {{ $status['name'] }}
                        </span>
                    </li>
                    <li class="list-group-item">
                      <strong>{{ trans('paymentsmanagement.labelAmount') }}</strong>
                      <span>{{ $payment->valor }}</span>
                    </li>
                    <li class="list-group-item">
                      <strong>{{ trans('paymentsmanagement.labelTerm') }}</strong>
                      <span>{{ $payment->trimestre->nome }}</span>
                    </li>
                    <li class="list-group-item">
                      <strong>{{ trans('paymentsmanagement.labelMonth') }}</strong>
                      <span>{{ $payment->mes }}</span>
                    </li>
                    <li class="list-group-item">
                      <strong>{{ trans('paymentsmanagement.labelYear') }}</strong>
                      <span>{{ $payment->matricula->ano_lectivo }}</span>
                    </li>
                  </ul>
                </div>
                <div class="form-group has-feedback row {{ $errors->has('comprovativo') ? ' has-error ' : '' }}">
   								{!! Form::label('comprovativo', 'Attachment' , array('class' => 'col-md-3 control-label')); !!}
   								<div class="col-md-9">
                    @if($payment->aprovado == true &&
                        !is_null($payment->comprovativo) &&
                        $payment->comprovativo !== "")
   									<div class="image-preview">
                      <embed id="comprovativo_emb"
                              src="{{ asset($payment->comprovativo)}}"
                              type="application/pdf">
                          <a href="{{ asset($payment->comprovativo) }}">{{ $payment->comprovativo }}</a>
                      </embed>
   									</div>
                    @else
                      @if(!is_null($payment->comprovativo))
                        <a href="{{ asset($payment->comprovativo) }}">{{ $payment->comprovativo }}</a>
                      @else
                      <div><p>@lang('titles.noAttachment')</p></div>
                      @endif
                    @endif
                    @if($payment->aprovado != true)
   									<div class="input-group">
   										{!! Form::file('comprovativo', ['id' => 'comprovativo',
   																	  'class' => 'form-control required borrowerImageFile',
   																		'data-errormsg' => 'PhotoUploadErrorMsg',
   																		'placeholder' => trans('forms.create_system-ph-footer-logo')
   																		]) !!}
   										<label class="input-group-addon" for="comprovativo">
   											<i class="fa fa-fw fa-file " aria-hidden="true"></i>
   										</label>
   									</div>
                    @endif
   									@if ($errors->has('comprovativo'))
   										<span class="help-block">
   											<strong>{{ $errors->first('comprovativo') }}</strong>
   										</span>
   									@endif
   								</div>
   							</div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-save" aria-hidden="true"></i>&nbsp;' . trans('forms.save_changes'),
                     array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save',
                     'type' => 'button', 'data-toggle' => 'modal',
                     'data-target' => '#confirmSave',
                     'data-title' => trans('modals.edit_user__modal_text_confirm_title'),
                     'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-save')
  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
  @include('scripts.file-preview')
@endsection

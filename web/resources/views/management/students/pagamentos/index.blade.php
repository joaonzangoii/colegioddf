@extends('layouts.admin')

@section('template_title')
  @lang('titles.showingAllPayments')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .payments-table {
        border: 0;
    }
    .payments-table tr td:first-child {
        padding-left: 15px;
    }
    .payments-table tr td:last-child {
        padding-right: 15px;
    }
    .payments-table.table-responsive,
    .payments-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('titles.showingAllPayments')
              <div class="btn-group pull-right btn-group-xs">
                ({{ $total }})
              </div>
            </div>
          </div>
          <div class="panel-body">
            @if(count($payments) === 0)
              <tr>
                <p class="text-center margin-half">
                  @lang('titles.noItemsFound')
                </p>
              </tr>
            @else
              <div class="table-responsive payments-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>@lang('titles.name')</th>
                      <th>@lang('titles.approved')</th>
                      <th>@lang('titles.month')</th>
                      <th>@lang('titles.year')</th>
                      <th>@lang('titles.total')</th>
                      <th>@lang('titles.CreatedAt')</th>
                      <th>@lang('titles.UpdatedAt')</th>
                      <th>@lang('titles.actions')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($payments as $payment)
                      <tr>
                        <td>{{$payment->id}}</td>
                        <td>{{$payment->estudante->nome}}</td>
                        <td>
                          @include('partials.admin.label-aprovado', ['aprovado' => $payment->aprovado])
                        </td>
                        <td>{{$payment->mes}}</td>
                        <td>{{$payment->matricula->ano_lectivo}}</td>
                        <td>{{$payment->valor}}</td>
                        <td>{{$payment->created_at}}</td>
                        <td>{{$payment->updated_at}}</td>
                        @php
                         $role = Auth::user()->roles[0]->slug;
                        @endphp
                        <td>
                          <a class="btn btn-sm btn-info btn-block"
                             href="{{ route('estudante.pagamento.edit', $payment->id) }}"
                             data-toggle="tooltip" title="{{trans('titles.edit')}}">
                            <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm">@lang('titles.edit')</span>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($payments) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{--@include('scripts.tooltips')--}}
@endsection

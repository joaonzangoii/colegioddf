@extends('layouts.admin')

@php
  $user = $student->user;
  $levelAmount = trans('studentsmanagement.labelStudentLevel');
  if ($user->level() >= 2) {
    $levelAmount = trans('studentsmanagement.labelStudentLevels');
  }
@endphp

@section('template_title')
  {{ $student->user->nome }}
@endsection
@section('template_linked_css')
  <style>
  .panel > .panel-heading > .panel-title > a {
    text-decoration: none;
  }
  .pdsa-panel-toggle {
    float: right;
    cursor: pointer;
  }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel @if ($student->user->activated == 1) panel-success @else panel-danger @endif">
          <div class="panel-heading">
            {{ trans('studentsmanagement.studentsPanelTitle') }}
            <div class="btn-group pull-right btn-group-xs">
              <button type="button"
                      class="btn btn-primary dropdown-toggle"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                <span class="sr-only">
                    @lang('titles.showingStudentsManagementMenu')
                </span>
              </button>
              <ul class="dropdown-menu">
                @role('admin')
                  <li>
                    <a href="{{ route($notas_add, $student->id) }}">
                      <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                        @lang('titles.addStudentsMarks')
                    </a>
                  </li>
                @else
                  {{-- <li>
                    <a href="{{ route($payments_route) }}">
                      <i class="fa fa-fw fa-money" aria-hidden="true"></i>
                        @lang('titles.adminPaymentsList')
                    </a>
                  </li> --}}
                @endrole
              </ul>
            </div>
            <a href="{{ route($index_route) }}" class="btn btn-primary btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">{{ trans('studentsmanagement.studentsBackBtn') }}</span>
            </a>
          </div>
          <div class="panel-body">
            <div class="well">
              <div class="row">
                <div class="col-sm-6">
                  <img src="@if ($user->profile && $user->profile->avatar_status == 1)
                             {{ $user->profile->avatar }}
                           @else {{ Gravatar::get($user->email) }} @endif"
                       alt="{{ $user->nome }}"
                       id=""
                       class="img-circle center-block margin-bottom-2 margin-top-1 user-image">
                </div>

                <div class="col-sm-6">
                  <h4 class="text-muted margin-top-sm-1 text-center text-left-tablet">
                    {{ $user->nome }}
                  </h4>
                  <p class="text-center text-left-tablet">
                    <strong>
                      {{ $user->primeiro_nome }} {{ $user->sobrenome }}
                    </strong>
                    <br />
                    {{ HTML::mailto($user->email, $user->email) }}
                  </p>

                  @if ($user->profile)
                    <div class="text-center text-left-tablet margin-bottom-1">
                      <a href="{{ route('profile.show', $user->nome) }}"
                         class="btn btn-sm btn-info">
                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                        <span class="hidden-xs hidden-sm hidden-md">
                          {{ trans('studentsmanagement.viewProfile') }}
                        </span>
                      </a>

                      <a href="{{ route('estudantes.edit', $student->id) }}"
                         class="btn btn-sm btn-warning">
                        <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        <span class="hidden-xs hidden-sm hidden-md">
                          {{ trans('studentsmanagement.editStudent') }}
                        </span>
                      </a>
                      @role('admin')
                      {!! Form::open(['url' => route('estudante.destroy', $student->id),
                                      'class' => 'form-inline']) !!}
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md">' . trans('studentsmanagement.deleteStudent') . '</span>' , array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Student', 'data-message' => 'Are you sure you want to delete this student?')) !!}
                      @endrole
                      {!! Form::close() !!}

                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            @if ($user->nome)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelStudentName') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->nome }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->email)
            <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelEmail') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ HTML::mailto($user->email, $user->email) }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->primeiro_nome)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelFirstName') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->primeiro_nome }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            @if ($user->sobrenome)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelLastName') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->sobrenome }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            <div class="col-sm-5 col-xs-6 text-larger">
              <strong>
                {{ trans('studentsmanagement.labelRole') }}
              </strong>
            </div>

            <div class="col-sm-7">
              @foreach ($user->roles as $student_role)

                @if ($student_role->name == 'Student')
                  @php $labelClass = 'primary' @endphp

                @elseif ($student_role->name == 'Admin')
                  @php $labelClass = 'warning' @endphp

                @elseif ($student_role->name == 'Unverified')
                  @php $labelClass = 'danger' @endphp

                @else
                  @php $labelClass = 'default' @endphp

                @endif

                <span class="label label-{{$labelClass}}">{{ $student_role->name }}</span>
              @endforeach
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            <div class="col-sm-5 col-xs-6 text-larger">
              <strong>
                {{ trans('studentsmanagement.labelStatus') }}
              </strong>
            </div>

            <div class="col-sm-7">
              @if ($user->activated == 1)
                <span class="label label-success">
                  Activated
                </span>
              @else
                <span class="label label-danger">
                  Not-Activated
                </span>
              @endif
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            <div class="col-sm-5 col-xs-6 text-larger">
              <strong>
                {{ trans('studentsmanagement.labelAccessLevel')}} {{ $levelAmount }}:
              </strong>
            </div>

            <div class="col-sm-7">
              @if($user->level() >= 5)
                <span class="label label-primary margin-half margin-left-0">5</span>
              @endif

              @if($user->level() >= 4)
                <span class="label label-info margin-half margin-left-0">4</span>
              @endif

              @if($user->level() >= 3)
                <span class="label label-success margin-half margin-left-0">3</span>
              @endif

              @if($user->level() >= 2)
                <span class="label label-warning margin-half margin-left-0">2</span>
              @endif

              @if($user->level() >= 1)
                <span class="label label-default margin-half margin-left-0">1</span>
              @endif
            </div>

            <div class="clearfix"></div>
            <div class="border-bottom"></div>
            @if($user->level() == 5)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelPermissions') }}
                </strong>
              </div>

              <div class="col-sm-7">
                @if($user->canViewStudents())
                  <span class="label label-primary margin-half margin-left-0"">
                    {{ trans('permsandroles.permissionView') }}
                  </span>
                @endif

                @if($user->canCreateStudents())
                  <span class="label label-info margin-half margin-left-0"">
                    {{ trans('permsandroles.permissionCreate') }}
                  </span>
                @endif

                @if($user->canEditStudents())
                  <span class="label label-warning margin-half margin-left-0"">
                    {{ trans('permsandroles.permissionEdit') }}
                  </span>
                @endif

                @if($user->canDeleteStudents())
                  <span class="label label-danger margin-half margin-left-0"">
                    {{ trans('permsandroles.permissionDelete') }}
                  </span>
                @endif
              </div>
            @endif

            <div class="clearfix"></div>
            <div class="border-bottom"></div>

            @if ($user->created_at)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelCreatedAt') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->created_at }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->updated_at)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelUpdatedAt') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->updated_at }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->signup_ip_address)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelIpEmail') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->signup_ip_address }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>

            @endif

            @if ($user->signup_confirmation_ip_address)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelIpConfirm') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->signup_confirmation_ip_address }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->signup_sm_ip_address)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelIpSocial') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->signup_sm_ip_address }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->admin_ip_address)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelIpAdmin') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->admin_ip_address }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            @if ($user->updated_ip_address)
              <div class="col-sm-5 col-xs-6 text-larger">
                <strong>
                  {{ trans('studentsmanagement.labelIpUpdate') }}
                </strong>
              </div>

              <div class="col-sm-7">
                {{ $user->updated_ip_address }}
              </div>

              <div class="clearfix"></div>
              <div class="border-bottom"></div>
            @endif

            {{ $user->results }}
          </div>
        </div>
        @include("management.students.notas._accordion" , [
          'student' => $student,
          'matriculas' => $matriculas,
        ])
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')

  <script type="text/javascript">
      //*********************************************************************//
      // PDSA Collapser
      // The following functions are for the PDSA collapser which adds
      // a glyph icon to the right side of the bootstrap panel collapser.
      //*********************************************************************//
      $(document).ready(function () {
        // Hook into glyph anchor tag so we can
        // toggle the collapse and change the glyph
        $(".pdsa-panel-toggle").click(function () {
          // Retrieve the previous anchor tag's 'href' attribute
          // so we can toggle it.
          $($(this).prev().attr("href")).collapse("toggle");
          // Change the glyphs
          toggleGlyphs($(this));
        });

        // Hook into title anchor tag so we can toggle the glyph
        $("a[data-toggle='collapse']").click(function () {
          // Change the glyphs
          toggleGlyphs($(this).next());
        });

        // Add the down glyph to all elements that have the class 'pdsa-panel-toggle'
        $(".pdsa-panel-toggle").addClass("glyphicon glyphicon-chevron-down");

        // Find any panel's that have the class '.in',
        // remove the down glyph and add the up glyph
        initialGlyphsSetup();
      });

      function initialGlyphsSetup() {
        var list = $(".in");
        for (var i = 0; i < list.length; i++) {
          $($("a[href='#" + $(list[i]).attr("id") + "']")).next()
            .removeClass("glyphicon glyphicon-chevron-down")
            .addClass("glyphicon glyphicon-chevron-up");
        }
      }

      // This function toggles the glyphs
      function toggleGlyphs(ctl) {
        var el =  $(ctl);
        if (el.hasClass("glyphicon glyphicon-chevron-up")) {
          el.removeClass("glyphicon glyphicon-chevron-up");
          el.addClass("glyphicon glyphicon-chevron-down");
        }
        else {
          el.removeClass("glyphicon glyphicon-chevron-down");
          el.addClass("glyphicon glyphicon-chevron-up");
        }

        // var $myGroup = $('#accordion');
        // $myGroup.on('show.bs.collapse','.collapse', function() {
        //     $myGroup.find('.collapse.in').collapse('hide');
        // });
      }
      //*********************************************************************//
      // END: PDSA Collapser
      //*********************************************************************//
  </script>
@endsection

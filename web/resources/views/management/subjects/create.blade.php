@extends('layouts.admin')

@section('template_title')
  Create Subject
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            Create Subject
            <a href="{{ route('disciplinas') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Subjects</span>
            </a>
          </div>
          <div class="panel-body">
            {!! Form::open(array('action' => 'SubjectsManagementController@store', 'method' => 'POST', 'role' => 'form')) !!}
              {!! csrf_field() !!}
              <div class="form-group has-feedback row {{ $errors->has('nome') ? ' has-error ' : '' }}">
                {!! Form::label('nome', trans('forms.create_subject_label_nome'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('nome', NULL, array('id' => 'nome', 'class' => 'form-control', 'placeholder' => trans('forms.create_subject_ph_nome'))) !!}
                    <label class="input-group-addon" for="nome">
                      <i class="fa fa-fw {{ trans('forms.create_subject_icon_nome') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('nome'))
                    <span class="help-block">
                      <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

            {!! Form::button('<i class="fa fa-book" aria-hidden="true"></i>&nbsp;' . trans('forms.create_subject_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}
            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('footer_scripts')
@endsection

@extends('layouts.admin')
@section('template_title')
  @lang('subjectsmanagement.editSubject') {{ $subject->nome }}
@endsection
@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>@lang('subjectsmanagement.editSubject'):</strong> {{ $subject->name }}
            <a href="{{ route('disciplinas.show', $subject->id) }}" class="btn btn-primary btn-xs pull-right" style="margin-left: 1em;">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('subjectsmanagement.backToSubject')
            </a>
            <a href="{{ route('disciplinas') }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('subjectsmanagement.backToSubjects')
            </a>
          </div>

          {!! Form::model($subject, array('action' => array('SubjectsManagementController@update', $subject->id), 'method' => 'PUT')) !!}
            {!! csrf_field() !!}
            <div class="panel-body">
              <div class="form-group has-feedback row {{ $errors->has('nome') ? ' has-error ' : '' }}">
                {!! Form::label('nome', 'Nome' , array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('nome', old('nome'), array('id' => 'nome',
                                   'class' => 'form-control',
                                   'placeholder' => trans('forms.ph-nome'))) !!}
                    <label class="input-group-addon" for="nome">
                      <i class="fa fa-fw fa-book }}" aria-hidden="true"></i>
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-save" aria-hidden="true"></i>&nbsp;' . trans('forms.save_changes'),
                    array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save',
                    'type' => 'button',
                    'data-toggle' => 'modal',
                    'data-target' => '#confirmSave',
                    'data-title' => trans('modals.edit_user__modal_text_confirm_title'),
                    'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-save')
  @include('modals.modal-delete')
@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
@endsection

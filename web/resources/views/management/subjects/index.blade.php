@extends('layouts.admin')

@section('template_title')
  @lang('subjectsmanagement.showAllSubjects')
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style type="text/css" media="screen">
      .subjects-table {
          border: 0;
      }
      .subjects-table tr td:first-child {
          padding-left: 15px;
      }
      .subjects-table tr td:last-child {
          padding-right: 15px;
      }
      .subjects-table.table-responsive,
      .subjects-table.table-responsive table {
          margin-bottom: 0;
      }
    </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              @lang('subjectsmanagement.showAllSubjects')
              <div class="btn-group pull-right btn-group-xs">
                <button type="button" class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                  <span class="sr-only">
                    @lang('subjectsmanagement.showSubjectsManagementMenu')
                  </span>
                </button>
                <ul class="dropdown-menu">
                  <li>
                    <a href="{{ route('disciplinas.create') }}">
                      <i class="fa fa-fw fa-book" aria-hidden="true"></i>
                        @lang('titles.adminCreateSubject')
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="panel-body">
            @if(count($subjects) === 0)
              <tr>
                <p class="text-center margin-half">
                  @lang('titles.noItemsFound')
                </p>
              </tr>
            @else
              <div class="table-responsive subjects-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>@lang('titles.name')</th>
                      <th>@lang('titles.CreatedAt')</th>
                      <th>@lang('titles.UpdatedAt')</th>
                      <th>@lang('titles.actions')</th>
                      <th></th>
                    </tr>
                  </thead>
                   <tbody>
                    @foreach($subjects as $subject)
                      <tr>
                        <td>{{$subject->id}}</td>
                        <td class="hidden-xs">{{$subject->nome}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$subject->created_at}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$subject->updated_at}}</td>
                        <td>
                          {!! Form::open(['url' => route('disciplina.destroy', $subject->id),
                                          'class' => '',
                                          'data-toggle' => 'tooltip',
                                          'title' => trans('titles.delete')]) !!}
                            {!! Form::hidden('_method', 'DELETE') !!}
                            @php
                              $label = "<i class=\"fa fa-trash-o fa-fw\" aria-hidden=\"true\"></i>" .
                                     "<span class=\"hidden-xs hidden-sm\">" . trans('titles.delete') .
                                     "</span>";
                            @endphp
                            {!! Form::button($label,
                            array('class' => 'btn btn-danger btn-sm',
                                  'type' => 'button',
                                  'style' =>'width: 100%;',
                                  'data-toggle' => 'modal',
                                  'data-target' => '#confirmDelete',
                                  'data-title' => 'Delete Subject',
                                  'data-message' => 'Are you sure you want to delete this subject?')) !!}
                          {!! Form::close() !!}
                        </td>
                        <td>
                          <a class="btn btn-sm btn-info btn-block"
                             href="{{ route('disciplina.edit', $subject->id) }}"
                             data-toggle="tooltip" title="{{trans('titles.edit')}}">
                              <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                              <span class="hidden-xs hidden-sm">@lang('titles.edit')</span>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($subjects) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.tooltips')
@endsection

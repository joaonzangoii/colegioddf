@extends('layouts.admin')

@section('template_title')
  @lang('subjectsmanagement.showSubject') {{ $subject->nome }}
@endsection

@php

@endphp

@section('content')

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')

  @include('scripts.delete-modal-script')

@endsection

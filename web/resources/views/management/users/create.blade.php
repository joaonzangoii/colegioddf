@extends('layouts.admin')

@section('template_title')
 {{ $createTitle }}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            {{ $createTitle }}
            <a href="{{ route($indexRoute) }}"
               class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('usersmanagement.backToUsers')
            </a>

          </div>
          <div class="panel-body">

            {!! Form::open(array('action' => 'UsersManagementController@store',
                                 'method' => 'POST',
                                 'role' => 'form')) !!}
              {!! csrf_field() !!}
              <div class="form-group has-feedback row {{ $errors->has('primeiro_nome') ? ' has-error ' : '' }}">
                {!! Form::label('primeiro_nome', trans('forms.create_user_label_firstname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('primeiro_nome', NULL, array('id' => 'primeiro_nome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                    <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('primeiro_nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('primeiro_nome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('sobrenome') ? ' has-error ' : '' }}">
                {!! Form::label('sobrenome', trans('forms.create_user_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('sobrenome', NULL, array('id' => 'sobrenome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                    <label class="input-group-addon" for="sobrenome"><i class="fa fa-fw {{ trans('forms.create_user_icon_lastname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('sobrenome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sobrenome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('email', NULL, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                    <label class="input-group-addon" for="email"><i class="fa fa-fw {{ trans('forms.create_user_icon_email') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('sexo') ? ' has-error ' : '' }}">
                {!! Form::label('sexo', trans('forms.create_user_label_sexo'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control" name="sexo" id="sexo">
                      <option value="">{{ trans('forms.create_user_ph_sexo') }}</option>
                      @if (count($sexos))
                        @foreach($sexos as $sKey =>$sexo)
                          <option value="{{ $sKey}}">{{ $sexo }}</option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="sexo">
                      <i class="fa fa-fw {{ trans('forms.create_user_icon_sexo') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('sexo'))
                    <span class="help-block">
                       <strong>{{ $errors->first('sexo') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('data_de_nascimento') ? ' has-error ' : '' }}">
                {!! Form::label('data_de_nascimento', trans('forms.create_user_label_dob'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('data_de_nascimento', NULL, array('id' => 'data_de_nascimento', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_dob'))) !!}
                    <label class="input-group-addon" for="data_de_nascimento"><i class="fa fa-fw {{ trans('forms.create_user_icon_dob') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('data_de_nascimento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data_de_nascimento') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('telefone') ? ' has-error ' : '' }}">
                {!! Form::label('telefone', trans('forms.create_user_label_mobile'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('telefone', NULL, array('id' => 'telefone', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_mobile'))) !!}
                    <label class="input-group-addon" for="telefone">
                      <i class="fa fa-fw {{ trans('forms.create_user_icon_mobile') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('telefone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telefone') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">
                {!! Form::label('role', trans('forms.create_user_label_role'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control" name="role" id="role">
                      <option value="">{{ trans('forms.create_user_ph_role') }}</option>
                      @if ($roles->count())
                        @foreach($roles as $role)
                          @if($role->name !== "Estudante")
                            <option value="{{ $role->id }}">
                              {{ $role->name }}
                            </option>
                          @endif
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="role"><i class="fa fa-fw {{ trans('forms.create_user_icon_role') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'))) !!}
                    <label class="input-group-addon" for="password"><i class="fa fa-fw {{ trans('forms.create_user_icon_password') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                    <label class="input-group-addon" for="password_confirmation"><i class="fa fa-fw {{ trans('forms.create_user_icon_pw_confirmation') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_user_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('footer_scripts')
  @include('scripts.date-picker')
@endsection

@extends('layouts.admin')
@section('template_title')
   {{ $createTitle }} {{ $user->nome }}
@endsection
@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong> {{ $createTitle }} : </strong> {{ $user->nome }}
            <a href="{{ route($showRoute, $user->id) }}"
               class="btn btn-primary btn-xs pull-right"
               style="margin-left: 1em;">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              @lang('usersmanagement.backToUser')
            </a>
            <a href="{{ route($indexRoute) }}" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">{{ trans('usersmanagement.usersBackBtn') }}</span>
            </a>
          </div>

          {!! Form::model($user, array('action' => array('UsersManagementController@update', $user->id),
            'method' => 'PUT')) !!}
            {!! csrf_field() !!}
            <div class="panel-body">
              <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                {!! Form::label('email', 'E-mail' , array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('email', old('email'), array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.ph-useremail'))) !!}
                    <label class="input-group-addon" for="email"><i class="fa fa-fw fa-envelope " aria-hidden="true"></i></label>
                  </div>
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('primeiro_nome') ? ' has-error ' : '' }}">
                {!! Form::label('primeiro_nome', trans('forms.create_user_label_firstname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('primeiro_nome', $user->userable->primeiro_nome, array('id' => 'primeiro_nome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                    <label class="input-group-addon" for="primeiro_nome"><i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('primeiro_nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('primeiro_nome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('sobrenome') ? ' has-error ' : '' }}">
                {!! Form::label('sobrenome', trans('forms.create_user_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('sobrenome', $user->userable->sobrenome, array('id' => 'sobrenome', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                    <label class="input-group-addon" for="sobrenome"><i class="fa fa-fw {{ trans('forms.create_user_icon_lastname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('sobrenome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sobrenome') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('data_de_nascimento') ? ' has-error ' : '' }}">
                {!! Form::label('data_de_nascimento', trans('forms.create_user_label_dob'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('data_de_nascimento', $user->userable->data_de_nascimento, array('id' => 'data_de_nascimento', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_dob'))) !!}
                    <label class="input-group-addon" for="data_de_nascimento">
                      <i class="fa fa-fw {{ trans('forms.create_user_icon_dob') }}" aria-hidden="true"></i>
                    </label>
                  </div>
                  @if ($errors->has('data_de_nascimento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data_de_nascimento') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">
                {!! Form::label('role', trans('forms.create_user_label_role'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    <select class="form-control" name="role" id="role">
                      <option value="">{{ trans('forms.create_user_ph_role') }}</option>
                      @if ($roles->count())
                        @foreach($roles as $role)
                          <option value="{{ $role->id }}" {{ $currentRole->id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                        @endforeach
                      @endif
                    </select>
                    <label class="input-group-addon" for="role"><i class="fa fa-fw {{ trans('forms.create_user_icon_role') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="pw-change-container">
                <div class="form-group has-feedback row">
                  {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'))) !!}
                      <label class="input-group-addon" for="password"><i class="fa fa-fw {{ trans('forms.create_user_icon_password') }}" aria-hidden="true"></i></label>
                    </div>
                  </div>
                </div>

                <div class="form-group has-feedback row">
                  {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                      <label class="input-group-addon" for="password_confirmation">
                        <i class="fa fa-fw {{ trans('forms.create_user_icon_pw_confirmation') }}" aria-hidden="true">
                        </i>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-6">
                  <a href="#" class="btn btn-default btn-block margin-bottom-1 btn-change-pw" title="Change Password">
                    <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                    <span></span> @lang('usersmanagement.changePassword')
                  </a>
                </div>
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-save" aria-hidden="true"></i>&nbsp;' . trans('forms.save_changes'),
                                    array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save',
                                          'type' => 'button',
                                          'data-toggle' => 'modal',
                                          'data-target' => '#confirmSave',
                                          'data-title' => trans('modals.edit_user__modal_text_confirm_title'),
                                          'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-save')
  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
  @include('scripts.date-picker')
@endsection

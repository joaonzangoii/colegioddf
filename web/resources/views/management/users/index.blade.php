@extends('layouts.admin')

@section('template_title')
  {{ $title }}
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css" media="screen">
    .users-table {
        border: 0;
    }
    .users-table tr td:first-child {
        padding-left: 15px;
    }
    .users-table tr td:last-child {
        padding-right: 15px;
    }
    .users-table.table-responsive,
    .users-table.table-responsive table {
        margin-bottom: 0;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div style="display: flex; justify-content: space-between; align-items: center;">
              {{ $title }}
              <div class="btn-group pull-right btn-group-xs">
                <button type="button"
                        class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                  <span class="sr-only">
                    {{ $title }}
                  </span>
                </button>
                <ul class="dropdown-menu">
                  <li>
                    <a href="{{ route($createRoute) }}">
                      <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                      {{ $createTitle }}
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('deleted.index') }}">
                      <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                      @lang('usersmanagement.showDeletedUsers')
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="panel-body">
            @if(count($users))
              <div class="table-responsive users-table">
                <table class="table table-striped table-condensed data-table">
                  <thead>
                    <tr>
                        <th>ID</th>
                        <th>@lang('studentsmanagement.attrFirstName')</th>
                        <th>@lang('studentsmanagement.attrLastName')</th>
                        <th>@lang('studentsmanagement.attrEmail')</th>
                        <th>@lang('studentsmanagement.attrRole')</th>
                        @if($type === 'encarregados')
                        <th>@lang('studentsmanagement.attrStudentsNumber')</th>
                        @endif
                        <th>@lang('titles.CreatedAt')</th>
                        <th>@lang('titles.UpdatedAt')</th>
                        <th>@lang('titles.actions')</th>
                        <th></th>
                        <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->userable->primeiro_nome}}</td>
                        <td>{{$user->userable->sobrenome}}</td>
                        <td>
                          <a href="mailto:{{ $user->email }}"
                            title="email {{ $user->email }}">
                            {{ $user->email }}
                          </a>
                        </td>
                        <td>
                            @include('partials.admin.user_label_class')
                        </td>
                        @if($type === 'encarregados')
                        <td>
                          {{ count($user->userable->estudantes) }}
                        </td>
                        @endif
                        <td class="hidden-sm hidden-xs hidden-md">{{$user->created_at}}</td>
                        <td class="hidden-sm hidden-xs hidden-md">{{$user->updated_at}}</td>
                        <td>
                          {!! Form::open(['url' => route($deleteRoute, $user->id),
                                          'class' => '',
                                          'data-toggle' => 'tooltip',
                                          'title' => trans('titles.delete')]) !!}
                            {!! Form::hidden('_method', 'DELETE') !!}
                            @php
                              $label = "<i class=\"fa fa-trash-o fa-fw\" aria-hidden=\"true\"></i>" .
                                     "<span class=\"hidden-xs hidden-sm\">" . trans('titles.delete') .
                                     "</span>";
                            @endphp
                            {!! Form::button($label, array('class' => 'btn btn-danger btn-sm',
                                                          'type' => 'button',
                                                          'style' =>'width: 100%;' ,
                                                          'data-toggle' => 'modal',
                                                          'data-target' => '#confirmDelete',
                                                          'data-title' => trans('usersmanagement.deleteUser'),
                                                          'data-message' => trans('usersmanagement.deleteUserMsg'))) !!}
                          {!! Form::close() !!}
                        </td>
                        <td>
                          <a class="btn btn-sm btn-success btn-block"
                             href="{{ route($showRoute, $user->id) }}"
                             data-toggle="tooltip"
                             title="{{trans('titles.show')}}">
                            <i class="fa fa-eye fa-fw"
                               aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm">@lang('titles.show')</span>
                          </a>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-info btn-block"
                               href="{{ route($editRoute, $user->id) }}"
                               data-toggle="tooltip"
                               title="{{trans('titles.edit')}}">
                                <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                <span class="hidden-xs hidden-sm">@lang('titles.edit')</span>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @else
              <div class="text-center">
                <h1>@lang('titles.noItemsFound')</h1>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @if (count($users) > 10)
    @include('scripts.datatables')
  @endif
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{-- @include('scripts.tooltips')--}}
@endsection

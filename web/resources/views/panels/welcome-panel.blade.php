@php
  $levelAmount = 'level';
  if (Auth::User()->level() >= 2) {
      $levelAmount = 'levels';
  }
@endphp

@section('head')
  <link rel="stylesheet" href="{{asset('website/css/fullcalendar.css')}}">
  <link rel="stylesheet" href="{{asset('website/entypo/css/entypo.css')}}">
  @include('styles.top-info')
  @include('styles.full-calendar')
@endsection

<div class="row">
  @for ($x=0 ;$x  < 4; $x++)
    @php
      if($x == 0 ){
        $cor = "tile-red";
        $title = trans('titles.students');
        $total_title = trans('titles.total_students');
        $icon = "entypo-users";
        $count = App\Models\Student::all()->count();
      }else if($x == 1 ){
        $cor = "tile-green";
        $title = trans('titles.teachers');
        $total_title = trans('titles.total_teachers');
        $icon = "entypo-suitcase";
        $count = App\Models\Teacher::all()->count();
      }else if($x == 2 ){
        $cor = "tile-blue";
        $title = trans('titles.guardians');
        $total_title = trans('titles.total_parents');
        $icon = "entypo-user";
        $count = App\Models\Guardian::all()->count();
      }else{
        $cor = "tile-yellow";
        $title = trans('titles.payments');
        $total_title = trans('titles.total_payments');
        $icon = "entypo-credit-card";
        $user = Auth::user();
        if($user->admin() == false)
        {
          $count = App\Models\Payment::where('estudante_id', $user->userable->id)->count();
        }else{
          $count = App\Models\Payment::all()->count();
        }
      }
    @endphp
    <div class="col-md-3">
      <div class="tile-stats {{$cor}}">
          <div class="icon"><i class="{!!$icon!!}"></i></div>
          <div class="num counter"
              data-start="0"
              data-end="{{ $count }}"
              data-postfix=""
              data-duration="1500"
              data-delay="0"
              data-count="{{ $count }}">
              0
          </div>
          <h3>{{$title}}</h3>
          <p>{{$total_title}}</p>
      </div>
    </div>
  @endfor
</div>
<div class="row">
  <div class="col-md-8">
    <div class="panel panel-primary @role('admin', true) panel-info  @endrole">
      <div class="panel-heading">
        @lang('titles.welcome') {{ Auth::user()->nome }}
        @role('admin', true)
          <span class="pull-right label label-primary" style="margin-top:4px">
            @lang('titles.admin_access')
          </span>
        @else
          <span class="pull-right label label-warning" style="margin-top:4px">
          @lang('titles.user_access')
          </span>
        @endrole
      </div>
      <div class="panel-body panel-body-no-padding">
        <div id='calendar'></div>
      </div>
    </div>
  </div>
</div>
@section('footer_scripts')
  @include('scripts.fullcalendarusage')
  <script type="text/javascript">
    $('.counter').each(function() {
      var $this = $(this),
          countTo = $this.attr('data-count');
      $({ countNum: $this.text()}).animate({
        countNum: countTo
      },
      {
        duration: 1000,
        easing:'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
        }
      });
    });
  </script>
@endsection

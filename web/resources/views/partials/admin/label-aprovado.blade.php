@php
  if($aprovado){
    $labelClass = 'label-success';
    $labelLabel = trans('titles.approved');
  }else{
    $labelClass = 'label-danger';
    $labelLabel = trans('titles.notApproved');
  }
@endphp
<span class="label {{ $labelClass }}">{{ $labelLabel }}</span>

<div class="row login-button">
  <button type="button" onclick="admin_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Admin</button>
  <button type="button" onclick="teacher_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Teacher</button>
  <button type="button" onclick="parent_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Parent</button>
  <br><br>
  <button type="button" onclick="student_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Student</button>
  <button type="button" onclick="accountant_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Accountant</button>
  <button type="button" onclick="librarian_login()" class="btn btn-default" style="background: none; color: #fff; border-color: #2191bf;">Librarian</button>
</div>

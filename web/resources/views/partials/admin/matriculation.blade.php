<div class="panel panel-default">
  <div class="panel-heading">
    Matriculate Student
  </div>
  <div class="panel-body">
    <div class="form-group has-feedback row {{ $errors->has('classe_id') ? ' has-error ' : '' }}">
      {!! Form::label('classe_id', trans('forms.create_user_label_classe'),
      ['class' => 'col-md-3 control-label']) !!}
      <div class="col-md-9">
        <div class="input-group">
          <select class="form-control"
                  name="classe_id"
                  id="classe_id"
                  value= "{{ old('class_id')}}">
            <option value="">@lang('forms.create_user_ph_classe')</option>
            @if ($classes->count())
              @foreach($classes as $class)
                <option value="{{ $class->id }}" {{ old('classe_id') == $class->id ? 'selected="selected"' : '' }}>{{ $class->nome }}</option>
              @endforeach
            @endif
          </select>
          <label class="input-group-addon" for="classe_id">
            <i class="fa fa-fw {{ trans('forms.create_user_icon_classe') }}" aria-hidden="true"></i>
          </label>
        </div>
        @if ($errors->has('classe_id'))
          <span class="help-block">
            <strong>{{ $errors->first('classe_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group has-feedback row {{ $errors->has('curso_id') ? ' has-error ' : '' }}">
      {!! Form::label('curso_id', trans('forms.create_user_label_course'),
      ['class' => 'col-md-3 control-label']) !!}
      <div class="col-md-9">
        <div class="input-group">
          <select class="form-control"
                  name="curso_id"
                  id="curso_id"
                  value="{{ old('curso_id') }}">
            <option value="">@lang('forms.create_user_ph_course')</option>
            @if ($cursos->count())
              @foreach($cursos as $curso)
                <option value="{{ $curso->id }}" {{ old('curso_id') == $curso->id ? 'selected="selected"' : '' }}>{{ $curso->nome }}</option>
              @endforeach
            @endif
          </select>
          <label class="input-group-addon" for="curso_id">
            <i class="fa fa-fw {{ trans('forms.create_user_icon_course') }}" aria-hidden="true"></i>
          </label>
        </div>
        @if ($errors->has('curso_id'))
          <span class="help-block">
            <strong>{{ $errors->first('curso_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group has-feedback row {{ $errors->has('ano_lectivo') ? ' has-error ' : '' }}">
      {!! Form::label('ano', trans('forms.create_user_label_year'), array('class' => 'col-md-3 control-label')); !!}
      <div class="col-md-9">
        <div class="input-group">
          <select name ='ano_lectivo' class = "form-control">
            <option value="">@lang('forms.create_user_ph_year')</option>
            @foreach(range(2000, 2999) as $year)
              <option value="{{$year}}" {{ old('ano_lectivo') == $year ? 'selected="selected"' : '' }}>{{$year}} </option>
            @endforeach
          </select>
          <label class="input-group-addon" for="ano">
            <i class="fa fa-fw {{ trans('forms.create_user_icon_year') }}"
               aria-hidden="true"></i>
          </label>
        </div>
        @if ($errors->has('ano_lectivo'))
          <span class="help-block">
              <strong>{{ $errors->first('ano_lectivo') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group has-feedback row {{ $errors->has('valor_da_matricula') ? ' has-error ' : '' }}">
      {!! Form::label('valor_da_matricula', trans('forms.create_user_label_valor_da_matricula'), array('class' => 'col-md-3 control-label')); !!}
      <div class="col-md-9">
        <div class="input-group">
          {!! Form::text('valor_da_matricula',NULL, ['id' => 'valor_da_matricula', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_valor_da_matricula')]) !!}
          <label class="input-group-addon" for="valor_da_matricula"><i class="fa fa-fw {{ trans('forms.create_user_icon_valor_da_matricula') }}" aria-hidden="true"></i></label>
        </div>
        @if ($errors->has('valor_da_matricula'))
          <span class="help-block">
              <strong>{{ $errors->first('valor_da_matricula') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group has-feedback row {{ $errors->has('propina_mensal') ? ' has-error ' : '' }}">
      {!! Form::label('propina_mensal', trans('forms.create_user_label_propina_mensal'), array('class' => 'col-md-3 control-label')); !!}
      <div class="col-md-9">
        <div class="input-group">
          {!! Form::text('propina_mensal',NULL, ['id' => 'propina_mensal', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_propina_mensal')]) !!}
          <label class="input-group-addon" for="propina_mensal">
            <i class="fa fa-fw {{ trans('forms.create_user_icon_propina_mensal') }}" aria-hidden="true"></i>
          </label>
        </div>
        @if ($errors->has('propina_mensal'))
          <span class="help-block">
            <strong>{{ $errors->first('propina_mensal') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group has-feedback row {{ $errors->has('disciplinas_ids') ? ' has-error ' : '' }}">
      {!! Form::label('disciplinas_ids', trans('forms.create_user_label_disciplinas'),
      ['class' => 'col-md-3 control-label']) !!}
      <div class="col-md-9">
        <div class="row">
          @foreach($disciplinas as $disciplina)
            @php
             $checked = "";
             if(old("disciplinas_ids")){
               $checked = in_array($disciplina->id , old("disciplinas_ids")) ? "checked" : "";
             }
            @endphp
            <div class="col-md-6">
              <input type="checkbox" id="disciplina_id-{{$disciplina->id}}" name="disciplinas_ids[]"
                     value="{{ $disciplina->id }}" {{$checked}}/>
              <label for="disciplina_id-{{$disciplina->id}}">{{ $disciplina->nome }}</label>
            </div>
          @endforeach
        </div>
        @if ($errors->has('disciplinas_ids'))
          <span class="help-block">
            <strong>{{ $errors->first('disciplinas_ids') }}</strong>
          </span>
        @endif
      </div>
    </div>
  </div>
</div>

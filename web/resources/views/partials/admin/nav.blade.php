<nav class="navbar navbar-default navbar-static-top">
  <div class="container" style="margin-left: 5px;">
    <div class="navbar-header">
      {{-- Collapsed Hamburger --}}
      <button type="button"
              class="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#app-navbar-collapse">
          <span class="sr-only">{!! trans('titles.toggleNav') !!}</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      {{-- Branding Image --}}
      <a class="navbar-brand" href="{{ route('dashboard.home') }}">
        {!! trans('titles.app') !!}
      </a>
    </div>
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      {{-- Left Side Of Navbar --}}
      <ul class="nav navbar-nav">

      </ul>
      {{-- Right Side Of Navbar --}}
      <ul class="nav navbar-nav navbar-right">
        {{-- Authentication Links --}}
        @if (Auth::guest())
          <li>
            <a href="{{ route('login') }}">{!! trans('titles.login') !!}</a>
          </li>
          <li>
            <a href="{{ route('register') }}">
              {!! trans('titles.register') !!}
            </a>
          </li>
        @else
          <li>
              <a href="{{route('home')}}" target="_blank">
                  {{-- <span class="badge">4</span> --}}
                  <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                  <span class="menu-description">@lang('titles.schoolWebsite')</span>
              </a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle"
              data-toggle="dropdown"
              role="button" aria-expanded="false">
              @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                <img src="{{ Auth::user()->profile->avatar }}"
                     alt="{{ Auth::user()->nome }}" class="user-avatar-nav">
              @else
                <div class="user-avatar-nav"></div>
              @endif
              {{ Auth::user()->nome }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li  class="{{ Active::check(['profile'], true) }}">
                {!! HTML::link(route('profile.show', Auth::user()->nome), trans('titles.profile')) !!}
              </li>
              <li>
                <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                  {!! trans('titles.logout') !!}
                </a>
                <form id="logout-form"
                      action="{{ route('logout') }}"
                      method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
        @endif
      </ul>
    </div>
  </div>
</nav>

<div class="overlay"></div>
<nav class="radim navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ route('dashboard.home') }}"
      style="display: block;
            float: left;
            padding: 0px;
            margin-left: 10px;
            margin-top: 10px;
            height: 70px;">
      <img src="{{ asset('images/logo_ddf.png') }}" alt="">
    </a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="{{ Active::check('home',true) }}">
        <a href="{{ route('dashboard.home') }}">
          <span class="entypo-gauge" aria-hidden="true"></span>
          <span class="menu-description">@lang('titles.dashboard')</span>
        </a>
      </li>
      @role('admin')
        <li class="dropdown {{ Active::check(['estudantes', 'pagamentos'], true) }}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <span class="fa fa-group" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.students')</span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li {{ Request::is('estudantes/*') ? 'class=active' : null }}>
              {!! HTML::link(route('estudantes'), Lang::get('titles.adminStudentsList')) !!}
            </li>
            <li {{ Request::is('estudantes/*') ? 'class=active' : null }}>
              {!! HTML::link(route('estudantes.create'), Lang::get('titles.adminCreateStudent')) !!}
            </li>
            <li class="{{ Active::check('pagamentos', true) }}">
              {!! HTML::link(route('pagamentos'), Lang::get('titles.adminPaymentsList')) !!}
            </li>
          </ul>
        </li>
        <li class="{{ Active::check('professores', true) }}">
          <a href="{{route('professores')}}">
            <span class="entypo-user" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.adminTeacherList')</span>
          </a>
        </li>
        <li class="{{ Active::check('encarregados', true) }}">
          <a href="{{ route('encarregados') }}">
            <span class="entypo-users" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.adminGuardianList')</span>
          </a>
        </li>
        <li class="dropdown {{ Active::check('disciplinas', true) }}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"
             role="button" aria-expanded="false">
            <span class="fa fa-book" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.subjects')</span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="{{ Active::check('disciplinas', true) }}">
              {!! HTML::link(route('disciplinas'), Lang::get('titles.adminSubjectsList')) !!}
            </li>
            <li class="{{ Active::check('disciplinas', true) }}">
              {!! HTML::link(route('disciplinas.create'), Lang::get('titles.adminCreateSubject')) !!}
            </li>
          </ul>
        </li>
        <li class="{{ Active::check('contactos', true) }}">
          <a href="{{ route('contactos') }}">
            <span class="fa fa-inbox" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.adminContactsList')</span>
          </a>
        </li>
        <li class="dropdown {{ Active::check('admin', true) }}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
          aria-expanded="false">
            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.settings')</span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="{{ Active::check('logs', true) }}">
              {!! HTML::link(route('admin.logs'), Lang::get('titles.adminLogs')) !!}
            </li>
            <li class="{{ Active::check('php', true) }}">
              {!! HTML::link(route('admin.php'), Lang::get('titles.adminPHP')) !!}</li>
            <li class="{{ Active::check('routes',true) }}">
              {!! HTML::link(route('admin.routes'), Lang::get('titles.adminRoutes')) !!}
            </li>
            <li class="{{ Active::check('details', true) }}">
              {!! HTML::link(route('admin.details'), Lang::get('titles.adminGeneralSettings')) !!}
            </li>
            <li class="{{ Active::check('enveditor', true) }}">
              {!! HTML::link(url('/enveditor'), Lang::get('titles.adminTechnicalSettings')) !!}
            </li>
          </ul>
        </li>
        <li class="dropdown {{ Active::check(['photos', 'temas'], true) }}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <span class="fa fa-home" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.frontend')</span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="{{ Active::check('about-us', true) }}">
              {!! HTML::link(route('admin.pages.about'), Lang::get('titles.adminAboutUs')) !!}
            </li>
            <li class="{{ Active::check('eventos', true) }}">
              {!! HTML::link(route('admin.eventos'), Lang::get('titles.adminEvents')) !!}
            </li>
            <li class="{{ Active::check('noticias', true) }}">
              {!! HTML::link(route('admin.noticias'), Lang::get('titles.adminNews')) !!}
            </li>
            <li class="{{ Active::check('photos', true) }}">
              {!! HTML::link(route('photos'), Lang::get('titles.adminGalleryImageList')) !!}
            </li>
            <li class="{{ Active::check('temas', true) }}">
              {!! HTML::link(route('temas'), Lang::get('titles.adminThemesList')) !!}
            </li>
          </ul>
        </li>
      @endrole
      @role('encarregado')
        <li class="dropdown {{ Active::check(['estudantes', 'pagamentos'], true) }}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <span class="fa fa-group" aria-hidden="true"></span>
            <span class="menu-description">@lang('titles.students')</span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li {{ Request::is('/encarregado/estudantes/*') ? 'class=active' : null }}>
              {!! HTML::link(route('encarregado.estudantes'),
                Lang::get('titles.adminStudentsList')) !!}
            </li>
            <li class="{{ Active::check('pagamentos', true) }}">
              {!! HTML::link(route('encarregado.estudantes.pagamentos'),
                Lang::get('titles.adminPaymentsList')) !!}
            </li>
          </ul>
        </li>
      @endrole
      @role('estudante')
        <li class="{{ Active::check('pagamentos', true) }}">
          <a href="{{ route('estudante.pagamentos') }}">
            <span class="fa fa-money" aria-hidden="true"></span>
            <span class="menu-description">{{Lang::get('titles.adminPaymentsList')}}</span>
          </a>
        </li>
        <li class="{{ Active::check(['estudantes/disciplinas'],
                                     true) }}">
          <a href="{{ route('estudante.matriculas') }}">
            <span class="fa fa-book" aria-hidden="true"></span>
            <span class="menu-description">{{Lang::get('titles.showingAllMatriculations')}}</span>
          </a>
        </li>
        <li class="{{ Active::check(['estudantes/notas'],
                                     true) }}">
          <a href="{{ route('estudante.notas') }}">
            <span class="fa fa-check-square-o" aria-hidden="true"></span>
            <span class="menu-description">{{Lang::get('titles.showingAllMarks')}}</span>
          </a>
        </li>
      @endrole
      <li class="{{ Active::check('profile',true) }}">
        <a href="{{route('profile.show', Auth::user()->nome)}}">
          <span class="fa fa-lock" aria-hidden="true"></span>
          <span class="menu-description">@lang('titles.profile')</span>
        </a>
      </li>
    </ul>
  </div>
</nav>

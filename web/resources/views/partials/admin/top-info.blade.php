<div class="row text-center pad-top">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-circle-o-notch fa-5x"></i>
                      <h4>Check Data</h4>
                      </a>
                      </div>


                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-envelope-o fa-5x"></i>
                      <h4>Mail Box</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-lightbulb-o fa-5x"></i>
                      <h4>New Issues</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-users fa-5x"></i>
                      <h4>See Users</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-key fa-5x"></i>
                      <h4>Admin </h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="blank.html">
 <i class="fa fa-comments-o fa-5x"></i>
                      <h4>Support</h4>
                      </a>
                      </div>                   
                  </div>
              </div>



<div class="col-md-12 main-chart">
  <div class="row mtbox">
    <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 box0">
      <div class="box1">
        <span class="li_user"></span>
        <h3>{{ App\Models\Student::all()->count() }}</h3>
      </div>
      <p>There are 10 Registered Students.</p>
    </div>
    <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 box0">
      <div class="box1">
        <span class="li_user"></span>
        <h3>{{ App\Models\Teacher::all()->count() }}</h3>
      </div>
      <p>There are 10 registered Teachers.</p>
    </div>
    <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 box0">
      <div class="box1">
        <span class="li_news"></span>
        <h3>{{ App\Models\News::all()->count() }}</h3>
      </div>
      <p>There are over {{ App\Models\News::all()->count() }} posts.</p>
    </div>

    <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 box0">
      <div class="box1">
        <span class="li_note"></span>
        <h3>Session</h3>
      </div>
      <p>The current Session is 2016/2016.</p>
    </div>
  </div><!-- /row mt -->
</div>

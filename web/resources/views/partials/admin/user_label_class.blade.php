@foreach ($user->roles as $user_role)
  @if ($user_role->name == 'Estudante')
    @php $labelClass = 'primary' @endphp
  @elseif ($user_role->name == 'Professor')
    @php $labelClass = 'success' @endphp
  @elseif ($user_role->name == 'Admin')
    @php $labelClass = 'warning' @endphp
  @elseif ($user_role->name == 'Unverified')
    @php $labelClass = 'danger' @endphp
  @else
    @php $labelClass = 'default' @endphp
  @endif
  <span class="label label-{{$labelClass}}">
    {{ $user_role->name }}
  </span>
@endforeach

@php
  $siteinfo = App\Models\SiteInfo::latest()->first();
@endphp
@if(!is_null($siteinfo))
  <footer>
      <div class="footer-widget-area">
          <div class="container">
              <div class="row">
                  <div class="col-lg-4 col-md-12">
                    <div class="footer-widget info-widget text-right">
                      <ul>
                        <li class="address">
                          {{$siteinfo->endereco}}
                          <i class="fa fa-map-marker"></i>
                        </li>
                        <li class="phone">
                          {{$siteinfo->telefone}}<i class="fa fa-phone"></i>
                        </li>
                        <li class="fax">
                          {{$siteinfo->fax}}<i class="fa fa-fax"></i></li>
                        <li class="email">
                          {{$siteinfo->email}}<i class="fa fa-envelope"></i>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-12">
                      <div class="footer-widget logo-widget text-center">
                        <div class="footer-logo-container">
                            <img src="{{asset($siteinfo->logo_rodape)}}" alt="">
                        </div>
                        <div class="footer-socials">
                          <ul>
                            @if(!is_null($siteinfo->facebook_link))
                              <li>
                                <a href="{{$siteinfo->facebook_link}}" target="_blank">
                                  <i class="fa fa-facebook-official"></i>
                                </a>
                              </li>
                            @endif
                            @if(!is_null($siteinfo->twitter_link))
                              <li>
                                <a href="{{$siteinfo->facebook_link}}"  target="_blank">
                                  <i class="fa fa-twitter"></i>
                                </a>
                              </li>
                            @endif
                            @if(!is_null($siteinfo->twitter_link))
                              <li>
                                <a href="{{$siteinfo->linkedin_link}}"  target="_blank">
                                  <i class="fa fa-linkedin"></i>
                                </a>
                              </li>
                            @endif
                            @if(!is_null($siteinfo->twitter_link))
                              <li>
                                <a href="{{$siteinfo->google_plus_link}}"  target="_blank">
                                  <i class="fa fa-google-plus"></i>
                                </a>
                              </li>
                            @endif
                            @if(!is_null($siteinfo->twitter_link))
                              <li>
                                <a href="{{$siteinfo->youtube_link}}"  target="_blank">
                                  <i class="fa fa-youtube"></i>
                                </a>
                              </li>
                            @endif
                            @if(!is_null($siteinfo->twitter_link))
                              <li>
                                <a href="{{$siteinfo->instagram_link}}"  target="_blank">
                                  <i class="fa fa-instagram"></i>
                                </a>
                              </li>
                            @endif
                          </ul>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-12">
                    <div class="footer-widget link-widget">
                      <ul>
                        <li>
                          <a href="{{route('terms.conditions')}}">
                          @lang('app.terms_and_conditions')
                          </a>
                        </li>
                        <li>
                          <a href="{{route('contactar')}}">
                            @lang('app.contact')
                          </a>
                        </li>
                        <li>
                          <a href="{{route('login')}}" target="_blank">
                            @lang('app.login')
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="copyright-area">
          <div class="container">
              <div class="row">
                  <div class="col">
                      <div class="copyright-text text-center">
                          <p>@lang('app.rightsReserved')</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </footer>
@endif
{{-- <script src="{{mix('js/vendor.js')}}"></script> --}}
<script src="{{asset('website/js/popper.min.js')}}"></script>
<script src="{{asset('website/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('website/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('website/frontend/js/scripts.js')}}"></script>
<script src="{{asset('website/frontend/js/jquery.magnific-popup.min.js')}}"></script>
@include('scripts.language_selector')
@yield('js')

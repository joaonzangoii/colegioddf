@php $siteinfo = App\Models\SiteInfo::latest()->first(); @endphp
<header>
  <div class="top-area">
    <div class="container">
      <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 text-right">
          <ul class="list-inline">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
              @php
                $active = Active::check($localeCode, true);
                $pathArray = explode('/', Request::path());
                if(count($pathArray) > 0) {
                  if(!in_array($pathArray[0], ['en', 'pt', 'es']) && $localeCode == "pt")
                  {
                    $active = "active";
                  }
                }
              @endphp
              <li class="list-inline-item {{ $active }} ">
                <a rel="alternate"
                   hreflang="{{ $localeCode }}"
                   href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                  {{ ucFirst($properties['native']) }}
                </a>
              </li>
            @endforeach

            @if(Auth::guest())
            <li class="list-inline-item">
              <a href="{{route('login')}}" target="_blank">
                @lang('app.login')
              </a>
            </li>
            @else
            <li class="list-inline-item">
              <a href="{{route('dashboard.home')}}" target="_blank">
                @lang('app.dashboard')
              </a>
            </li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>
<header>
  <div class="logo-area">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col">
          <div class="logo-container text-center">
            <a href="{{route('home')}}">
              <img src="{{asset($siteinfo->logo_cabecalho)}}"
                   alt="{{route('home')}}">
              <h2>{{$siteinfo->nome}}</h2>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="menu-area">
    <div class="container">
      <nav class="navbar navbar-expand-lg">
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-md-center"
             id="navbarSupportedContent">
          <ul class="navbar-nav ">
            <li class="nav-item active">
              <a class="nav-link" href="{{route('home')}}">
                @lang('app.home')
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link"
                 href="#"
                 id="dropdownMenuLink"
                 data-toggle="dropdown"
                 aria-haspopup="false"
                 aria-expanded="false">
                @lang('app.about')
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><a href="{{route('about')}}">@lang('app.the_college')</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link"
                 href="#"
                 id="dropdownMenuLink"
                 data-toggle="dropdown"
                 aria-haspopup="false"
                 aria-expanded="false">
                @lang('app.news')
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><a href="{{route('noticias.index')}}">@lang('app.news')</a></li>
                <li><a href="{{route('chatter.home')}}">@lang('app.allForums')</a></li>
              </ul>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{route('eventos')}}">
                @lang('app.events')
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link"
                 href="#"
                 id="dropdownMenuLink"
                 data-toggle="dropdown"
                 aria-haspopup="false"
                 aria-expanded="false">
                @lang('app.informations')
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><a href="{{route('documentos.matricula')}}">@lang('app.registrationDocuments')</a></li>
                <li><a href="{{route('tabela.precos')}}">@lang('app.pricesTable')</a></li>
              </ul>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{route('gallery')}}">
                @lang('app.gallery')
              </a>
            </li>
            <li  class="nav-item ">
              <a class="nav-link" href="{{route('contactar')}}">@lang('app.contact')</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>

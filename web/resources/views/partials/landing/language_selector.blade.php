 <button type="button" class="btn btn-default btn-lg">
  <img id="imgBtnSel" src="" alt="..." class="img-thumbnail icon-medium">  
  <span id="lanBtnSel">PT</span></button>
  <button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    @php
      $locales = LaravelLocalization::getSupportedLocales();
    @endphp
    @foreach($locales as $localeCode => $properties)
      @php
        $id = strtoupper($localeCode);
      @endphp
      <li>
        <a id="btn{{ $id }}" class="language"
           rel="alternate" 
           hreflang="{{ $id }}"  
           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"> 
          <img id="imgBtn{{ $id }}" src="" alt="..." class="img-thumbnail icon-small">  
          <span id="lanBtn{{ $id }}">{{ $properties['native'] }}</span>
        </a>
      </li>
    @endforeach
  </ul>

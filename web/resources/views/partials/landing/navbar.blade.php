<nav class="main-nav" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="nav-item {{ active_check('/') }}">
          <a href="{{ route('home') }}">
          @lang('app.home')
        </a>
        </li>
        <li class="{{ active_check('noticias') }} nav-item dropdown">
          <a class="dropdown-toggle"
             data-toggle="dropdown"
             data-hover="dropdown"
             data-delay="0" data-close-others="false" href="{{ route('home') }}">
             @lang('app.news') <i class="fa fa-angle-down"></i>
           </a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('chatter.home')}}">@lang('app.allForums')</a></li>
          </ul>
        </li>
        <li class="nav-item {{ active_check('eventos') }}">
          <a href="{{ route('eventos') }}">@lang('app.events')</a>
        </li>
        <li class="nav-item {{ active_check('galeria') }}">
          <a href="{{ route('gallery') }}">@lang('app.gallery')</a>
        </li>
        <li class="nav-item {{ active_check('contactar') }}">
          <a href="{{ route('contactar') }}">@lang('app.contact')</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

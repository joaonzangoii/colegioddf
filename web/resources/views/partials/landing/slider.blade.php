 <div class="slider-area">
   <div id="main-slider" class="main-slider owl-carousel owl-loaded owl-drag">
     @foreach($noticias as $key => $noticia)
       @php
         $animations =[
           [
             "data-animation-in"  => "rollIn",
             "data-animation-out" => "animate-out rollOut",
           ],
           [
             "data-animation-in"  => "flipInY",
             "data-animation-out" => "animate-out fadeOutUp",
           ],
           [
             "data-animation-in"  => "slideInDown",
             "data-animation-out" => "animate-out slideOutUp",
           ],
           [
             "data-animation-in"  => "slideInUp",
             "data-animation-out" => "animate-out slideOutDown",
           ],
         ];

         $animations_count = count($animations) - 1;
         $animation_h2 = $animations [rand(0, $animations_count)];
         $animation_p  = $animations[rand(0, $animations_count)];
       @endphp
       <div class="single-slide d-flex" style="background-image:url({{ asset($noticia->imagem)}})">
         <div class="slide-content align-self-end">
           <div class="container">
             <div class="row">
               <div class="col">
                 <div class="slide-text">
                   <h2 data-animation-in ="{{ $animation_h2['data-animation-in'] }}"
                       data-animation-out="{{ $animation_h2['data-animation-out'] }}">
                      <a href="{{ route('noticias.show', $noticia->id) }}">
                        {{$noticia->titulo}}
                      </a>
                    </h2>
                   @php
                     $descricao = str_limit($noticia->descricao, 100);
                     $descricao = strip_tags($descricao);
                   @endphp
                   <p data-animation-in ="{{ $animation_p['data-animation-in'] }}"
                      data-animation-out="{{ $animation_p['data-animation-out'] }}">
                      {!! $descricao !!}
                   </p>
                 </div>
               </div>
             </div>
           </div>
         </div>
     </div>
     @endforeach
     {{-- <div class="owl-dots"></div> --}}
     <div class="owl-dots"></div>
   </div>
 </div>
</div>

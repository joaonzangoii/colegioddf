@extends('layouts.admin')

@section('template_title')
	{{ $user->nome }}'s Profile
@endsection

@section('template_fastload_css')
	#map-canvas{
		min-height: 300px;
		height: 100%;
		width: 100%;
	}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						{{ trans('profile.showProfileTitle', ['username' => $user->nome]) }}
					</div>
					<div class="panel-body">
						  @php
							if ($user->profile->avatar_status == 1)	{
								$avatar =  $user->profile->avatar;
							}
							else{
								$avatar =  Gravatar::get($user->email);
							}
						  @endphp
    					<img src="{{ $avatar }}" alt="{{ $user->name }}" class="user-avatar">

						<dl class="user-info">
							@if($user->userable_type === "Estudante")
								<dt>
									{{ trans('profile.showProfileRegNumber') }}
								</dt>
								<dd>
									{{ $user->userable->nr_de_reg }}
								</dd>
							@endif
							<dt>
								{{ trans('profile.showProfileUsername') }}
							</dt>
							<dd>
								{{ $user->nome }}
							</dd>

							<dt>
								{{ trans('profile.showProfileFirstName') }}
							</dt>
							<dd>
								{{ $user->userable->primeiro_nome }}
							</dd>

							@if ($user->userable->sobrenome)
								<dt>
									{{ trans('profile.showProfileLastName') }}
								</dt>
								<dd>
									{{ $user->userable->sobrenome }}
								</dd>
							@endif

							<dt>
								{{ trans('profile.showProfileEmail') }}
							</dt>
							<dd>
								{{ $user->email }}
							</dd>

							@if ($user->profile)

								@if ($user->profile->theme_id)
									<dt>
										{{ trans('profile.showProfileTheme') }}
									</dt>
									<dd>
										{{ $currentTheme->name }}
									</dd>
								@endif
								@if ($user->profile->location)
									<dt>
										{{ trans('profile.showProfileLocation') }}
									</dt>
									<dd>
										{{ $user->profile->location }} <br />
										Latitude: <span id="latitude"></span> / Longitude: <span id="longitude"></span> <br />
										<div id="map-canvas"></div>
									</dd>
								@endif

								@if ($user->profile->bio)
									<dt>
										{{ trans('profile.showProfileBio') }}
									</dt>
									<dd>
										{{ $user->profile->bio }}
									</dd>
								@endif

								@if ($user->profile->twitter_username)
									<dt>
										{{ trans('profile.showProfileTwitterUsername') }}
									</dt>
									<dd>
										@php
											 $twitter_username = $user->profile->twitter_username;
										@endphp
										{!! HTML::link('https://twitter.com/'.$twitter_username, $twitter_username,
											             array('class' => 'twitter-link', 'target' => '_blank')) !!}
									</dd>
								@endif

								@if ($user->profile->github_username)
									<dt>
										{{ trans('profile.showProfileGitHubUsername') }}
									</dt>
									<dd>
										@php
											$github_username = $user->profile->github_username;
										@endphp
										{!! HTML::link('https://github.com/'.$github_username, $github_username,
											              array('class' => 'github-link', 'target' => '_blank')) !!}
									</dd>
								@endif
							@endif
						</dl>
						@if ($user->profile)
							@if (Auth::user()->id == $user->id)
								{!! HTML::icon_link(URL::to(route('profile.edit', Auth::user()->nome)),
									                 'fa fa-fw fa-cog', trans('titles.editProfile'),
																	 array('class' => 'btn btn-small btn-info btn-block')) !!}
							@endif
						@else
							<p>{{ trans('profile.noProfileYet') }}</p>
							{!! HTML::icon_link(URL::to(route('profile.edit', Auth::user()->nome)),
								                  'fa fa-fw fa-plus ', trans('titles.createProfile'),
																	array('class' => 'btn btn-small btn-info btn-block')) !!}
						@endif

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')

	@include('scripts.google-maps-geocode-and-map')

@endsection

{{-- FYI: Datatables do not support colspan or rowpan --}}
@php
  $locale = LaravelLocalization::setLocale();
  if(is_null($locale)){
    $locale = 'pt';
  }
@endphp
<script type="text/javascript" src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('.data-table').dataTable({
        "order": [[0, "desc" ]],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": 'T<"clear">lfrtip',
        "sPaginationType": "full_numbers",
        'aoColumnDefs': [{
            'bSortable': false,
            'searchable': false,
            'aTargets': ['no-search'],
            'bTargets': ['no-sort']
        }],
        "language": {
          "url": "/json/{{$locale}}.json"
        }
      });
    });
</script>

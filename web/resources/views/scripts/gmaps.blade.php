<script>
  // GOOGLE MAP
  var center = [-5.604732,12.196533];
  $('.gmap').gmap3({
  center: center,
  scrollwheel: false,
  zoom: 15,
        streetViewControl : true,
        styles: [
      {
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#f5f5f5"
        }
      ]
      },
      {
      "elementType": "labels.icon",
      "stylers": [
        {
        "visibility": "off"
        }
      ]
      },
      {
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#485d5e"
        }
      ]
      },
      {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
        "color": "#f5f5f5"
        }
      ]
      },
      {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#8080ff"
        }
      ]
      },
      {
      "featureType": "landscape.man_made",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "landscape.natural",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#eeeeee"
        }
      ]
      },
      {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#757575"
        }
      ]
      },
      {
      "featureType": "poi.business",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi.government",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi.medical",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#e5e5e5"
        }
      ]
      },
      {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#a5d6a7"
        }
      ]
      },
      {
      "featureType": "poi.place_of_worship",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi.school",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "poi.sports_complex",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#dce2e2"
        }
      ]
      },
      {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#ffffff"
        }
      ]
      },
      {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#757575"
        }
      ]
      },
      {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#dadada"
        }
      ]
      },
      {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#bfd1d5"
        }
      ]
      },
      {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#616161"
        }
      ]
      },
      {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#485d5e"
        }
      ]
      },
      {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#e5e5e5"
        }
      ]
      },
      {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#eeeeee"
        }
      ]
      },
      {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
        "color": "#c9c9c9"
        }
      ]
      },
      {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
        "color": "#a5cbe2"
        }
      ]
      },
      {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        "color": "#ffffff"
        }
      ]
      }
    ],
  })
  .marker({
  position: center,
  icon: '/website/frontend/img/icons/marker.png'
  });
</script>

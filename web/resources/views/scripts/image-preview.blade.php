<script type="text/javascript">
$(document).ready(function() {
  function readURL(input, image) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(image).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#logo_cabecalho").change(function(e) {
    readURL(this, "#logo_cabecalho_img");
  });

  $("#logo_rodape").change(function(e) {
    readURL(this, "#logo_rodape_img");
  });

  $("#foto_de_capa_acerca").change(function(e) {
    readURL(this, "#foto_de_capa_acerca_img");
  });

  $("#imagem").change(function(e) {
    readURL(this, "#imagem_img");
  });
});
</script>

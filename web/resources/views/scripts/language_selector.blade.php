<script>
  $(document).ready(function(){
    var PtImgLink = "http://www.roemheld.de/IT/Data/Images/Address/portugal.gif";
    var EnImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Grossbritanien.gif";
    // var deuImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Deutschland.gif";
    // var fraImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Frankreich.gif";

    var imgBtnSel = $('#imgBtnSel');
    var imgBtnPt = $('#imgBtnPT');
    var imgBtnEn = $('#imgBtnEN');

    var imgNavSel = $('#imgNavSel');
    var imgNavPt = $('#imgNavPT');
    var imgNavEn = $('#imgNavEN');

    var spanNavSel = $('#lanNavSel');
    var spanBtnSel = $('#lanBtnSel');

    imgBtnSel.attr("src", PtImgLink);
    imgBtnPt.attr("src", PtImgLink);
    imgBtnEn.attr("src", EnImgLink);


    imgNavSel.attr("src", PtImgLink);
    imgNavPt.attr("src", PtImgLink);
    imgNavEn.attr("src", EnImgLink);
    
    $( ".language" ).on( "click", function( event ) {
      var currentId = $(this).attr('id');
      if(currentId == "navPT") {
        imgNavSel.attr("src",PtImgLink);
        spanNavSel.text("PT");
      } else if (currentId == "navEN") {
        imgNavSel.attr("src", EnImgLink);
        spanNavSel.text("EN");
      } 
      // else if (currentId == "navDeu") {
      //   imgNavSel.attr("src",deuImgLink);
      //   spanNavSel.text("DEU");
      // } else if (currentId == "navFra") {
      //   imgNavSel.attr("src",fraImgLink);
      //   spanNavSel.text("FRA");
      // }

      if(currentId == "btnPT") {
        imgBtnSel.attr("src",PtImgLink);
        spanBtnSel.text("PT");
      } else if (currentId == "btnEN") {
        imgBtnSel.attr("src", EnImgLink);
        spanBtnSel.text("EN");
      } 
      // else if (currentId == "btnDeu") {
      //   imgBtnSel.attr("src",deuImgLink);
      //   spanBtnSel.text("DEU");
      // } else if (currentId == "btnFra") {
      //   imgBtnSel.attr("src",fraImgLink);
      //   spanBtnSel.text("FRA");
      // }
    });
});
</script>

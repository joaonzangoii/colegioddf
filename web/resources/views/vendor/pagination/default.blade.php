@if ($paginator->hasPages())
  <div class="row">
    <div class="col">
      <div class="album-grid-pagination">
        <nav aria-label="pagination">
          <ul class="pagination justify-content-end">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
              <li class="page-item disabled">
                <span class="page-link">&laquo;</span>
              </li>
            @else
              <li class="page-item">
                <span class="page-link">
                  <a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
                </span>
              </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled">
                      <span class="page-link">{{ $element }}</span>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                  @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                      <li class="page-item active">
                        <span class="page-link">
                          {{ $page }}
                          <span class="sr-only">(current)</span>
                        </span>
                      </li>
                    @else
                      <li class="page-item">
                        <span class="page-link">
                          <a href="{{ $url }}">{{ $page }}</a>
                        </span>
                      </li>
                    @endif
                  @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
              <li class="page-item">
                <span class="page-link">
                  <a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a>
                </span>
              </li>
            @else
              <li class="page-item disabled">
                <span class="page-link">&raquo;</span>
              </li>
            @endif
          </ul>
        </nav>
      </div>
    </div>
  </div>
@endif

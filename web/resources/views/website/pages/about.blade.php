@extends('layouts.landing')
@section('css')
  <style>
    .image {
      background-repeat: no-repeat;
      background-position: 50%;; height: 600px;
    }
  </style>
@endsection
@section('content')
<div class="about-us-area page-content-area">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="page-title">
          <h1>@lang('app.about')</h1>
          <hr>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        @if(!is_null($siteinfo))
        <div class="about-us-wrapper">
          <div class="about-image">
            <div class="image" style="background-image: url('{{asset($siteinfo->foto_de_capa_acerca)}}');" ></div>
            <img src="" alt="" class="img-fluid">
          </div>
          <div class="page-content">
            <h1>@lang('app.about_our_school')</h1>
            {!! $siteinfo->acerca !!}
          </div>
        </div>
        @endif
      </div>
  </div>
</div>
@endsection

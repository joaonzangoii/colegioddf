@extends('layouts.landing')
@section('css')
  <style>
    .fade {
      opacity: 100;
    }
  </style>
@endsection
@section('content')
@php
  $siteinfo = App\Models\SiteInfo::latest()->first();
@endphp
<div class="privacy-area page-content-area">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="page-title">
          <h1>@lang('app.contact')</h1>
          <hr>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="map-wrapper">
            <div class="gmap"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-12">
        <div class="contact-form-wrapper">
          <div class="contact-form">
            @include('partials.admin.form-status')
            {!! Form::open(['url' => route('contactar.post'), 'method' => 'POST'])!!}
                <div class="form-group name has-feedback {{ $errors->has('nome') ? ' has-error ' : '' }}">
                  <label for="nome">@lang('app.name')</label>
                  <input id="nome"
                         name="nome"
                         type="text"
                         class="form-control"
                         value="{{ old('nome') }}"
                         placeholder="@lang('app.enterName')">
                   @if ($errors->has('nome'))
                     <span class="help-block">
                       <strong>{{ $errors->first('nome') }}</strong>
                     </span>
                   @endif
                </div>
                <div class="form-group email has-feedback {{ $errors->has('email') ? ' has-error ' : '' }}">
                  <label for="email">
                    @lang('app.email')
                    <span class="required">*</span>
                  </label>
                  <input id="email"
                         name="email"
                         type="email"
                         class="form-control"
                         value="{{ old('email') }}"
                         placeholder="@lang('app.enterEmail')">
                   @if ($errors->has('email'))
                     <span class="help-block">
                       <strong>{{ $errors->first('email') }}</strong>
                     </span>
                   @endif
                </div>
                <div class="form-group phone has-feedback {{ $errors->has('telefone') ? ' has-error ' : '' }}">
                  <label for="telefone">@lang('app.phone')</label>
                  <input id="telefone"
                         name="telefone"
                         type="tel"
                         class="form-control"
                         value="{{ old('telefone') }}"
                         placeholder="@lang('app.enterPhone')">
                  @if ($errors->has('telefone'))
                    <span class="help-block">
                      <strong>{{ $errors->first('telefone') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group message has-feedback {{ $errors->has('mensagem') ? ' has-error ' : '' }}">
                    <label for="mensagem">
                        @lang('app.message')<span class="required">*</span>
                    </label>
                    <textarea id="mensagem"
                              name="mensagem"
                              class="form-control"
                              rows="6"
                              placeholder="@lang('app.enterMessage')...">{{ old('mensagem') }}</textarea>
                    @if ($errors->has('mensagem'))
                      <span class="help-block">
                        <strong>{{ $errors->first('mensagem') }}</strong>
                      </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">@lang('app.sendMessage')</button>
            {!! Form::close()!!}
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="sidebar-widgets">
          <div class="widgets contact-info-widget">
            <div class="widget-title">
              <h4>@lang('app.contact')</h4>
            </div>
            <div class="widgets-content">
              <ul>
                <li>
                  <p><strong>@lang('app.address'): </strong>{{ $siteinfo->endereco }}</p>
                </li>
                <li>
                  <p><strong>@lang('app.email'): </strong>{{ $siteinfo->email }}</p>
                </li>
                <li>
                  <p><strong>@lang('app.telephone'): </strong>{{ $siteinfo->telefone }}</p>
                </li>
                <li>
                  <p><strong>@lang('app.fax'): </strong>{{ $siteinfo->fax }}</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkSQBRz8ELzVHMLW8hBYEWByd_icFPQho"></script>
  <script src="{{asset('website/frontend/js/gmap3.min.js')}}"></script>
  @include('scripts.gmaps')
@endsection

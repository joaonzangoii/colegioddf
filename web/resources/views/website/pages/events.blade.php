@extends('layouts.landing')
@section('content')
  <div class="single-news-area page-content-area">
    <div class="container">
      <div class="row page-title">
        <div class="col-md-6">
          <div>
            <h1>@lang('app.events')</h1>
          </div>
        </div>
        <div class="col-md-6">
          {!! $eventos->render() !!}
        </div>
        <div class="col"><hr></div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-md-12">
          <div class="event-list-wrapper">
            <div class="event-list">
              <ul>
                @foreach($eventos as $key => $evento)
                  <li>
                    <div class="event-date text-center">
                      <h6 class="date">
                        {{$evento->data->format('d')}}
                        <span class="month">{{$evento->data->format('M')}}</span>
                      </h6>
                    </div>
                    <h5 class="event-title">
                      {{$evento->nome}}
                      <br>
                      <i class="fa fa-clock-o"></i>
                      {{$evento->hora_de_comeco}} - {{$evento->hora_de_termino}}
                      <br>
                      <i class="fa fa-map-marker"></i>
                      {{$evento->lugar}}
                    </h5>
                  </li>
                @endforeach
              </ul>
              {!! $eventos->render() !!}
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12">
          <div class="sidebar-widgets">
            <div class="widgets recent-news-widget">
              <div class="widget-title">
                <h4>@lang('app.latestNews')</h4>
              </div>
              <div class="widgets-content">
                <ul>
                  @foreach($noticias as $key => $noticia)
                    <li>
                      <h5 class="news-title">
                        <a href="{{ route("noticias.show", $noticia->id) }}">
                          {{ $noticia->titulo}}
                        </a>
                      </h5>
                      <span class="meta">
                        {{$noticia->created_at->format('d M, Y')}}
                      </span>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@extends('layouts.landing')

@section('css')
  <style>
  </style>
@endsection

@section('content')
  <div class="single-album-area page-content-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            @if($images->count() > 0)
              <div class="single-album-wrapper">
                <div class="album-content lightbox-popup">
                  <div class="card-columns">
                    @foreach ($images as $key=>$image)
                        <div class="card">
                          <a href="{{asset($image->imagem)}}">
                            <img class="img-fluid"
                                 src="{{asset($image->imagem)}}"
                                 title="{{$image->titulo}}">
                          </a>
                        </div>
                    @endforeach
                  </div>
                </div>
              </div>

            <div class="album-grid-pagination">
              {!! $images->render() !!}
            </div>
            @else
              <div><span>@lang("app.noGallery")</span></div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')

@endsection

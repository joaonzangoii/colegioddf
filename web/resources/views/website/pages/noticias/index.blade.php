@extends('layouts.landing')
@section('content')
  <div class="news-area page-content-area">
    <div class="container">
      <div class="row page-title">
        <div class="col-md-6">
          <div>
            <h1>@lang('app.allNews')</h1>
          </div>
        </div>
        <div class="col-md-6">
          <nav aria-label="pagination"></nav>
        </div>
        <div class="col"><hr></div>
      </div>
      <div class="row">
        @foreach($noticias as $key => $noticia)
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card ekattor-grid-news">
              <img class="card-img-top img-fluid" alt="" src="{{ $noticia->imagem}}">
              <div class="card-body">
                <h3 class="card-title">
                  <a href="{{ route("noticias.show", $noticia->id)}}">{{ $noticia->titulo}}</a>
                </h3>
                @php
                  $descricao = str_limit($noticia->descricao, 100);
                  $descricao = strip_tags($descricao);
                @endphp
                <p class="card-text">{!! $descricao !!}</p>
                <hr>
                <p class="card-text pull-left">
                  <small class="text-muted">
                    {{$noticia->created_at->format('d M, Y')}}
                  </small>
                </p>
                <a href="{{ route("noticias.show", $noticia->id)}}" class="pull-right btn">
                  @lang("app.readMore")..
                </a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <div class="row">
        <div class="col">
          <div class="event-list-pagination">
            <nav aria-label="pagination">
              {!! $noticias->render() !!}
            </nav>
          </div>
        </div>
      </div>

        {{-- <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">
          <section class="widget has-divider">
            <h3 class="title">Arcu Aliquet Quam Vel</h3>
            <p>Maecenas nisl urna, condimentum ac justo a, adipiscing hendrerit magna. Fusce pharetra laoreet accumsan. Phasellus elit sapien, consequat vel sapien sit amet, condimentum vulputate odio. Aliquam fringilla justo quis est placerat, eu imperdiet lorem cursus. Curabitur pretium nulla lorem, sed egestas ante vestibulum dignissim.</p>
          </section><!--//widget-->
          <section class="widget has-divider">
            <h3 class="title">@lang("app.upcomingEvents")</h3>
            @foreach($eventos as $key => $evento)
              <article class="events-item row page-row">
                <div class="date-label-wrapper col-md-3 col-sm-4 col-xs-4">
                    <p class="date-label">
                        <span class="month">{{$evento->data->format('M')}}</span>
                        <span class="date-number">{{$evento->data->format('d')}}</span>
                    </p>
                </div><!--//date-label-wrapper-->
                <div class="details col-md-9 col-sm-8 col-xs-8">
                    <h5 class="title">{{$evento->nome}}</h5>
                    <p class="time text-muted">{{$evento->hora_de_comeco}} - {{$evento->hora_de_termino}}
                      <br/>{{$evento->lugar}}
                    </p>
                </div><!--//details-->
              </article>
            @endforeach
          </section><!--//widget-->
        </aside> --}}
    </div>
  </div>
@endsection

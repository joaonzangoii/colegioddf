@extends('layouts.landing')
@section('content')
  <div class="single-news-area page-content-area">
    <div class="container">
      <div class="row">
        {{-- <header class="page-heading clearfix">
          <h1 class="heading-title pull-left">{{$noticia->titulo}}</h1>
          <div class="breadcrumbs pull-right">
            <ul class="breadcrumbs-list">
              <li class="breadcrumbs-label">@lang('app.youAreHere'):</li>
              <li><a href="{{ route('home') }}">@lang('app.home')</a><i class="fa fa-angle-right"></i></li>
              <li><a href="{{ route('noticias.index') }}">@lang('app.news')</a><i class="fa fa-angle-right"></i></li>
              <li class="current">{{$noticia->titulo}}</li>
            </ul>
          </div><!--//breadcrumbs-->
        </header> --}}
        <div class="col-lg-8 col-md-12">
          <div class="single-news-wrapper" id="notice-print">
            <div class="news-header clearfix">
              <div class="left-title pull-left">
                <h1 class="news-title">
                  {{ $noticia->titulo}}
                </h1>
                <span class="news-meta">{{$noticia->created_at->format('d M, Y')}}</span>
              </div>
              <div class="right-btn pull-right">
                <button type="button" class="btn btn-sm btn-outline-dark d-print-none"
                  onclick="printDiv('notice-print')">Print
                </button>
              </div>
            </div>
            <div class="news-image">
              <img src="{{ $noticia->imagem}}" alt="" class="img-fluid">
            </div>
            <div class="page-content">
              {!! $noticia->descricao !!}
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12">
          <div class="sidebar-widgets">
            <div class="widgets recent-news-widget">
              <div class="widget-title">
                <h4>@lang('app.news')</h4>
              </div>
              <div class="widgets-content">
                <ul>
                  @foreach($noticias as $key => $noticia)
                    <li>
                      <h5 class="news-title">
                        <a href="{{ route("noticias.show", $noticia->id)}}">
                          {{ $noticia->titulo}}
                        </a>
                      </h5>
                      <span class="meta">{{$noticia->created_at->format('d M, Y')}}</span>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="widgets recent-events-widget">
              <div class="widget-title">
                <h4>@lang('app.events')</h4>
              </div>
              <div class="widgets-content">
                <ul>
                  @foreach($eventos as $key => $event)
                    <li>
                      <h5 class="news-title">
                        <a>
                          {{ $event->nome}}
                        </a>
                      </h5>
                      <span class="meta">{{$event->created_at->format('d M, Y')}}</span>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@extends('layouts.landing')
@section('stylesheets')
  <link rel="resource" type="application/l10n" href="website/pdfjs/web/locale/locale.properties">
  <link href="website/pdfjs/web/viewer.css" rel="stylesheet" />
  <style type="text/css">
   .event-list-wrapper {
      min-height: 800px;
    }

    #outerContainer,
    #mainContainer,
    #myPDF .viewer {
      height: 800px;
    }

    #viewerContainer {
      background: #000000;
    }
  </style>
@endsection
@section('content')
  <div class="single-news-area page-content-area">
    <div class="container">
      <div class="row page-title">
        <div class="col-md-12">
          <div>
            <h1>@lang('app.registrationDocuments')</h1>
          </div>
        </div>
        <div class="col"><hr></div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-md-12">
          <div class="event-list-wrapper">
            <div id="myPDF" src="/files/DocumentosDeMatricula.pdf">
                @include('partials.landing.pdf')
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12">
          <div class="sidebar-widgets">
            <div class="widgets recent-news-widget">
              <div class="widget-title">
                <h4>@lang('app.latestNews')</h4>
              </div>
              <div class="widgets-content">
                <ul>
                  @foreach($noticias as $key => $noticia)
                    <li>
                      <h5 class="news-title">
                        <a href="{{ route("noticias.show", $noticia->id) }}">
                          {{ $noticia->titulo}}
                        </a>
                      </h5>
                      <span class="meta">
                        {{$noticia->created_at->format('d M, Y')}}
                      </span>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script type="text/javascript" src="website/pdfjs/build/pdf.js"></script>
<script type="text/javascript" src="website/pdfjs/web/viewer.js"></script>
@endsection

@extends('layouts.admin')

@section('template_title')
	See Message
@endsection

@section('head')
@endsection

@section('content')

 <div class="container">
	<div class="row">
    <div class="col-md-12">
		 @include('partials.admin.form-status')
    </div>
  </div>
</div>

@endsection

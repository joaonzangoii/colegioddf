@extends('layouts.landing')
@section('stylesheets')
<style>
  .event-list .event-date {
    background: #fff;
    color: #105997;
  }

  .event-list .event-date .month {
    color: #105997;
  }

  .slide-text h2 {
		font-size: 50px;
    text-shadow: 0.075em 0.08em 0.1em rgba(0, 0, 0, 1);
	}

  .slide-text h2 a {
		color: #ffffff;
	}

  .slide-text h2 a:hover {
		color: #961e26;
	}

  .slide-text p {
    color: #ffffff;
		text-shadow: 5px 5px 3px #333;
  }

  .slide-text {
    max-width: 100%;
    text-align: center;
  }

  /* CSS FOR DEMO ONLY */
body {
  font-family: Arial, sans-serif;
}
.animated  {
  -webkit-animation-duration : 3s  ;
  animation-duration : 3s  ;

  -webkit-animation-delay : 500ms  ;
  animation-delay : 500ms  ;
}

.animate-out {
  -webkit-animation-delay : 0ms  ;
  animation-delay : 0ms  ;
}

/*#main-slider h2 {
  font-size: 28px;
}*/

#main-slider p {
  width: 50%;
  text-align: center;
  margin: 0 auto 20px;
}

.owl-item {
  display: table;
}

.owl-carousel .item {
  height: 80vh;
  background-color: #fc0;
  display: table-cell;
  vertical-align: middle;
  text-align: center;
}

.btn {
  display: inline-block;
  line-height: 35px;
  height: 35px;
  text-align: center;
  padding: 0 20px;
  width: auto;
  background-color: #000;
  text-decoration: none;
  color: #fff;
}

</style>
@stop
@section('content')
  @include('partials.landing.slider', ['some' => 'data'])
  <div class="main-msg-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-7 col-md-12">
          <div class="intro-msg text-center">
            <h3 class="welcome-title">
              @lang('app.welcome') {{$siteinfo->nome}}
            </h3>
            <p class="welcome-desc">{{ $siteinfo->descricao }}</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 ml-lg-auto align-self-stretch">
          <div class="home-event-widget">
            <div class="widget-title text-center">
              <h4>@lang('app.upcomingEvents')</h4>
            </div>
            <div class="widgets-content">
              <div class="event-list">
                <ul>
                  @foreach ($eventos as $evento)
                    <li>
                      <div class="event-date text-center">
                        <h6 class="date">
                          {{$evento->data->format('d')}}
                          <span class="month">{{$evento->data->format('M')}}</span>
                        </h6>
                      </div>
                      <h5 class="event-title">
                        {{str_limit($evento->nome, 20)}}
                        <br>
                        <i class="fa fa-clock-o"></i>
                        {{$evento->hora_de_comeco}} - {{$evento->hora_de_termino}}
                        <br>
                        <i class="fa fa-map-marker"></i>
                        {{$evento->lugar}}
                      </h5>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="widget-link text-center">
              <a href="{{route('eventos')}}" class="btn btn-primary">
                @lang('app.viewAll')
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
  <script type="text/javascript">
    $(document).ready(function(){
      "use strict";
      let props = {
         autoplay:true,
         autoplayTimeout:2000,
         autoplayHoverPause:true,
         dots: true,
         merge:false,
         loop:true,
         items:1,
         nav: true,
         margin:0,
         navSpeed:500,
         navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
     };

     //  props['animateOut'] = getRandomAnimation();
     //  props['animateIn'] = getRandomAnimation();

     var owl = $("#main-slider");
     owl.owlCarousel(props);
    // Fired before current slide change
     owl.on('change.owl.carousel', function(event) {
       var $currentItem = $('.owl-item', owl).eq(event.item.index);
       var $elemsToanim = $currentItem.find("[data-animation-out]");
       setAnimation ($elemsToanim, 'out');
     });

    // Fired after current slide has been changed
     owl.on('changed.owl.carousel', function(event) {
       var $currentItem = $('.owl-item', owl).eq(event.item.index);
       var $elemsToanim = $currentItem.find("[data-animation-in]");
       setAnimation ($elemsToanim, 'in');
     });

      /*------ SCREENSHOT POPUP ------*/
      $('.lightbox-popup').magnificPopup({
         delegate: 'a',
         type:'image',
         gallery:{
             enabled:true
         }
      });

      // add animate.css class(es) to the elements to be animated
      function setAnimation ( _elem, _InOut ) {
        // Store all animationend event name in a string.
        // cf animate.css documentation
        var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        _elem.each ( function () {
          var $elem = $(this);
          var $animationType = 'animated ' + $elem.data( 'animation-' + _InOut );
          $elem.addClass($animationType).one(animationEndEvent, function () {
            $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
          });
        });
      }

      // Fired before current slide change
      owl.on('change.owl.carousel', function(event) {
        var $currentItem = $('.owl-item', owl).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-out]");
        setAnimation ($elemsToanim, 'out');
      });

      // Fired after current slide has been changed
      owl.on('changed.owl.carousel', function(event) {
        var $currentItem = $('.owl-item', owl).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-in]");
        setAnimation ($elemsToanim, 'in');
      })
    });
  </script>
@endsection

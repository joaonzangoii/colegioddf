<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
  // Homepage Route
  Route::name('home')->get('/', 'WelcomeController@welcome');
  Route::name('eventos')->get('/eventos', 'WelcomeController@eventos');
  Route::name('contactar')->get('/contactar', 'WelcomeController@contactar');
  Route::name('contactar.post')->post('/contactar', 'WelcomeController@postContactar');
  Route::name('about')->get('/sobre-o-colegio', 'WelcomeController@about');
  Route::name('informacoes')->get('/informacoes', 'WelcomeController@informacoes');
  Route::name('documentos.matricula')->get('/documentos-para-matricula', 'WelcomeController@documentosMatricula');
  Route::name('tabela.precos')->get('/tabela-de-precos', 'WelcomeController@tabelaDePrecos');
  Route::name('terms.conditions')->get('/termos-e-condicoes', 'WelcomeController@terms');
  Route::name('gallery')->get('/galeria', 'WelcomeController@gallery');

   Route::resource(
    'noticias',
    'NewsController', [
      'only'  => [
        'index',
        'show',
      ],
    ]);

  // Authentication Routes
  Auth::routes();

  // Public Routes
  Route::group(['middleware' => 'web'], function () {

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);
    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);
    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);
    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
  });

  // Registered and Activated User Routes
  Route::group(['middleware' => ['auth', 'activated']], function () {
    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'dashboard.home',   'uses' => 'PagesManagementController@index']);
    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'        => '{username}',
        'uses'      => 'ProfilesController@show',
    ]);
  });

  // Registered, activated, and is current user routes.
  Route::group(['middleware'=> ['auth', 'activated', 'currentUser']], function () {
    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only'  => [
              'show',
              'edit',
              'update',
              'create',
            ],
        ]
    );

    Route::put('profile/{username}/updateUserAccount', [
      'as'        => '{username}',
      'uses'      => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
      'as'        => '{username}',
      'uses'      => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
      'as'        => '{username}',
      'uses'      => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses'      => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);

    Route::resource('estudantes/notas', 'StudentsMarksManagementController', [
      'names' => [
        'index'   => 'estudante.notas',
        'show'   => 'estudante.notas.show',
      ],
      'only' => [
        'index',
        'show',
      ],
    ]);

    Route::resource('estudantes/{ano_lectivo}/notas', 'StudentsMarksManagementController', [
      'names' => [
        'show'   => 'estudante.ano_lectivo.notas.show',
      ],
      'only' => [
        'show',
      ],
    ]);

    Route::resource('estudantes/disciplinas', 'StudentsSubjectsManagementController', [
      'names' => [
        'show'    => 'estudante.disciplina.show',
      ],
      'only' => [
        'show',
      ]
    ]);

    Route::resource('estudantes/matriculas', 'StudentsMatriculationsManagementController', [
      'names' => [
        'index'   => 'estudante.matriculas',
        'show'    => 'estudante.matricula.show',
      ],
      'only' => [
        'index',
        'show',
      ]
    ]);

    Route::resource('estudantes/pagamentos', 'StudentsPaymentsManagementController', [
      'names' => [
        'index'   => 'estudante.pagamentos',
        'edit'    => 'estudante.pagamento.edit',
        'update'  => 'estudante.pagamento.update',
      ],
      'except' => [
        'show',
        'create',
        'store',
        'destroy',
      ]
    ]);
  });

  // Registered, activated, and is admin routes.
  Route::group(['middleware'=> ['auth', 'activated', 'role:admin']], function () {
    Route::resource('/usuarios/deleted', 'SoftDeletesController', [
      'only' => [
          'index', 'show', 'update', 'destroy',
      ],
    ]);

    Route::resource('usuarios', 'UsersManagementController', [
      'names' => [
        'index'   => 'usuarios',
        'show' => 'usuario.show',
        'edit' => 'usuario.edit',
        'destroy' => 'usuario.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::resource('professores', 'UsersManagementController', [
      'names' => [
        'index'   => 'professores',
        'show'   => 'professor.show',
        'edit' => 'professor.edit',
        'destroy' => 'professor.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::resource('encarregados', 'UsersManagementController', [
      'names' => [
        'index'   => 'encarregados',
        'show' => 'encarregado.show',
        'edit' => 'encarregado.edit',
        'destroy' => 'encarregado.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::resource('disciplinas', 'SubjectsManagementController', [
      'names' => [
        'index'   => 'disciplinas',
        'edit' => 'disciplina.edit',
        'destroy' => 'disciplina.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::resource('contactos', 'ContactosManagementController', [
      'names' => [
        'index'   => 'contactos',
        'show' => 'contacto.show',
        'edit' => 'contacto.edit',
        'destroy' => 'contacto.destroy',
      ],
      'except' => [
        'deleted',
        'create',
        'store',
      ],
    ]);

    Route::name('estudante.matriculas.readmitir')->get('estudantes/matriculas/readmitir',
                                                       'StudentsManagementController@getReadmitir');
    Route::name('estudante.matriculas.readmitir')->post('estudantes/matriculas/readmitir',
                                                       'StudentsManagementController@postReadmitir');

    Route::resource('estudantes', 'StudentsManagementController', [
      'names' => [
        'index'   => 'estudantes',
        'destroy' => 'estudante.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::resource('pagamentos', 'PaymentsManagementController', [
      'names' => [
        'index'   => 'pagamentos',
        'destroy' => 'pagamento.destroy',
      ],
    ]);


    Route::get('estudantes/adicionar/notas/create/{id}',[
      'as'=> 'estudante.notas.add.create',
      'uses' => 'AdminMarksManagementController@create'
    ]);

    Route::resource('estudantes/adicionar/notas',
      'AdminMarksManagementController',[
      'names' => [
        'index' => 'estudante.notas.add.index',
        'show'   =>  'estudante.notas.add.show',
        'store'  => 'estudante.notas.add.store',
      ],
      'except' => [
        'create',
        'edit',
      ],
    ]);

    Route::get('estudantes/adicionar/notas/ver/{student}', [
      'as' => 'student.get.marks',
      'uses' => 'AdminMarksManagementController@getMarks'
    ]);

    Route::get('estudantes/adicionar/notas/ver/current/{student}', [
      'as' => 'student.get.current.marks',
      'uses' => 'AdminMarksManagementController@getCurrentMarks'
    ]);

    Route::get('estudantes/adicionar/notas/ver/current/{student}/subject', [
      'as' => 'student.get.current.marks.by.subject',
      'uses' => 'AdminMarksManagementController@getCurrentMarksBySubject'
    ]);

    Route::get('estudantes/adicionar/disciplinas/ver/{student}', [
      'as' => 'student.get.subjects',
      'uses' => 'AdminMarksManagementController@getSubjects'
    ]);

    Route::resource('temas', 'ThemesManagementController', [
      'names' => [
        'index'   => 'temas',
        'show' => 'tema.show',
        'edit' => 'tema.edit',
        'destroy' => 'tema.destroy',
      ],
    ]);

    Route::group(['prefix' => 'admin'], function(){
      Route::name('admin.logs')->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
      Route::name('admin.php')->get('php',  'PagesManagementController@listPHPInfo');
      Route::name('admin.routes')->get('routes', 'PagesManagementController@listRoutes');
      Route::name('admin.details')->get('details', 'PagesManagementController@listSiteInfo');
      Route::name('admin.details.post')->post('details', 'PagesManagementController@postSiteInfo');
      Route::resource('paginas', 'PagesManagementController', [
        'names' => [
          'index'   => 'paginas',
          'destroy' => 'pagina.destroy',
        ],
        'except' => [
          'deleted',
        ],
      ]);
    });

    Route::name('admin.pages.info')->get('pages/info', 'PagesManagementController@getInfoPage');
    Route::name('admin.pages.info.post')->post('pages/info', 'PagesManagementController@postInfoPage');
    Route::name('admin.pages.about')->get('pages/about-us', 'PagesManagementController@getAboutUsPage');
    Route::name('admin.pages.about.post')->post('pages/about-us', 'PagesManagementController@postAboutUsPage');
    Route::resource('pages/eventos', 'EventsManagementController', [
      'names' => [
        'index'   => 'admin.eventos',
        'create'  => 'admin.eventos.create',
        'edit' => 'admin.evento.edit',
        'destroy' => 'admin.evento.destroy',
      ],
      'except' => [
        'show',
        'deleted',
      ],
    ]);
    Route::resource('pages/noticias', 'NewsManagementController', [
      'names' => [
        'index'   => 'admin.noticias',
        'create'  => 'admin.noticias.create',
        'show' => 'admin.noticia.show',
        'edit' => 'admin.noticia.edit',
        'destroy' => 'admin.noticia.destroy',
      ],
      'except' => [
        'deleted',
      ],
    ]);

    Route::name('photos.all')->get('photos/all', 'GalleryManagementController@all');
    Route::name('photos.filtered')->post('photos/filtered', 'GalleryManagementController@filtered');
    Route::resource('photos', 'GalleryManagementController', [
      'names' => [
        'index'   => 'photos',
        'edit' => 'photo.edit',
        'destroy' => 'photo.destroy',
      ],
      'except' => [
        'show',
        'deleted',
      ],
    ]);
  });
// });HERE

Route::group(['prefix' => 'encarregado', 'middleware'=>
['auth',
'activated',
 'role:encarregado'
 ]], function () {

 Route::resource('estudantes/pagamentos', 'GuardianStudentsPaymentsManagementController', [
   'names' => [
     'index'   => 'encarregado.estudantes.pagamentos',
     'edit'    => 'encarregado.estudante.pagamento.edit',
     'update'  => 'encarregado.estudante.pagamento.update',
   ],
   'except' => [
     'show',
     'create',
     'store',
     'destroy',
   ]
 ]);

  Route::resource('estudantes', 'GuardianStudentsManagementController', [
    'names' => [
      'index'   => 'encarregado.estudantes',
      'show'    => 'encarregado.estudantes.show',
      'edit'    => 'encarregado.estudantes.edit',
    ],
    'except' => [
      'create',
      'store',
      'destroy',
      'deleted',
    ],
  ]);
});

  // ===========================
  // CHATTER
  // ===========================
  // Route helper.
  $route = function ($accessor, $default = '') {
    return Config::get('chatter.routes.'.$accessor, $default);
  };
  // Middleware helper.
  $middleware = function ($accessor, $default = []) {
    return Config::get('chatter.middleware.'.$accessor, $default);
  };
  // Authentication middleware helper.
  $authMiddleware = function ($accessor) use ($middleware) {
    return array_unique(
      array_merge((array) $middleware($accessor), ['auth'])
    );
  };
  /*
  * Chatter routes.
  */
  Route::group([
    'as'         => 'chatter.',
    'prefix'     => $route('home'),
    'middleware' => $middleware('global', 'web'),
    // 'namespace'  => '\DevDojo\Chatter\Controllers',
  ], function () use ($route, $middleware, $authMiddleware) {
      // Home view.
      Route::get('/', [
        'as'         => 'home',
        'uses'       => '\DevDojo\Chatter\Controllers\ChatterController@index',
        'middleware' => $middleware('home'),
      ]);
      // Single category view.
      Route::get($route('category').'/{slug}', [
        'as'         => 'category.show',
        'uses'       => '\DevDojo\Chatter\Controllers\ChatterController@index',
        'middleware' => $middleware('category.show'),
      ]);
      /*
      * Auth routes.
      */
      // Login view.
      Route::get('login', [
        'as'   => 'login',
        'uses' => '\DevDojo\Chatter\Controllers\ChatterController@login',
      ]);
      // Register view.
      Route::get('register', [
        'as'   => 'register',
        'uses' => '\DevDojo\Chatter\Controllers\ChatterController@register',
      ]);
      /*
       * Discussion routes.
       */
      Route::group([
        'as'     => 'discussion.',
        'prefix' => $route('discussion'),
      ], function () use ($middleware, $authMiddleware) {
          // All discussions view.
          Route::get('/', [
            'as'         => 'index',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@index',
            'middleware' => $middleware('discussion.index'),
          ]);
          // Create discussion view.
          Route::get('create', [
            'as'         => 'create',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@create',
            'middleware' => $authMiddleware('discussion.create'),
          ]);
          // Store discussion action.
          Route::post('/', [
            'as'         => 'store',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@store',
            'middleware' => $authMiddleware('discussion.store'),
          ]);
          // Single discussion view.
          Route::get('{category}/{slug}', [
            'as'         => 'showInCategory',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@show',
            'middleware' => $middleware('discussion.show'),
          ]);
          // Add user notification to discussion
          Route::post('{category}/{slug}/email', [
            'as'         => 'email',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@toggleEmailNotification',
          ]);
          /*
           * Specific discussion routes.
           */
          Route::group([
              'prefix' => '{discussion}',
          ], function () use ($middleware, $authMiddleware) {
              // Single discussion view.
              Route::get('/', [
                'as'         => 'show',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@show',
                'middleware' => $middleware('discussion.show'),
              ]);
              // Edit discussion view.
              Route::get('edit', [
                'as'         => 'edit',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@edit',
                'middleware' => $authMiddleware('discussion.edit'),
              ]);
              // Update discussion action.
              Route::match(['PUT', 'PATCH'], '/', [
                'as'         => 'update',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@update',
                'middleware' => $authMiddleware('discussion.update'),
              ]);
              // Destroy discussion action.
              Route::delete('/', [
                'as'         => 'destroy',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterDiscussionController@destroy',
                'middleware' => $authMiddleware('discussion.destroy'),
              ]);
          });
      });
      /*
       * Post routes.
       */
      Route::group([
        'as'     => 'posts.',
        'prefix' => $route('post', 'posts'),
      ], function () use ($middleware, $authMiddleware) {
          // All posts view.
          Route::get('/', [
            'as'         => 'index',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@index',
            'middleware' => $middleware('post.index'),
          ]);
          // Create post view.
          Route::get('create', [
            'as'         => 'create',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@create',
            'middleware' => $authMiddleware('post.create'),
          ]);
          // Store post action.
          Route::post('/', [
            'as'         => 'store',
            'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@store',
            'middleware' => $authMiddleware('post.store'),
          ]);
          /*
           * Specific post routes.
           */
          Route::group([
            'prefix' => '{post}',
          ], function () use ($middleware, $authMiddleware) {
              // Single post view.
              Route::get('/', [
                'as'         => 'show',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@show',
                'middleware' => $middleware('post.show'),
              ]);
              // Edit post view.
              Route::get('edit', [
                'as'         => 'edit',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@edit',
                'middleware' => $authMiddleware('post.edit'),
              ]);
              // Update post action.
              Route::match(['PUT', 'PATCH'], '/', [
                'as'         => 'update',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@update',
                'middleware' => $authMiddleware('post.update'),
              ]);
              // Destroy post action.
              Route::delete('/', [
                'as'         => 'destroy',
                'uses'       => '\DevDojo\Chatter\Controllers\ChatterPostController@destroy',
                'middleware' => $authMiddleware('post.destroy'),
              ]);
          });
      });
  });
  /*
  * Atom routes
  */
  Route::get($route('home').'.atom', [
    'as'         => 'chatter.atom',
    'uses'       => '\DevDojo\Chatter\Controllers\ChatterAtomController@index',
    'middleware' => $middleware('home'),
  ]);
});

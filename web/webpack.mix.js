  const { mix } = require('laravel-mix');
/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
*/
var assets = "resources/assets/";

var landingAssets = assets + "website/",
    adminAssets   = assets + "adminlte/";

var appCss = [

];

var appJs = [
  assets + "js/router.js",
];

var adminCss = [
  "public/css/bootstrap-datepicker.css",
  "public/css/select2.min.css')}}",
  "public/css/select2-bootstrap.css')}}",
  "public/css/bootstrap-select.min.css')}}",
  "node_modules/js-datepicker/datepicker.css",
  "node_modules/pace-js/themes/blue/pace-theme-minimal.css",
];

var adminJs = [
  "node_modules/js-datepicker/datepicker.min.js	",
  "node_modules/pace-js/pace.js	",
];

mix.js('resources/assets/js/app.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .combine(appCss,'public/css/vendor.css')
  .combine(appJs,'public/js/vendor.js')
  .combine(adminCss,'public/css/admin.css')
  .combine(adminJs,'public/js/admin.js')
  // .copy(landingAssets + 'plugins/font-awesome/fonts','public/fonts', true)
  // .copy(landingAssets + 'gallery/*.css','public/css', true)
  // .copy(landingAssets + 'gallery/*.js','public/js', true)
  // .copy(landingAssets + 'images','public/images', true)
  // .copy(landingAssets + 'plugins/flexslider/images','public/images', true)
  .version();
